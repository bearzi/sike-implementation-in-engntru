#ifndef crypto_encode_761x4591_H
#define crypto_encode_761x4591_H

#define crypto_encode_761x4591_portable_STRBYTES 1158
#define crypto_encode_761x4591_portable_ITEMBYTES 2
#define crypto_encode_761x4591_portable_ITEMS 761

#define crypto_encode_761x4591 crypto_encode_761x4591_portable
#define crypto_encode_761x4591_STRBYTES crypto_encode_761x4591_portable_STRBYTES
#define crypto_encode_761x4591_ITEMBYTES crypto_encode_761x4591_portable_ITEMBYTES
#define crypto_encode_761x4591_ITEMS crypto_encode_761x4591_portable_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_encode_761x4591(unsigned char *,const void *);
#ifdef __cplusplus
}
#endif

#define crypto_encode_761x4591_IMPLEMENTATION "crypto_encode/761x4591/portable"
#define crypto_encode_761x4591_implementation "crypto_encode/761x4591/portable"
#ifndef crypto_encode_761x4591_portable_VERSION
#define crypto_encode_761x4591_portable_VERSION "-"
#endif
#define crypto_encode_761x4591_VERSION crypto_encode_761x4591_portable_VERSION

#endif
