#ifndef crypto_encode_761x3_H
#define crypto_encode_761x3_H

#define crypto_encode_761x3_ref_STRBYTES 191
#define crypto_encode_761x3_ref_ITEMBYTES 1
#define crypto_encode_761x3_ref_ITEMS 761

#define crypto_encode_761x3 crypto_encode_761x3_ref
#define crypto_encode_761x3_STRBYTES crypto_encode_761x3_ref_STRBYTES
#define crypto_encode_761x3_ITEMBYTES crypto_encode_761x3_ref_ITEMBYTES
#define crypto_encode_761x3_ITEMS crypto_encode_761x3_ref_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_encode_761x3(unsigned char *,const void *);
#ifdef __cplusplus
}
#endif

#define crypto_encode_761x3_IMPLEMENTATION "crypto_encode/761x3/ref"
#define crypto_encode_761x3_implementation "crypto_encode/761x3/ref"
#ifndef crypto_encode_761x3_ref_VERSION
#define crypto_encode_761x3_ref_VERSION "-"
#endif
#define crypto_encode_761x3_VERSION crypto_encode_761x3_ref_VERSION

#endif
