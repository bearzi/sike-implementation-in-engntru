#ifndef crypto_encode_761xint16_H
#define crypto_encode_761xint16_H

#define crypto_encode_761xint16_little_STRBYTES 1522
#define crypto_encode_761xint16_little_ITEMBYTES 2
#define crypto_encode_761xint16_little_ITEMS 761

#define crypto_encode_761xint16 crypto_encode_761xint16_little
#define crypto_encode_761xint16_STRBYTES crypto_encode_761xint16_little_STRBYTES
#define crypto_encode_761xint16_ITEMBYTES crypto_encode_761xint16_little_ITEMBYTES
#define crypto_encode_761xint16_ITEMS crypto_encode_761xint16_little_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_encode_761xint16(unsigned char *,const void *);
#ifdef __cplusplus
}
#endif

#define crypto_encode_761xint16_IMPLEMENTATION "crypto_encode/761xint16/little"
#define crypto_encode_761xint16_implementation "crypto_encode/761xint16/little"
#ifndef crypto_encode_761xint16_little_VERSION
#define crypto_encode_761xint16_little_VERSION "-"
#endif
#define crypto_encode_761xint16_VERSION crypto_encode_761xint16_little_VERSION

#endif
