#ifndef crypto_encode_int16_H
#define crypto_encode_int16_H

#define crypto_encode_int16_ref_STRBYTES 2
#define crypto_encode_int16_ref_ITEMBYTES 2
#define crypto_encode_int16_ref_ITEMS 1

#define crypto_encode_int16 crypto_encode_int16_ref
#define crypto_encode_int16_STRBYTES crypto_encode_int16_ref_STRBYTES
#define crypto_encode_int16_ITEMBYTES crypto_encode_int16_ref_ITEMBYTES
#define crypto_encode_int16_ITEMS crypto_encode_int16_ref_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_encode_int16(unsigned char *,const void *);
#ifdef __cplusplus
}
#endif

#define crypto_encode_int16_IMPLEMENTATION "crypto_encode/int16/ref"
#define crypto_encode_int16_implementation "crypto_encode/int16/ref"
#ifndef crypto_encode_int16_ref_VERSION
#define crypto_encode_int16_ref_VERSION "-"
#endif
#define crypto_encode_int16_VERSION crypto_encode_int16_ref_VERSION

#endif
