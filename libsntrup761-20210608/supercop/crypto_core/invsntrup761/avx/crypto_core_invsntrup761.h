#ifndef crypto_core_invsntrup761_H
#define crypto_core_invsntrup761_H

#define crypto_core_invsntrup761_avx_OUTPUTBYTES 1523
#define crypto_core_invsntrup761_avx_INPUTBYTES 761
#define crypto_core_invsntrup761_avx_KEYBYTES 0
#define crypto_core_invsntrup761_avx_CONSTBYTES 0

#define crypto_core_invsntrup761 crypto_core_invsntrup761_avx
#define crypto_core_invsntrup761_OUTPUTBYTES crypto_core_invsntrup761_avx_OUTPUTBYTES
#define crypto_core_invsntrup761_INPUTBYTES crypto_core_invsntrup761_avx_INPUTBYTES
#define crypto_core_invsntrup761_KEYBYTES crypto_core_invsntrup761_avx_KEYBYTES
#define crypto_core_invsntrup761_CONSTBYTES crypto_core_invsntrup761_avx_CONSTBYTES

#ifdef __cplusplus
extern "C" {
#endif
extern int crypto_core_invsntrup761(unsigned char *,const unsigned char *,const unsigned char *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_core_invsntrup761_IMPLEMENTATION "crypto_core/invsntrup761/avx"
#define crypto_core_invsntrup761_implementation "crypto_core/invsntrup761/avx"
#ifndef crypto_core_invsntrup761_avx_VERSION
#define crypto_core_invsntrup761_avx_VERSION "-"
#endif
#define crypto_core_invsntrup761_VERSION crypto_core_invsntrup761_avx_VERSION

#endif
