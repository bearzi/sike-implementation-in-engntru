#ifndef crypto_core_H
#define crypto_core_H

#include "crypto_core_multsntrup761.h"
#define crypto_core crypto_core_multsntrup761
#define crypto_core_OUTPUTBYTES crypto_core_multsntrup761_OUTPUTBYTES
#define crypto_core_INPUTBYTES crypto_core_multsntrup761_INPUTBYTES
#define crypto_core_KEYBYTES crypto_core_multsntrup761_KEYBYTES
#define crypto_core_CONSTBYTES crypto_core_multsntrup761_CONSTBYTES

#define crypto_core_PRIMITIVE "multsntrup761"
#define crypto_core_implementation crypto_core_multsntrup761_implementation
#define crypto_core_IMPLEMENTATION crypto_core_multsntrup761_IMPLEMENTATION

#endif
