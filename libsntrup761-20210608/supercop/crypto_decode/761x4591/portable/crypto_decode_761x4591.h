#ifndef crypto_decode_761x4591_H
#define crypto_decode_761x4591_H

#define crypto_decode_761x4591_portable_STRBYTES 1158
#define crypto_decode_761x4591_portable_ITEMBYTES 2
#define crypto_decode_761x4591_portable_ITEMS 761

#define crypto_decode_761x4591 crypto_decode_761x4591_portable
#define crypto_decode_761x4591_STRBYTES crypto_decode_761x4591_portable_STRBYTES
#define crypto_decode_761x4591_ITEMBYTES crypto_decode_761x4591_portable_ITEMBYTES
#define crypto_decode_761x4591_ITEMS crypto_decode_761x4591_portable_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_decode_761x4591(void *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_decode_761x4591_IMPLEMENTATION "crypto_decode/761x4591/portable"
#define crypto_decode_761x4591_implementation "crypto_decode/761x4591/portable"
#ifndef crypto_decode_761x4591_portable_VERSION
#define crypto_decode_761x4591_portable_VERSION "-"
#endif
#define crypto_decode_761x4591_VERSION crypto_decode_761x4591_portable_VERSION

#endif
