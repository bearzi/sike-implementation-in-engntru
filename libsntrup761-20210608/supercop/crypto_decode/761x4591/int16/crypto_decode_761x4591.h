#ifndef crypto_decode_761x4591_H
#define crypto_decode_761x4591_H

#define crypto_decode_761x4591_int16_STRBYTES 1158
#define crypto_decode_761x4591_int16_ITEMBYTES 2
#define crypto_decode_761x4591_int16_ITEMS 761

#define crypto_decode_761x4591 crypto_decode_761x4591_int16
#define crypto_decode_761x4591_STRBYTES crypto_decode_761x4591_int16_STRBYTES
#define crypto_decode_761x4591_ITEMBYTES crypto_decode_761x4591_int16_ITEMBYTES
#define crypto_decode_761x4591_ITEMS crypto_decode_761x4591_int16_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_decode_761x4591(void *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_decode_761x4591_IMPLEMENTATION "crypto_decode/761x4591/int16"
#define crypto_decode_761x4591_implementation "crypto_decode/761x4591/int16"
#ifndef crypto_decode_761x4591_int16_VERSION
#define crypto_decode_761x4591_int16_VERSION "-"
#endif
#define crypto_decode_761x4591_VERSION crypto_decode_761x4591_int16_VERSION

#endif
