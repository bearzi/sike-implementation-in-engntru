#ifndef crypto_decode_761x1531_H
#define crypto_decode_761x1531_H

#define crypto_decode_761x1531_portable_STRBYTES 1007
#define crypto_decode_761x1531_portable_ITEMBYTES 2
#define crypto_decode_761x1531_portable_ITEMS 761

#define crypto_decode_761x1531 crypto_decode_761x1531_portable
#define crypto_decode_761x1531_STRBYTES crypto_decode_761x1531_portable_STRBYTES
#define crypto_decode_761x1531_ITEMBYTES crypto_decode_761x1531_portable_ITEMBYTES
#define crypto_decode_761x1531_ITEMS crypto_decode_761x1531_portable_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_decode_761x1531(void *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_decode_761x1531_IMPLEMENTATION "crypto_decode/761x1531/portable"
#define crypto_decode_761x1531_implementation "crypto_decode/761x1531/portable"
#ifndef crypto_decode_761x1531_portable_VERSION
#define crypto_decode_761x1531_portable_VERSION "-"
#endif
#define crypto_decode_761x1531_VERSION crypto_decode_761x1531_portable_VERSION

#endif
