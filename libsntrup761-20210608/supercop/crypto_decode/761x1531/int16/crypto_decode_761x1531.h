#ifndef crypto_decode_761x1531_H
#define crypto_decode_761x1531_H

#define crypto_decode_761x1531_int16_STRBYTES 1007
#define crypto_decode_761x1531_int16_ITEMBYTES 2
#define crypto_decode_761x1531_int16_ITEMS 761

#define crypto_decode_761x1531 crypto_decode_761x1531_int16
#define crypto_decode_761x1531_STRBYTES crypto_decode_761x1531_int16_STRBYTES
#define crypto_decode_761x1531_ITEMBYTES crypto_decode_761x1531_int16_ITEMBYTES
#define crypto_decode_761x1531_ITEMS crypto_decode_761x1531_int16_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_decode_761x1531(void *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_decode_761x1531_IMPLEMENTATION "crypto_decode/761x1531/int16"
#define crypto_decode_761x1531_implementation "crypto_decode/761x1531/int16"
#ifndef crypto_decode_761x1531_int16_VERSION
#define crypto_decode_761x1531_int16_VERSION "-"
#endif
#define crypto_decode_761x1531_VERSION crypto_decode_761x1531_int16_VERSION

#endif
