#ifndef crypto_sort_H
#define crypto_sort_H

#include "crypto_sort_int32.h"
#define crypto_sort crypto_sort_int32
#define crypto_sort_BYTES crypto_sort_int32_BYTES

#define crypto_sort_PRIMITIVE "int32"
#define crypto_sort_implementation crypto_sort_int32_implementation
#define crypto_sort_IMPLEMENTATION crypto_sort_int32_IMPLEMENTATION

#endif
