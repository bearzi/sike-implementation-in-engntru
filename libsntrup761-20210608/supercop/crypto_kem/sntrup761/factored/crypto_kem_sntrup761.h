#ifndef crypto_kem_sntrup761_H
#define crypto_kem_sntrup761_H

#define crypto_kem_sntrup761_factored_PUBLICKEYBYTES 1158
#define crypto_kem_sntrup761_factored_SECRETKEYBYTES 1763
#define crypto_kem_sntrup761_factored_BYTES 32
#define crypto_kem_sntrup761_factored_CIPHERTEXTBYTES 1039

#define crypto_kem_sntrup761_keypair crypto_kem_sntrup761_factored_keypair
#define crypto_kem_sntrup761_keypair_batch crypto_kem_sntrup761_factored_keypair_batch
#define crypto_kem_sntrup761_enc crypto_kem_sntrup761_factored_enc
#define crypto_kem_sntrup761_dec crypto_kem_sntrup761_factored_dec
#define crypto_kem_sntrup761_PUBLICKEYBYTES crypto_kem_sntrup761_factored_PUBLICKEYBYTES
#define crypto_kem_sntrup761_SECRETKEYBYTES crypto_kem_sntrup761_factored_SECRETKEYBYTES
#define crypto_kem_sntrup761_BYTES crypto_kem_sntrup761_factored_BYTES
#define crypto_kem_sntrup761_CIPHERTEXTBYTES crypto_kem_sntrup761_factored_CIPHERTEXTBYTES

#ifdef __cplusplus
extern "C" {
#endif
extern int crypto_kem_sntrup761_keypair(unsigned char *,unsigned char *) __attribute__((visibility("default")));
extern int crypto_kem_sntrup761_keypair_batch(unsigned char *pk,unsigned char *sk, unsigned num) __attribute__((visibility("default")));
extern int crypto_kem_sntrup761_enc(unsigned char *,unsigned char *,const unsigned char *) __attribute__((visibility("default")));
extern int crypto_kem_sntrup761_dec(unsigned char *,const unsigned char *,const unsigned char *) __attribute__((visibility("default")));
#ifdef __cplusplus
}
#endif

#define crypto_kem_sntrup761_IMPLEMENTATION "crypto_kem/sntrup761/factored"
#define crypto_kem_sntrup761_implementation "crypto_kem/sntrup761/factored"
#ifndef crypto_kem_sntrup761_factored_VERSION
#define crypto_kem_sntrup761_factored_VERSION "-"
#endif
#define crypto_kem_sntrup761_VERSION crypto_kem_sntrup761_factored_VERSION

#endif
