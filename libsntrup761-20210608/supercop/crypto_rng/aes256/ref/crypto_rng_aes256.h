#ifndef crypto_rng_aes256_H
#define crypto_rng_aes256_H

#define crypto_rng_aes256_ref_KEYBYTES 32
#define crypto_rng_aes256_ref_OUTPUTBYTES 736

#define crypto_rng_aes256 crypto_rng_aes256_ref
#define crypto_rng_aes256_KEYBYTES crypto_rng_aes256_ref_KEYBYTES
#define crypto_rng_aes256_OUTPUTBYTES crypto_rng_aes256_ref_OUTPUTBYTES

#ifdef __cplusplus
extern "C" {
#endif
extern int crypto_rng_aes256(unsigned char *,unsigned char *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_rng_aes256_IMPLEMENTATION "crypto_rng/aes256/ref"
#define crypto_rng_aes256_implementation "crypto_rng/aes256/ref"
#ifndef crypto_rng_aes256_ref_VERSION
#define crypto_rng_aes256_ref_VERSION "-"
#endif
#define crypto_rng_aes256_VERSION crypto_rng_aes256_ref_VERSION

#endif
