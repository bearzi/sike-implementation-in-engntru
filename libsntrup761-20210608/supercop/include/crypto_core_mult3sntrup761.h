#ifndef crypto_core_mult3sntrup761_H
#define crypto_core_mult3sntrup761_H

#define crypto_core_mult3sntrup761_avx_OUTPUTBYTES 761
#define crypto_core_mult3sntrup761_avx_INPUTBYTES 761
#define crypto_core_mult3sntrup761_avx_KEYBYTES 761
#define crypto_core_mult3sntrup761_avx_CONSTBYTES 0

#define crypto_core_mult3sntrup761 crypto_core_mult3sntrup761_avx
#define crypto_core_mult3sntrup761_OUTPUTBYTES crypto_core_mult3sntrup761_avx_OUTPUTBYTES
#define crypto_core_mult3sntrup761_INPUTBYTES crypto_core_mult3sntrup761_avx_INPUTBYTES
#define crypto_core_mult3sntrup761_KEYBYTES crypto_core_mult3sntrup761_avx_KEYBYTES
#define crypto_core_mult3sntrup761_CONSTBYTES crypto_core_mult3sntrup761_avx_CONSTBYTES

#ifdef __cplusplus
extern "C" {
#endif
extern int crypto_core_mult3sntrup761(unsigned char *,const unsigned char *,const unsigned char *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_core_mult3sntrup761_IMPLEMENTATION "crypto_core/mult3sntrup761/avx"
#define crypto_core_mult3sntrup761_implementation "crypto_core/mult3sntrup761/avx"
#ifndef crypto_core_mult3sntrup761_avx_VERSION
#define crypto_core_mult3sntrup761_avx_VERSION "-"
#endif
#define crypto_core_mult3sntrup761_VERSION crypto_core_mult3sntrup761_avx_VERSION

#endif
