#ifndef crypto_sort_int32_H
#define crypto_sort_int32_H

#define crypto_sort_int32_avx2_BYTES 4

#define crypto_sort_int32 crypto_sort_int32_avx2
#define crypto_sort_int32_BYTES crypto_sort_int32_avx2_BYTES

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_sort_int32(void *,long long);
#ifdef __cplusplus
}
#endif

#define crypto_sort_int32_IMPLEMENTATION "crypto_sort/int32/avx2"
#define crypto_sort_int32_implementation "crypto_sort/int32/avx2"
#ifndef crypto_sort_int32_avx2_VERSION
#define crypto_sort_int32_avx2_VERSION "-"
#endif
#define crypto_sort_int32_VERSION crypto_sort_int32_avx2_VERSION

#endif
