#ifndef crypto_encode_761xint16_H
#define crypto_encode_761xint16_H

#define crypto_encode_761xint16_ref_STRBYTES 1522
#define crypto_encode_761xint16_ref_ITEMBYTES 2
#define crypto_encode_761xint16_ref_ITEMS 761

#define crypto_encode_761xint16 crypto_encode_761xint16_ref
#define crypto_encode_761xint16_STRBYTES crypto_encode_761xint16_ref_STRBYTES
#define crypto_encode_761xint16_ITEMBYTES crypto_encode_761xint16_ref_ITEMBYTES
#define crypto_encode_761xint16_ITEMS crypto_encode_761xint16_ref_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_encode_761xint16(unsigned char *,const void *);
#ifdef __cplusplus
}
#endif

#define crypto_encode_761xint16_IMPLEMENTATION "crypto_encode/761xint16/ref"
#define crypto_encode_761xint16_implementation "crypto_encode/761xint16/ref"
#ifndef crypto_encode_761xint16_ref_VERSION
#define crypto_encode_761xint16_ref_VERSION "-"
#endif
#define crypto_encode_761xint16_VERSION crypto_encode_761xint16_ref_VERSION

#endif
