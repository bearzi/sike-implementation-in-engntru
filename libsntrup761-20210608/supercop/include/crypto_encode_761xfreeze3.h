#ifndef crypto_encode_761xfreeze3_H
#define crypto_encode_761xfreeze3_H

#define crypto_encode_761xfreeze3_avx_STRBYTES 761
#define crypto_encode_761xfreeze3_avx_ITEMBYTES 2
#define crypto_encode_761xfreeze3_avx_ITEMS 761

#define crypto_encode_761xfreeze3 crypto_encode_761xfreeze3_avx
#define crypto_encode_761xfreeze3_STRBYTES crypto_encode_761xfreeze3_avx_STRBYTES
#define crypto_encode_761xfreeze3_ITEMBYTES crypto_encode_761xfreeze3_avx_ITEMBYTES
#define crypto_encode_761xfreeze3_ITEMS crypto_encode_761xfreeze3_avx_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_encode_761xfreeze3(unsigned char *,const void *);
#ifdef __cplusplus
}
#endif

#define crypto_encode_761xfreeze3_IMPLEMENTATION "crypto_encode/761xfreeze3/avx"
#define crypto_encode_761xfreeze3_implementation "crypto_encode/761xfreeze3/avx"
#ifndef crypto_encode_761xfreeze3_avx_VERSION
#define crypto_encode_761xfreeze3_avx_VERSION "-"
#endif
#define crypto_encode_761xfreeze3_VERSION crypto_encode_761xfreeze3_avx_VERSION

#endif
