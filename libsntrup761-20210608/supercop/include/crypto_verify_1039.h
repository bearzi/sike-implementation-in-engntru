#ifndef crypto_verify_1039_H
#define crypto_verify_1039_H

#define crypto_verify_1039_avx_BYTES 1039

#define crypto_verify_1039 crypto_verify_1039_avx
#define crypto_verify_1039_BYTES crypto_verify_1039_avx_BYTES

#ifdef __cplusplus
extern "C" {
#endif
extern int crypto_verify_1039(const unsigned char *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_verify_1039_IMPLEMENTATION "crypto_verify/1039/avx"
#define crypto_verify_1039_implementation "crypto_verify/1039/avx"
#ifndef crypto_verify_1039_avx_VERSION
#define crypto_verify_1039_avx_VERSION "-"
#endif
#define crypto_verify_1039_VERSION crypto_verify_1039_avx_VERSION

#endif
