#ifndef crypto_encode_761x1531_H
#define crypto_encode_761x1531_H

#define crypto_encode_761x1531_avx_STRBYTES 1007
#define crypto_encode_761x1531_avx_ITEMBYTES 2
#define crypto_encode_761x1531_avx_ITEMS 761

#define crypto_encode_761x1531 crypto_encode_761x1531_avx
#define crypto_encode_761x1531_STRBYTES crypto_encode_761x1531_avx_STRBYTES
#define crypto_encode_761x1531_ITEMBYTES crypto_encode_761x1531_avx_ITEMBYTES
#define crypto_encode_761x1531_ITEMS crypto_encode_761x1531_avx_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_encode_761x1531(unsigned char *,const void *);
#ifdef __cplusplus
}
#endif

#define crypto_encode_761x1531_IMPLEMENTATION "crypto_encode/761x1531/avx"
#define crypto_encode_761x1531_implementation "crypto_encode/761x1531/avx"
#ifndef crypto_encode_761x1531_avx_VERSION
#define crypto_encode_761x1531_avx_VERSION "-"
#endif
#define crypto_encode_761x1531_VERSION crypto_encode_761x1531_avx_VERSION

#endif
