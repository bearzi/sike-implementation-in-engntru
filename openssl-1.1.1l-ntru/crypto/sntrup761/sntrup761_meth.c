#include <string.h>
#include "crypto/evp.h"
#include "crypto/asn1.h"
#include "crypto_kem.h"

typedef struct {
    unsigned char pubkey[crypto_kem_PUBLICKEYBYTES];
    unsigned char prvkey[crypto_kem_SECRETKEYBYTES];
    unsigned char ptskey[crypto_kem_BYTES];
} SNTRUP761_KEY;

static
void sntrup761_pkey_free(EVP_PKEY *pkey)
{
    SNTRUP761_KEY *key = NULL;

    if (pkey != NULL
            && NULL != (key = EVP_PKEY_get0(pkey))) {
        OPENSSL_secure_clear_free(key, sizeof(*key));
    }
}

static int pkey_sntrup761_keygen(EVP_PKEY_CTX *ctx, EVP_PKEY *pkey)
{
    SNTRUP761_KEY *key = NULL;

    if ((key = OPENSSL_secure_zalloc(sizeof(*key))) == NULL
            || crypto_kem_keypair(key->pubkey, key->prvkey) != 0
            || !EVP_PKEY_assign(pkey, EVP_PKEY_SNTRUP761, key)) {
        OPENSSL_secure_free(key);
        return 0;
    }

    return 1;
}

static int pkey_sntrup761_encrypt(EVP_PKEY_CTX *ctx,
                            unsigned char *out, size_t *outlen,
                            const unsigned char *in, size_t inlen)
{
    SNTRUP761_KEY *key = NULL;

    if (out == NULL && outlen != NULL) {
        *outlen = crypto_kem_CIPHERTEXTBYTES;
        return 1;
    }
    if (in == NULL && inlen == 0 && out != NULL && outlen != NULL) {
        if (ctx->pkey == NULL || ctx->pkey->pkey.ptr == NULL)
            return 0;
        key = (SNTRUP761_KEY *)(ctx->pkey->pkey.ptr);
        *outlen = crypto_kem_CIPHERTEXTBYTES;
        return crypto_kem_enc(out, key->ptskey, key->pubkey) == 0;
    }
    return 0;
}

static int pkey_sntrup761_decrypt(EVP_PKEY_CTX *ctx,
                            unsigned char *out, size_t *outlen,
                            const unsigned char *in, size_t inlen)
{
    SNTRUP761_KEY *key = NULL;

    if (out == NULL && outlen == NULL && in != NULL && inlen == crypto_kem_CIPHERTEXTBYTES) {
        if (ctx->pkey == NULL || ctx->pkey->pkey.ptr == NULL)
            return 0;
        key = (SNTRUP761_KEY *)(ctx->pkey->pkey.ptr);
        return crypto_kem_dec(key->ptskey, in, key->prvkey) == 0;
    }
    return 0;
}

static int pkey_sntrup761_derive(EVP_PKEY_CTX *ctx, unsigned char *key,
                                size_t *keylen)
{
    SNTRUP761_KEY *k = NULL;

    if (keylen == NULL)
        return 0;
    if (key == NULL) {
        *keylen = crypto_kem_BYTES;
        return 1;
    }
    k = (SNTRUP761_KEY *)(ctx->pkey->pkey.ptr);
    memcpy(key, k->ptskey, crypto_kem_BYTES);
    OPENSSL_cleanse(k->ptskey, crypto_kem_BYTES);
    *keylen = crypto_kem_BYTES;
    return 1;
}

static int pkey_sntrup761_ctrl(EVP_PKEY_CTX *ctx, int type, int p1, void *p2)
{
    /* Only need to handle peer key for derivation */
    if (type == EVP_PKEY_CTRL_PEER_KEY)
        return 1;
    return -2;
}

/* BEGIN ameth stuff */

static int sntrup761_ctrl(EVP_PKEY *pkey, int op, long arg1, void *arg2)
{
    SNTRUP761_KEY *key = NULL;

    switch (op) {
    case ASN1_PKEY_CTRL_SET1_TLS_ENCPT:
        if ((key = OPENSSL_zalloc(sizeof(*key))) == NULL)
            return 0;
        memcpy(key->pubkey, arg2, arg1);
        return EVP_PKEY_assign(pkey, EVP_PKEY_SNTRUP761, key);
    case ASN1_PKEY_CTRL_GET1_TLS_ENCPT:
        if (pkey->pkey.ptr == NULL)
            return 0;
        key = (SNTRUP761_KEY *)(pkey->pkey.ptr);
        unsigned char **ppt = arg2;
        *ppt = OPENSSL_memdup(key->pubkey, crypto_kem_PUBLICKEYBYTES);
        return (*ppt == NULL) ? 0 : crypto_kem_PUBLICKEYBYTES;
    default:
        return -2;
    }
}

/* "parameters" are always equal */
static int sntrup761_param_cmp(const EVP_PKEY *a, const EVP_PKEY *b)
{
    return 1;
}

/* END ameth stuff */

const EVP_PKEY_ASN1_METHOD sntrup761_asn1_meth = {
    EVP_PKEY_SNTRUP761, /* pkey_id */
    EVP_PKEY_SNTRUP761, /* pkey_base_id */
    0, /*pkey_flags */
    "SNTRUP761", /* pem_str */
    "OpenSSL SNTRUP761 algorithm", /* info */
    0, /* pub_decode */
    0, /* pub_encode */
    0, /* pub_cmp */
    0, /* pub_print */
    0, /* priv_decode */
    0, /* priv_encode */
    0, /* priv_print */
    0, /* pkey_size */
    0, /* pkey_bits */
    0, /* pkey_security_bits */
    0, /* param_decode */
    0, /* param_encode */
    0, /* param_missing */
    0, /* param_copy */
    sntrup761_param_cmp, /* param_cmp */
    0, /* param_print */
    0, /* sig_print */
    sntrup761_pkey_free, /* pkey_free */
    sntrup761_ctrl, /* pkey_ctrl */
    0, /* old_priv_decode */
    0, /* old_priv_encode */
    0, /* item_verify */
    0, /* item_sign */
    0, /* siginf_set */
    0, /* pkey_check */
    0, /* pkey_public_check */
    0, /* pkey_param_check */
    0, /* set_priv_key */
    0, /* set_pub_key */
    0, /* get_priv_key */
    0  /* get_pub_key */
};

const EVP_PKEY_METHOD sntrup761_pkey_meth = {
    EVP_PKEY_SNTRUP761, /* pkey_id */
    0, /* flags */
    0, /* init */
    0, /* copy */
    0, /* cleanup */
    0, /* paramgen_init */
    0, /* paramgen */
    0, /* keygen_init */
    pkey_sntrup761_keygen, /* keygen */
    0, /* sign_init */
    0, /* sign */
    0, /* verify_init */
    0, /* verify */
    0, /* verify_recover_init */
    0, /* verify_recover */
    0, /* signctx_init */
    0, /* signctx */
    0, /* verifyctx_init */
    0, /* verifyctx */
    0, /* encrypt_init */
    pkey_sntrup761_encrypt, /* encrypt */
    0, /* decrypt_init */
    pkey_sntrup761_decrypt, /* decrypt */
    0, /* derive_init */
    pkey_sntrup761_derive, /* derive */
    pkey_sntrup761_ctrl, /* ctrl */
    0, /* ctrl_str */
    0, /* digestsign */
    0, /* digestverify */
    0, /* check */
    0, /* public_check */
    0  /* param_check */
};
