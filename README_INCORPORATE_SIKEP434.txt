
This contains all the steps to incorporate SIKE into the engine


To be able to use the SIKE functions  we need to compile opensslntru with the liboqs.a created and need to include the header oqs/kem_sike.h, that depends on other headers. We do so by add the library and the headers inside the CMakeFile. : 
	set(LIBOQS_LIB "/usr/local/lib/liboqs.a")
	target_link_libraries(${ENGINE_NAME} ${LIBOQS_LIB})
	set(LIBOQS_HEADER "/usr/local/include/oqs/")
	target_include_directories(${ENGINE_NAME} PRIVATE ${LIBOQS_HEADER})



engntru-20210608/engntru.c	: Add the nids of the p434 everywhere, this will allow the engine to accept another KEM algo (along with all the other modifications needed)
								-Add "0" in engntru_pkey_meth_nids[]  = {}
								-Add "engntru_pkey_meth_nids[2] = NID_SIKEP434;" in engntru_pkey_meth_nids_init()
								-Add "static EVP_PKEY_METHOD *pmeth_sikep434  = NULL;"
								-Add "0" in engntru_pkey_asn1_meth_nids[]  = {}
								-Add "    engntru_pkey_asn1_meth_nids[2] = NID_SIKEP434;//Added" in static void engntru_pkey_asn1_meth_nids_init()
								-Add static EVP_PKEY_ASN1_METHOD *ameth_sikep434 = NULL;
								- in "static int engntru_pkey_meths", when the nid is checked : add "} else if (nid == NID_SIKEP434) { //Added
											*pmeth = pmeth_sikep434;
											debug_sl("pmeth_sikep434\n");
											return 1;
											}"
								- in  engntru_register_pmeth() : we add as well an if condition : "else if (id == NID_SIKEP434) {
										engntru_register_sikep434(*pmeth);"
									This function will be declared later (as engntru_register_*__NAME)
									
								- in engntru_pkey_asn1_meths() again : " else if (nid == NID_SIKEP434) {
										*ameth = ameth_sikep434;
										debug_sl("ameth_sikep434\n");
										return 1;
									}"
									
								- in engntru_register_ameth() : add 
									else if (id == NID_SIKEP434) {  //Added
										pem_str = OBJ_nid2sn(id);
										info = "Experimental sikep434 KEM";
									} 

								- in engntru_register_methods() we need to register both the ameth and pmeth functions for sike :
								if (!engntru_register_ameth(NID_SIKEP434, &ameth_sikep434, 0)) { 
								return 0;
								}
								if (!engntru_register_pmeth(NID_SIKEP434, &pmeth_sikep434, 0 )) {
								return 0;
								}

								- And finally we need to add the variables to be cleared in engnntru_clear_methods ():
									    pmeth_sikep434  = NULL;
										ameth_sikep434  = NULL; 
										
####################################################################################################################################################

From engntru-20210608/engntru.c, we need to modify several functions to make it work. We'll go through each of the headers : 

meths/engntru_pmeth_sntrup.h : simply add a function register the evpkeymethod of sike ->
								void engntru_register_sikep434( EVP_PKEY_METHOD *pmeth); 

in ossl/engntru_object.h and .c:
					In .h : add the definition of the NID : 
						#ifdef NID_SIKEP434
						#undef NID_SIKEP434
						#endif  /* NID_SIKEP434 added*/

						extern int NID_SIKEP434;
					In .c : add the NID of SIKE at the beginning and register the nid in the function engntru_register_nids() : 
						int NID_SIKEP434;
						
					in ..._register_nids():
						ENGNTRU_REGISTER_NID(SIKEP434);


####################################################################################################################################################
ossl/engntru_objects_internal.h:
	The added definitions correspond to the SIKEP434 values stored in the (to be modified) openssl

	#define engntru_OID_SIKEP434    "1.3.6.1.4.1.50263.0.3.1.22" //Added
	#define engntru_SN_SIKEP434     "SIKEP434"
	#define engntru_LN_SIKEP434     "SIKEP434 KEM"

####################################################################################################################################################
meths/kem_keypair.c:
	We include the header (that was created) and corresponding to the sikep434 value:
		#include "meths/internal/sikep434.h" 
	And add to the function engntru_kem_get_nid_data (that fetch the general information about the certain NID of the engine : 
		} else if (nid== NID_SIKEP434) 
			return sikep434_data();
		}
####################################################################################################################################################
meths/internal/sikep434.h(File to create): 
	We simply write the abstract definition of the function that will be written in sikep434.c : 
		#include "meths/kem_keypair.h"

		const struct engntru_kem_nid_data_st *sikep434_data(void);

meths/internal/sikep434.c(File to create): 
	Here we write the complete file such as the sntrup761:
	#include "meths/internal/sntrup761.h"

	#include "ossl/engntru_objects.h"

	#include <crypto_kem_sntrup761.h>

	//Added
	#include <oqs/oqs.h>

	#define CRYPTO_SECRETKEYBYTES OQS_KEM_sike_p434_length_secret_key
	#define CRYPTO_PUBLICKEYBYTES OQS_KEM_sike_p434_length_public_key
	#define CRYPTO_CIPHERTEXTBYTES OQS_KEM_sike_p434_length_ciphertext
	#define CRYPTO_BYTES OQS_KEM_sike_p434_length_shared_secret


	const struct engntru_kem_nid_data_st *sikep434_data(void)
	{
		static struct engntru_kem_nid_data_st _sikep434_data = { //Modified
			.nid = NID_undef,
			.name = "SIKEP434",//Added
			.sk_bytes = CRYPTO_SECRETKEYBYTES,
			.pk_bytes = CRYPTO_PUBLICKEYBYTES,
			.ct_bytes = CRYPTO_CIPHERTEXTBYTES,
			.ss_bytes = CRYPTO_BYTES,

			.bits          = 8 * CRYPTO_PUBLICKEYBYTES,
			.security_bits = 153 //HMM HH
		};

		if (_sikep434_data.nid == NID_undef)
			_sikep434_data.nid = NID_SIKEP434;

		return &_sikep434_data;
	}
				

	=====
	One thing I don't know what would be the correct value is
	the security_bits of the structure, there is no mention in the OQS header ?????
	==> in "meths/kem_keypair.h"  "security_bits" = * Bits of security is defined in SP800-57 => reference to nist
	https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-57pt1r5.pdf  (security strength)
	https://csrc.nist.gov/CSRC/media/Presentations/sike-round-2-presentation/images-media/sike-jao.pdf
	=====
####################################################################################################################################################

meths/engntru_asn1_meth.c:
	There are multiple places where we have to add something : 
		Nearby the DECLARE_ENGNTRU_CONCRETE_FUNCTION : 
			DECLARE_ENGNTRU_CONCRETE_FUNCTIONS(sikep434, NID_SIKEP434, (OBJ_nid2sn(NID_SIKEP434))    );
			
		in the engntru_register_asn1_meth() function, we add another condition to accept the NID of SIKE : 
			&& nid != NID_SIKEP434)
			
		Then still in this function we map all the encoding, decoding, copying function from SIKEP434 to the generic function of the engine : 
			} else if (nid == NID_SIKEP434) { //Added
					debug("USING sikep434 functions\n");
					// HMM what to replace with ??
					EVP_PKEY_asn1_set_ctrl(*ameth, engntru_sikep434_ctrl);

			#if 0
					/* decode, encode, print for private and public keys */
					asn1_priv_decode_fn = engntru_sikep434_priv_decode;
					asn1_priv_encode_fn = engntru_sikep434_priv_encode;
					asn1_priv_print_fn  = engntru_sikep434_priv_print;
					asn1_pub_decode_fn  = engntru_sikep434_pub_decode;
					asn1_pub_encode_fn  = engntru_sikep434_pub_encode;
					asn1_pub_print_fn   = engntru_sikep434_pub_print;

					/* functions to handle domain parameters for keys */
					asn1_param_decode_fn  = engntru_sikep434_param_decode;
					asn1_param_encode_fn  = engntru_sikep434_param_encode;
					asn1_param_missing_fn  = engntru_sikep434_param_missing;
					asn1_param_copy_fn  = engntru_sikep434_param_copy;
					asn1_param_print_fn   = engntru_sikep434_param_print;

					/* for X509 certificates */
					asn1_item_verify_fn = engntru_sikep434_item_verify;
					asn1_item_sign_fn   = engntru_sikep434_item_sign;
			#endif
			}

			

####################################################################################################################################################
meths/engntru_pmeth_sntrup.c:
	Here we add the mapping for the pmeth functions : 
	static 
	int sikep434_keygen(EVP_PKEY_CTX *ctx, EVP_PKEY *pkey)
	{
		return generic_kem_keygen(ctx, pkey, NID_SIKEP434);
	}

	void engntru_register_sikep434(EVP_PKEY_METHOD *pmeth)
	{
		EVP_PKEY_meth_set_keygen(pmeth, NULL, sikep434_keygen);
		EVP_PKEY_meth_set_encrypt(pmeth, NULL, generic_kem_encrypt);
		EVP_PKEY_meth_set_decrypt(pmeth, NULL, generic_kem_decrypt);
		EVP_PKEY_meth_set_derive(pmeth, NULL, generic_kem_derive);
		EVP_PKEY_meth_set_ctrl(pmeth, pmeth_generic_kem_ctrl, NULL);
	}


####################################################################################################################################################
Afterwards we have to take care of all the different providers : 
	providers/serial_lib/kem.c:
		Add the header : #include <oqs/oqs.h>

		In the engntru_prov_kem_keygen() :
			} else if (kp->nid == NID_SIKEP434) {
			  info("OQS_KEM_SIKE_p434 keypair\n");
			  return OQS_KEM_sike_p434_keypair(kp->pk, kp->sk) ? 0 : 1;
			}
		In the engntru_prov_kem_encapsulate():
			 else if (kp->nid == NID_SIKEP434) {//ADDED
				info("OQS_KEM_sike_p434 encapsulation\n");
				return OQS_KEM_sike_p434_encaps(ct, ss, kp->pk) ? 0 : 1;
			}
		In the engntru_prov_kem_decapsulate:
			} else if (kp->nid == NID_SIKEP434) {
				info("OQS_KEM_SIKE decaps\n");
					  return OQS_KEM_sike_p434_decaps(ss, ct, kp->sk) ? 0 : 1;
			}
	===============================		
	Now for the batch_lib provider : 
	
	providers/batch_lib/kem.c:	
		Add the header : #include <oqs/oqs.h>
		
		And again add the NID in the encaps and decaps function (here the keygen is actually done in the batch.c (that is to be modified as well))
		In engntru_prov_kem_encapsulate: 
			} else if (kp->nid == NID_SIKEP434) {//ADDED
				info("OQS_KEM_sike_p434 encapsulation\n");
				return OQS_KEM_sike_p434_encaps(ct, ss, kp->pk) ? 0 : 1;
			}
		In engntru_prov_kem_decapsulate:
			 else if (kp->nid == NID_SIKEP434) {
				info("OQS_KEM_SIKE decaps\n");
				return OQS_KEM_sike_p434_decaps(ss, ct, kp->sk) ? 0 : 1;
			}

	providers/batch_lib/batch.c:
		As usual add the header :#include <oqs/oqs.h>
		The batch size of SIKE is also defined : #define SIKEP434_BATCH_SIZE 32 //Added
		
		We add another context that will be responsible for sikep434 in the structure of the global_ctx, by following the actual code written for ntrup: 
			BATCH_CTX *sikep434_ctx; //Added
			
		We add a functiont that will generate the keys for the key batch (can be added right after the "fake_keypair_batch" of sntrup : 
			static int crypto_kem_sike434_keypair_batch(unsigned char *pk, unsigned char *s>
			{
				unsigned i;
				const struct engntru_kem_nid_data_st *nid_data = NULL;
				nid_data = engntru_kem_get_nid_data(NID_SIKEP434); //Test NID_SNTRUP761

				for (i = 0; i < n; i++) {
					int ret;
					ret = OQS_KEM_sike_p434_keypair(pk + i * nid_data->pk_bytes,
													   sk + i * nid_data->sk_bytes);
					if (ret != 0) {
						errorf("Error generating keypair");
						return 1;
					}
				}

				return 0;
			}
			/////////////////////////////////////////////////////////////////////////

		In the BATCH_STORE_fill(), which consists in attribuing the functions of the NID to the generic ones used by the store, we add another if condition with NID_SIKEP434 where we define the keygen_fn to be the one of sike:
			else if (nid == NID_SIKEP434) { //TEST
			#ifndef FAKE_BATCH_KEYGEN
					crypto_kem_batch_keygen_fn = crypto_kem_sike434_keypair_batch;
			#else
					crypto_kem_batch_keygen_fn = crypto_kem_sike434_keypair_batch;//_fake_k>
			#endif
					info("crypto_kem_sikep434_keypair_batch\n");
		=======
		??? here the ifndef may not be useful as we use no matter what the same function
		???
		=======
			
		In the BACTH_CTX_new(), where we allocate the new context, we add : 
			} else if (nid_data->nid == NID_SIKEP434) { //Test
				batch_size = ctx->batch_size = SIKEP434_BATCH_SIZE;
			}
			
		In the engntru_prov_sntrup_get_keypair(), where we acquire the lock according to the context, check that the store is still full, then copy and retrieve the key, we add another option for SIKE : 
			 else if (kp->nid == NID_SIKEP434) { //TEST ADDED
			if (global_ctx.sikep434_ctx == NULL) {
				ctx = global_ctx.sikep434_ctx = BATCH_CTX_new(kp->nid_data);
				if (ctx == NULL)
					return 0;
			} else {
				ctx = global_ctx.sikep434_ctx;
			}
			}

		In the engntru_prov_kem_batch_deinit, where we free the different components, we need to free the sike context as well: 
			BATCH_CTX_free(global_ctx.sikep434_ctx);//TEST

		And finally in the engntru_prov_kem_batch_get_keypair(), we add the condition of NID_SIKEP434:
			kp->nid == NID_SIKEP434
			
	
	Now it's turn to modify the async providers, it is quite similar to the batch_lib prodiders, as the only function that changes is the one taking care of refilling the store :
	
	providers/async_batch_lib/kem.c:
		As usual add the header :#include <oqs/oqs.h>
		In engntru_prov_kem_encapsulate: 
			} else if (kp->nid == NID_SIKEP434) {//ADDED
				info("OQS_KEM_sike_p434 encapsulation\n");
				return OQS_KEM_sike_p434_encaps(ct, ss, kp->pk) ? 0 : 1;
			}
		In engntru_prov_kem_decapsulate:
			 else if (kp->nid == NID_SIKEP434) {
				info("OQS_KEM_SIKE decaps\n");
				return OQS_KEM_sike_p434_decaps(ss, ct, kp->sk) ? 0 : 1;
			}
	providers/async_batch_lib/batch.c:
		As usual add the header :#include <oqs/oqs.h>
		The batch size of SIKE is also defined : #define SIKEP434_BATCH_SIZE 32 //Added
		
		We add another context that will be responsible for sikep434 in the structure of the global_ctx, by following the actual code written for ntrup: 
			BATCH_CTX *sikep434_ctx; //Added
			
		We add a functiont that will generate the keys for the key batch (can be added right after the "fake_keypair_batch" of sntrup : 
			static int crypto_kem_sike434_keypair_batch(unsigned char *pk, unsigned char *s>
			{
				unsigned i;
				const struct engntru_kem_nid_data_st *nid_data = NULL;
				nid_data = engntru_kem_get_nid_data(NID_SIKEP434); //Test NID_SNTRUP761

				for (i = 0; i < n; i++) {
					int ret;
					ret = OQS_KEM_sike_p434_keypair(pk + i * nid_data->pk_bytes,
													   sk + i * nid_data->sk_bytes);
					if (ret != 0) {
						errorf("Error generating keypair");
						return 1;
					}
				}

				return 0;
			}
			/////////////////////////////////////////////////////////////////////////

		In the BATCH_STORE_fill(), which consists in attribuing the functions of the NID to the generic ones used by the store, we add another if condition with NID_SIKEP434 where we define the keygen_fn to be the one of sike:
			else if (nid == NID_SIKEP434) { //TEST
			#ifndef FAKE_BATCH_KEYGEN
					crypto_kem_batch_keygen_fn = crypto_kem_sike434_keypair_batch;
			#else
					crypto_kem_batch_keygen_fn = crypto_kem_sike434_keypair_batch;//_fake_k>
			#endif
					info("crypto_kem_sikep434_keypair_batch\n");
		=======
		??? here the ifndef may not be useful as we use no matter what the same function
		=======
			
		In the BACTH_CTX_new(), where we allocate the new context, we add : 
			} else if (nid_data->nid == NID_SIKEP434) { //Test
				batch_size = ctx->batch_size = SIKEP434_BATCH_SIZE;
			}
			
		In the engntru_prov_sntrup_get_keypair(), where we acquire the lock according to the context, check that the store is still full, then copy and retrieve the key, we add another option for SIKE : 
			 else if (kp->nid == NID_SIKEP434) { //TEST ADDED
			if (global_ctx.sikep434_ctx == NULL) {
				ctx = global_ctx.sikep434_ctx = BATCH_CTX_new(kp->nid_data);
				if (ctx == NULL)
					return 0;
			} else {
				ctx = global_ctx.sikep434_ctx;
			}
			}

		In the engntru_prov_kem_batch_deinit, where we free the different components, we need to free the sike context as well: 
			BATCH_CTX_free(global_ctx.sikep434_ctx);//TEST

		And finally in the engntru_prov_kem_batch_get_keypair(), we add the condition of NID_SIKEP434:
			kp->nid == NID_SIKEP434
	
		
		
		

			
		
####################################################################################################################################################

Now we have to modify the version of openssl that is already alterated to take into account the version of SIKE we implemented:
	
	All of these changes are made into the opensslntrul-1.1.1l-ntru folder, those changes will then be ported to /local/ folder (from which we're going to create the connection : 
	
	apps/openssl.cnf : 
		We add the oid of SIKEP434 (the same as from the other file)
		we add it under the "[ new_oids ]" chapter:
			SIKEP434=1.3.6.1.4.1.50263.0.3.1.22


	fuzz/oids.txt at the end of the file we can add (the number is according to the previous OBJ_SNTRUP, we simply increment) : 
		OBJ_SIKEP434="\x2B\x06\x01\x04\x01\x83\x88\x57\x00\x03\x01\x16"
		
	include/openssl/obj_mac.h:
		Nearby #define OBJ_SNTRUP857, add : 
			#define SN_SIKEP434             "SIKEP434" //Added
			#define NID_SIKEP434            1197
			#define OBJ_SIKEP434            1L,3L,6L,1L,4L,1L,50263L,0L,3L,1L,22L
	
	include/openssl/evp.h:
		#define EVP_PKEY_SIKEP434 NID_SIKEP434
	
	ssl/t1_lib.c : 
		{NID_SIKEP434, 192, TLS_GROUP_KEM_FOR_TLS1_3, 0xFE02},

	crypto/objects/obj_dat.h: 
		0x2B,0x06,0x01,0x04,0x01,0x83,0x88,0x57,0x00,0x03,0x01,0x16,  /* [ 7785] OBJ_SIKEP434 */
		
		{"SIKEP434", "SIKEP434", NID_SIKEP434, 12, &so[7785]}, # 12 of difference
		
		Then we have to place twice the 1997 process: 1197,    /* "SIKEP434"  */

	
		Then : 1197,    /* OBJ_SIKEP434                  1 3 6 1 4 1 50263 0 3 1 22 */
		
	crypto/objects/obj_mac.num: Place at the end
		SIKEP434                1197

####################################################################################################################################################
