# Imported from https://github.com/jedisct1/libsodium/blob/2bf56d7ebd8a3167d6bb36340230b6013218a5cc/contrib/Findsodium.cmake
# Originally written in 2016 by Henrik Steffen Gaßmann <henrik@gassmann.onl>
# Adapted in 2020 by Nicola Tuveri <nic.tuv@gmail.com>
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see
#
# http://creativecommons.org/publicdomain/zero/1.0/
#
# ##############################################################################
# Tries to find the local libsntrup857batch installation.
#
# On Windows the sntrup857batch_DIR environment variable is used as a default hint which
# can be overridden by setting the corresponding cmake variable.
#
# Once done the following variables will be defined:
#
# sntrup857batch_FOUND sntrup857batch_INCLUDE_DIRS
# sntrup857batch_LIBRARIES_DEBUG sntrup857batch_LIBRARIES_RELEASE
# sntrup857batch_LIBRARIES (depending on the build type)
# sntrup857batch_VERSION_STRING
#
# Furthermore an imported "sntrup857batch" target is created.
#

if(CMAKE_C_COMPILER_ID STREQUAL "GNU" OR CMAKE_C_COMPILER_ID STREQUAL "Clang")
  set(_GCC_COMPATIBLE 1)
endif()

# static library option
if(NOT DEFINED sntrup857batch_USE_STATIC_LIBS)
  option(sntrup857batch_USE_STATIC_LIBS "enable to statically link against sntrup857batch" OFF)
endif()
if(sntrup857batch_USE_STATIC_LIBS)
  message(FATAL_ERROR "sntrup857batch_USE_STATIC_LIBS is not supported yet")
endif()
if(NOT (sntrup857batch_USE_STATIC_LIBS EQUAL sntrup857batch_USE_STATIC_LIBS_LAST))
  unset(sntrup857batch_LIBRARIES CACHE)
  unset(sntrup857batch_LIBRARIES_DEBUG CACHE)
  unset(sntrup857batch_LIBRARIES_RELEASE CACHE)
  unset(sntrup857batch_DLL_DEBUG CACHE)
  unset(sntrup857batch_DLL_RELEASE CACHE)
  set(sntrup857batch_USE_STATIC_LIBS_LAST
      ${sntrup857batch_USE_STATIC_LIBS}
      CACHE INTERNAL "internal change tracking variable")
endif()

if(NOT UNIX)
  message(FATAL_ERROR "Non-UNIX platforms are not supported yet")
endif(NOT UNIX)

# ##############################################################################
# UNIX
if(UNIX)
  # import pkg-config
  find_package(PkgConfig QUIET)
  if(PKG_CONFIG_FOUND)
    pkg_check_modules(sntrup857batch_PKG QUIET libsntrup857batch)
  endif()

  if(sntrup857batch_USE_STATIC_LIBS)
    if(sntrup857batch_PKG_STATIC_LIBRARIES)
      foreach(_libname ${sntrup857batch_PKG_STATIC_LIBRARIES})
        if(NOT _libname MATCHES "^lib.*\\.a$") # ignore strings already ending
                                               # with .a
          list(INSERT sntrup857batch_PKG_STATIC_LIBRARIES 0 "lib${_libname}.a")
        endif()
      endforeach()
      list(REMOVE_DUPLICATES sntrup857batch_PKG_STATIC_LIBRARIES)
    else()
      # if pkgconfig for libsntrup857batch doesn't provide static lib info, then
      # override PKG_STATIC here..
      set(sntrup857batch_PKG_STATIC_LIBRARIES libsntrup857batch.a)
    endif()

    set(XPREFIX sntrup857batch_PKG_STATIC)
  else()
    if(sntrup857batch_PKG_LIBRARIES STREQUAL "")
      set(sntrup857batch_PKG_LIBRARIES sntrup857batch)
    endif()

    set(XPREFIX sntrup857batch_PKG)
  endif()


  find_path(sntrup857batch_INCLUDE_DIRS "crypto_kem_sntrup857.h" HINTS ${${XPREFIX}_INCLUDE_DIRS})
  find_library(sntrup857batch_LIBRARIES_DEBUG
               NAMES ${${XPREFIX}_LIBRARIES}
               HINTS ${${XPREFIX}_LIBRARY_DIRS})
  find_library(sntrup857batch_LIBRARIES_RELEASE
               NAMES ${${XPREFIX}_LIBRARIES}
               HINTS ${${XPREFIX}_LIBRARY_DIRS})

  # ############################################################################
  # Windows
elseif(WIN32)
  set(sntrup857batch_DIR "$ENV{sntrup857batch_DIR}" CACHE FILEPATH "sntrup857batch install directory")
  mark_as_advanced(sntrup857batch_DIR)

  find_path(sntrup857batch_INCLUDE_DIRS sntrup857batch.h
            HINTS ${sntrup857batch_DIR}
            PATH_SUFFIXES include)

  if(MSVC)
    # detect target architecture
    file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/arch.c" [=[
            #if defined _M_IX86
            #error ARCH_VALUE x86_32
            #elif defined _M_X64
            #error ARCH_VALUE x86_64
            #endif
            #error ARCH_VALUE unknown
        ]=])
    try_compile(_UNUSED_VAR "${CMAKE_CURRENT_BINARY_DIR}"
                "${CMAKE_CURRENT_BINARY_DIR}/arch.c"
                OUTPUT_VARIABLE _COMPILATION_LOG)
    string(REGEX
           REPLACE ".*ARCH_VALUE ([a-zA-Z0-9_]+).*"
                   "\\1"
                   _TARGET_ARCH
                   "${_COMPILATION_LOG}")

    # construct library path
    if(_TARGET_ARCH STREQUAL "x86_32")
      string(APPEND _PLATFORM_PATH "Win32")
    elseif(_TARGET_ARCH STREQUAL "x86_64")
      string(APPEND _PLATFORM_PATH "x64")
    else()
      message(
        FATAL_ERROR
          "the ${_TARGET_ARCH} architecture is not supported by Findsntrup857batch.cmake."
        )
    endif()
    string(APPEND _PLATFORM_PATH "/$$CONFIG$$")

    if(MSVC_VERSION LESS 1900)
      math(EXPR _VS_VERSION "${MSVC_VERSION} / 10 - 60")
    else()
      math(EXPR _VS_VERSION "${MSVC_VERSION} / 10 - 50")
    endif()
    string(APPEND _PLATFORM_PATH "/v${_VS_VERSION}")

    if(sntrup857batch_USE_STATIC_LIBS)
      string(APPEND _PLATFORM_PATH "/static")
    else()
      string(APPEND _PLATFORM_PATH "/dynamic")
    endif()

    string(REPLACE "$$CONFIG$$"
                   "Debug"
                   _DEBUG_PATH_SUFFIX
                   "${_PLATFORM_PATH}")
    string(REPLACE "$$CONFIG$$"
                   "Release"
                   _RELEASE_PATH_SUFFIX
                   "${_PLATFORM_PATH}")

    find_library(sntrup857batch_LIBRARIES_DEBUG libsntrup857batch.lib
                 HINTS ${sntrup857batch_DIR}
                 PATH_SUFFIXES ${_DEBUG_PATH_SUFFIX})
    find_library(sntrup857batch_LIBRARIES_RELEASE libsntrup857batch.lib
                 HINTS ${sntrup857batch_DIR}
                 PATH_SUFFIXES ${_RELEASE_PATH_SUFFIX})
    if(NOT sntrup857batch_USE_STATIC_LIBS)
      set(CMAKE_FIND_LIBRARY_SUFFIXES_BCK ${CMAKE_FIND_LIBRARY_SUFFIXES})
      set(CMAKE_FIND_LIBRARY_SUFFIXES ".dll")
      find_library(sntrup857batch_DLL_DEBUG libsntrup857batch
                   HINTS ${sntrup857batch_DIR}
                   PATH_SUFFIXES ${_DEBUG_PATH_SUFFIX})
      find_library(sntrup857batch_DLL_RELEASE libsntrup857batch
                   HINTS ${sntrup857batch_DIR}
                   PATH_SUFFIXES ${_RELEASE_PATH_SUFFIX})
      set(CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES_BCK})
    endif()

  elseif(_GCC_COMPATIBLE)
    if(sntrup857batch_USE_STATIC_LIBS)
      find_library(sntrup857batch_LIBRARIES_DEBUG libsntrup857batch.a
                   HINTS ${sntrup857batch_DIR}
                   PATH_SUFFIXES lib)
      find_library(sntrup857batch_LIBRARIES_RELEASE libsntrup857batch.a
                   HINTS ${sntrup857batch_DIR}
                   PATH_SUFFIXES lib)
    else()
      find_library(sntrup857batch_LIBRARIES_DEBUG libsntrup857batch.dll.a
                   HINTS ${sntrup857batch_DIR}
                   PATH_SUFFIXES lib)
      find_library(sntrup857batch_LIBRARIES_RELEASE libsntrup857batch.dll.a
                   HINTS ${sntrup857batch_DIR}
                   PATH_SUFFIXES lib)

      file(GLOB _DLL
           LIST_DIRECTORIES false
           RELATIVE "${sntrup857batch_DIR}/bin"
           "${sntrup857batch_DIR}/bin/libsntrup857batch*.dll")
      find_library(sntrup857batch_DLL_DEBUG ${_DLL} libsntrup857batch
                   HINTS ${sntrup857batch_DIR}
                   PATH_SUFFIXES bin)
      find_library(sntrup857batch_DLL_RELEASE ${_DLL} libsntrup857batch
                   HINTS ${sntrup857batch_DIR}
                   PATH_SUFFIXES bin)
    endif()
  else()
    message(FATAL_ERROR "this platform is not supported by Findsntrup857batch.cmake")
  endif()

  # ############################################################################
  # unsupported
else()
  message(FATAL_ERROR "this platform is not supported by Findsntrup857batch.cmake")
endif()

# ##############################################################################
# common stuff

# extract sntrup857batch version
if(sntrup857batch_INCLUDE_DIRS)
  set(_VERSION_HEADER "${sntrup857batch_INCLUDE_DIRS}/sntrup857batch/version.h")
  if(EXISTS "${_VERSION_HEADER}")
    file(READ "${_VERSION_HEADER}" _VERSION_HEADER_CONTENT)
    string(
      REGEX
      REPLACE
        ".*define[ \t]+sntrup857batch_VERSION_STRING[^\"]+\"([^\"]+)\".*"
        "\\1"
        sntrup857batch_VERSION_STRING
        "${_VERSION_HEADER_CONTENT}")
    set(sntrup857batch_VERSION_STRING "${sntrup857batch_VERSION_STRING}")
  endif()
endif()

# communicate results
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(sntrup857batch
                                  REQUIRED_VARS
                                  sntrup857batch_LIBRARIES_RELEASE
                                  sntrup857batch_LIBRARIES_DEBUG
                                  sntrup857batch_INCLUDE_DIRS
                                  VERSION_VAR
                                  sntrup857batch_VERSION_STRING)

# mark file paths as advanced
mark_as_advanced(sntrup857batch_INCLUDE_DIRS)
mark_as_advanced(sntrup857batch_LIBRARIES_DEBUG)
mark_as_advanced(sntrup857batch_LIBRARIES_RELEASE)
if(WIN32)
  mark_as_advanced(sntrup857batch_DLL_DEBUG)
  mark_as_advanced(sntrup857batch_DLL_RELEASE)
endif()

# create imported target
if(sntrup857batch_USE_STATIC_LIBS)
  set(_LIB_TYPE STATIC)
else()
  set(_LIB_TYPE SHARED)
endif()
add_library(sntrup857batch ${_LIB_TYPE} IMPORTED)

set_target_properties(sntrup857batch
                      PROPERTIES INTERFACE_INCLUDE_DIRECTORIES
                                 "${sntrup857batch_INCLUDE_DIRS}"
                                 IMPORTED_LINK_INTERFACE_LANGUAGES
                                 "C")

if(sntrup857batch_USE_STATIC_LIBS)
  set_target_properties(sntrup857batch
                        PROPERTIES INTERFACE_COMPILE_DEFINITIONS
                                   "SNTRUP857BATCH_STATIC"
                                   IMPORTED_LOCATION
                                   "${sntrup857batch_LIBRARIES_RELEASE}"
                                   IMPORTED_LOCATION_DEBUG
                                   "${sntrup857batch_LIBRARIES_DEBUG}")
else()
  if(UNIX)
    set_target_properties(sntrup857batch
                          PROPERTIES IMPORTED_LOCATION
                                     "${sntrup857batch_LIBRARIES_RELEASE}"
                                     IMPORTED_LOCATION_DEBUG
                                     "${sntrup857batch_LIBRARIES_DEBUG}")
  elseif(WIN32)
    set_target_properties(sntrup857batch
                          PROPERTIES IMPORTED_IMPLIB
                                     "${sntrup857batch_LIBRARIES_RELEASE}"
                                     IMPORTED_IMPLIB_DEBUG
                                     "${sntrup857batch_LIBRARIES_DEBUG}")
    if(NOT (sntrup857batch_DLL_DEBUG MATCHES ".*-NOTFOUND"))
      set_target_properties(sntrup857batch
                            PROPERTIES IMPORTED_LOCATION_DEBUG
                                       "${sntrup857batch_DLL_DEBUG}")
    endif()
    if(NOT (sntrup857batch_DLL_RELEASE MATCHES ".*-NOTFOUND"))
      set_target_properties(sntrup857batch
                            PROPERTIES IMPORTED_LOCATION_RELWITHDEBINFO
                                       "${sntrup857batch_DLL_RELEASE}"
                                       IMPORTED_LOCATION_MINSIZEREL
                                       "${sntrup857batch_DLL_RELEASE}"
                                       IMPORTED_LOCATION_RELEASE
                                       "${sntrup857batch_DLL_RELEASE}")
    endif()
  endif()
endif()
