set(OPENSSL_APP ${OPENSSL_ROOT_DIR}/bin/openssl)
message(STATUS "DEBUG:OPENSSL_APP:${OPENSSL_APP}")
if(NOT EXISTS "${OPENSSL_APP}")
    set(OPENSSL_APP openssl)
endif()


set(env_no_verbosity
    "ENGNTRU_DEBUG=-3"
    )

set(env_debug_verbosity
    "ENGNTRU_DEBUG=-9"
    )

set(env_conf_engines
    "OPENSSL_CONF=${CMAKE_SOURCE_DIR}/tests/engntru.cnf"
    "OPENSSL_ENGINES=${CMAKE_BINARY_DIR}"
    )

set(env_defaults_silent
    ${env_conf_engines}
    ${env_no_verbosity}
    )

set(env_defaults
    ${env_conf_engines}
    ${env_debug_verbosity}
    )
