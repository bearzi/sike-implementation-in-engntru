/*
 *  engNTRU - An engine for batch NTRU Prime PQC in OpenSSL.
 *  Copyright (C) 2019 Tampere University Foundation sr
 *
 *  This file is part of engNTRU.
 *
 *  engNTRU is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or (at your
 *  option) any later version.
 *
 *  engNTRU is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ENGNTRU_PMETH_SNTRUP_H
#define ENGNTRU_PMETH_SNTRUP_H

#include <openssl/evp.h>

void engntru_register_sntrup761(EVP_PKEY_METHOD *pmeth);
void engntru_register_sntrup857(EVP_PKEY_METHOD *pmeth);
void engntru_register_sikep434( EVP_PKEY_METHOD *pmeth); //Added

#endif /* !defined(ENGNTRU_PMETH_SNTRUP_H) */
