/*
 *  engNTRU - An engine for batch NTRU Prime PQC in OpenSSL.
 *  Copyright (C) 2019 Tampere University Foundation sr
 *
 *  This file is part of engNTRU.
 *
 *  engNTRU is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or (at your
 *  option) any later version.
 *
 *  engNTRU is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "meths/engntru_pmeth_sntrup.h"
#include "debug/debug.h"

#include <openssl/crypto.h>
#include <openssl/err.h>
#include "ossl/engntru_err.h"

#include "ossl/engntru_objects.h"
#include "meths/kem_keypair.h"

#include "providers/api/kem.h"

#include <string.h>

static
int generic_kem_keygen(EVP_PKEY_CTX *ctx, EVP_PKEY *pkey, int nid)
{
    ENGNTRU_KEM_KEYPAIR *kpair = NULL;
    const struct engntru_kem_nid_data_st *nid_data = engntru_kem_get_nid_data(nid);

    (void)ctx;

    kpair = engntru_kem_keypair_new(nid);
    if (kpair != NULL
            && NULL == (kpair->sk = OPENSSL_secure_zalloc(nid_data->sk_bytes))) {
        ENGNTRUerr(ENGNTRU_F_GENERIC_KEM_KEYGEN, ERR_R_MALLOC_FAILURE);
        goto err;
    }

    if (!engntru_prov_kem_keygen(kpair)) {
        ENGNTRUerr(ENGNTRU_F_GENERIC_KEM_KEYGEN, ENGNTRU_R_PROV_KEYGEN_FAILED);
        goto err;
    }

    kpair->ready_for_derive = 0;

    EVP_PKEY_assign(pkey, nid, kpair);
    return 1;
err:
    if (kpair != NULL)
        engntru_kem_keypair_free(kpair);
    return 0;
}

static
int generic_kem_encrypt(EVP_PKEY_CTX *ctx,
                        unsigned char *out, size_t *outlen,
                        const unsigned char *in, size_t inlen)
{
    EVP_PKEY *pkey = NULL;
    ENGNTRU_KEM_KEYPAIR *key = NULL;
    const struct engntru_kem_nid_data_st *nid_data = NULL;

    if (NULL == (pkey = EVP_PKEY_CTX_get0_pkey(ctx))) {
        ENGNTRUerr(ENGNTRU_F_GENERIC_KEM_ENCRYPT, ENGNTRU_R_KEYS_NOT_SET);
        return 0;
    }

    key = EVP_PKEY_get0(pkey);
    if (engntru_kem_keypair_is_invalid(key)) {
        ENGNTRUerr(ENGNTRU_F_GENERIC_KEM_ENCRYPT, ENGNTRU_R_INVALID_KEY);
        goto err;
    }
    nid_data = key->nid_data;

    if (out == NULL && outlen != NULL) {
        /* return the cipher text length */
        *outlen = nid_data->ct_bytes;
        return 1;
    }

    if (in == NULL && inlen == 0 && out != NULL && outlen != NULL) {
        /* no input, but ct out requested: do KEM encapsulation */
        *outlen = nid_data->ct_bytes;
        if (!engntru_prov_kem_encapsulate(key, out, key->ss)) {
            ENGNTRUerr(ENGNTRU_F_GENERIC_KEM_ENCRYPT, ENGNTRU_R_PROV_ENCAPSULATE_FAILED);
            goto err;
        }

        key->ready_for_derive = 1;
        return 1;
    }

 err:
    if (key != NULL) {
        key->ready_for_derive = 0;
    }
    return 0;
}

static
int generic_kem_decrypt(EVP_PKEY_CTX *ctx,
                        unsigned char *out, size_t *outlen,
                        const unsigned char *in, size_t inlen)
{
    EVP_PKEY *pkey = NULL;
    ENGNTRU_KEM_KEYPAIR *key = NULL;
    const struct engntru_kem_nid_data_st *nid_data = NULL;

    if (NULL == (pkey = EVP_PKEY_CTX_get0_pkey(ctx))) {
        ENGNTRUerr(ENGNTRU_F_GENERIC_KEM_DECRYPT, ENGNTRU_R_KEYS_NOT_SET);
        return 0;
    }

    key = EVP_PKEY_get0(pkey);
    if (engntru_kem_keypair_is_invalid_private(key)) {
        ENGNTRUerr(ENGNTRU_F_GENERIC_KEM_DECRYPT, ENGNTRU_R_INVALID_PRIVATE_KEY);
        goto err;
    }
    nid_data = key->nid_data;

    if (out == NULL && outlen == NULL && in != NULL && inlen == nid_data->ct_bytes) {
        /* we have input ciphertext of proper size, but no output buffer: do KEM decapsulation */
        if (!engntru_prov_kem_decapsulate(key, in, key->ss)) {
            ENGNTRUerr(ENGNTRU_F_GENERIC_KEM_DECRYPT, ENGNTRU_R_PROV_DECAPSULATE_FAILED);
            goto err;
        }

        key->ready_for_derive = 1;
        return 1;
    }

 err:
    if (key != NULL) {
        key->ready_for_derive = 0;
    }
    return 0;
}

static
int generic_kem_derive(EVP_PKEY_CTX *ctx, unsigned char *key, size_t *keylen)
{
    size_t len;
    EVP_PKEY *_pkey, *_peerkey;
    ENGNTRU_KEM_KEYPAIR *pkey, *peerkey;
    const struct engntru_kem_nid_data_st *nid_data = NULL;

    _pkey = EVP_PKEY_CTX_get0_pkey(ctx);
    _peerkey = EVP_PKEY_CTX_get0_peerkey(ctx);

    if (_pkey == NULL || _peerkey == NULL) {
        ENGNTRUerr(ENGNTRU_F_GENERIC_KEM_DERIVE, ENGNTRU_R_KEYS_NOT_SET);
        return 0;
    }
    pkey = EVP_PKEY_get0(_pkey);
    peerkey = EVP_PKEY_get0(_peerkey);
#if 0
    if (engntru_kem_keypair_is_invalid_private(pkey)) {
        ENGNTRUerr(ENGNTRU_F_GENERIC_KEM_DERIVE, ENGNTRU_R_INVALID_PRIVATE_KEY);
        return 0;
    }
#else
    /* As a hack for KEM, we allow pkey to not contain a private key.
     * We relay on checking the internal flag `read_for_derive` instead */
    if (engntru_kem_keypair_is_invalid(pkey)) {
        ENGNTRUerr(ENGNTRU_F_GENERIC_KEM_DERIVE, ENGNTRU_R_INVALID_PRIVATE_KEY);
        return 0;
    }
#endif
    if (engntru_kem_keypair_is_invalid(peerkey) || pkey->nid != peerkey->nid) {
        ENGNTRUerr(ENGNTRU_F_GENERIC_KEM_DERIVE, ENGNTRU_R_INVALID_PEER_KEY);
        return 0;
    }
    nid_data = pkey->nid_data;
    len = nid_data->ss_bytes;
    *keylen = len;

    if (key != NULL) {
        if (pkey->ss == NULL || !(pkey->ready_for_derive)) {
            ENGNTRUerr(ENGNTRU_F_GENERIC_KEM_DERIVE, ENGNTRU_R_INVALID_DERIVE);
            return 0;
        }
        memcpy(key, pkey->ss, len);
        OPENSSL_cleanse(pkey->ss, len);
        pkey->ready_for_derive = 0;
    }

    return 1;
}

static
int pmeth_generic_kem_ctrl(EVP_PKEY_CTX *ctx, int type, int p1, void *p2)
{
    (void)ctx; (void)p1; (void)p2;
    /* Only need to handle peer key for derivation */
    if (type == EVP_PKEY_CTRL_PEER_KEY)
        return 1;
    return -2;
}

static
int sntrup761_keygen(EVP_PKEY_CTX *ctx, EVP_PKEY *pkey)
{
    return generic_kem_keygen(ctx, pkey, NID_SNTRUP761);
}

void engntru_register_sntrup761(EVP_PKEY_METHOD *pmeth)
{
    EVP_PKEY_meth_set_keygen(pmeth, NULL, sntrup761_keygen);
    EVP_PKEY_meth_set_encrypt(pmeth, NULL, generic_kem_encrypt);
    EVP_PKEY_meth_set_decrypt(pmeth, NULL, generic_kem_decrypt);
    EVP_PKEY_meth_set_derive(pmeth, NULL, generic_kem_derive);
    EVP_PKEY_meth_set_ctrl(pmeth, pmeth_generic_kem_ctrl, NULL);
}

static
int sntrup857_keygen(EVP_PKEY_CTX *ctx, EVP_PKEY *pkey)
{
    return generic_kem_keygen(ctx, pkey, NID_SNTRUP857);
}

void engntru_register_sntrup857(EVP_PKEY_METHOD *pmeth)
{
    EVP_PKEY_meth_set_keygen(pmeth, NULL, sntrup857_keygen);
    EVP_PKEY_meth_set_encrypt(pmeth, NULL, generic_kem_encrypt);
    EVP_PKEY_meth_set_decrypt(pmeth, NULL, generic_kem_decrypt);
    EVP_PKEY_meth_set_derive(pmeth, NULL, generic_kem_derive);
    EVP_PKEY_meth_set_ctrl(pmeth, pmeth_generic_kem_ctrl, NULL);
}

static //Added
int sikep434_keygen(EVP_PKEY_CTX *ctx, EVP_PKEY *pkey)
{
    return generic_kem_keygen(ctx, pkey, NID_SIKEP434);
}

void engntru_register_sikep434(EVP_PKEY_METHOD *pmeth)
{
    EVP_PKEY_meth_set_keygen(pmeth, NULL, sikep434_keygen);
    EVP_PKEY_meth_set_encrypt(pmeth, NULL, generic_kem_encrypt);
    EVP_PKEY_meth_set_decrypt(pmeth, NULL, generic_kem_decrypt);
    EVP_PKEY_meth_set_derive(pmeth, NULL, generic_kem_derive);
    EVP_PKEY_meth_set_ctrl(pmeth, pmeth_generic_kem_ctrl, NULL);
}
