/*
 *  engNTRU - An engine for batch NTRU Prime PQC in OpenSSL.
 *  Copyright (C) 2019 Tampere University Foundation sr
 *
 *  This file is part of engNTRU.
 *
 *  engNTRU is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or (at your
 *  option) any later version.
 *
 *  engNTRU is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ENGNTRU_KEM_KEYPAIR_H
#define ENGNTRU_KEM_KEYPAIR_H

#include <openssl/obj_mac.h>

#include <stdint.h> /* uint8_t */
#include <stddef.h> /* size_t */

struct engntru_kem_nid_data_st {
    int nid;
    const char *name;
    size_t sk_bytes; /* length of secret key */
    size_t pk_bytes; /* length of public key */
    size_t ct_bytes; /* length of ciphertext */
    size_t ss_bytes; /* length of shared secret */

    /*
     * The "bits" value should be the cryptographic length of the
     * cryptosystem to which the key belongs, in bits.
     *
     * The definition of cryptographic length is specific to the key
     * cryptosystem. (E.g. typical values for RSA and DSA are 2048, even
     * if derived from completely different "parameters"; for X448 it is
     * defined as 448, but ED448_BITS is 456. More details at
     * <https://github.com/openssl/openssl/pull/10778#discussion_r364525641>)
     */
    int bits;

    /*
     * The "security_bits" value should be the number of security bits
     * of the given key.
     * Bits of security is defined in SP800-57.
     */
    int security_bits;
};

typedef struct {
    const int nid;
    const struct engntru_kem_nid_data_st *nid_data;

    unsigned char *sk; /* secret key */
    unsigned char *pk; /* public key */
    unsigned char *ss; /* plaintext shared secret */

    unsigned int ready_for_derive : 1;
} ENGNTRU_KEM_KEYPAIR;

ENGNTRU_KEM_KEYPAIR *engntru_kem_keypair_new(int nid);

void engntru_kem_keypair_free(ENGNTRU_KEM_KEYPAIR *keypair);

const struct engntru_kem_nid_data_st *engntru_kem_get_nid_data(int nid);

#define _engntru_kem_keypair_is_invalid(kp, contains_private) \
    ( (kp) == NULL || ( (contains_private) && (NULL == (kp)->sk) ) || \
      NULL == (kp)->nid_data )

#define engntru_kem_keypair_is_invalid(kp) \
    _engntru_kem_keypair_is_invalid((kp), 0)
#define engntru_kem_keypair_is_invalid_private(kp) \
    _engntru_kem_keypair_is_invalid((kp), 1)
#define engntru_kem_keypair_is_invalid_public(kp) \
    engntru_kem_keypair_is_invalid((kp))

#endif /* ENGNTRU_KEM_KEYPAIR_H */
