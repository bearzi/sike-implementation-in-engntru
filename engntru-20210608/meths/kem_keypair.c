/*
 *  engNTRU - An engine for batch NTRU Prime PQC in OpenSSL.
 *  Copyright (C) 2019 Tampere University Foundation sr
 *
 *  This file is part of engNTRU.
 *
 *  engNTRU is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or (at your
 *  option) any later version.
 *
 *  engNTRU is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "meths/kem_keypair.h"
#include "ossl/engntru_objects.h"

#include <openssl/crypto.h>
#include <openssl/err.h>
#include "ossl/engntru_err.h"
#include "ossl/ossl_compat.h"

#include "meths/internal/sntrup761.h"
#include "meths/internal/sntrup857.h"
#include "meths/internal/sikep434.h" //Added

inline
const struct engntru_kem_nid_data_st *engntru_kem_get_nid_data(int nid)
{
    if (nid == NID_SNTRUP761) {
        return sntrup761_data();
    } else if (nid == NID_SNTRUP857) {
        return sntrup857_data();
    } else if (nid== NID_SIKEP434) { //Added
        return sikep434_data();
    }
    return NULL;
}


ENGNTRU_KEM_KEYPAIR *engntru_kem_keypair_new(int nid)
{
    ENGNTRU_KEM_KEYPAIR *kpair = NULL;
    const struct engntru_kem_nid_data_st *nid_data = engntru_kem_get_nid_data(nid);

    if (nid_data == NULL)
        return NULL;

    if (NULL == (kpair = OPENSSL_secure_zalloc(sizeof(*kpair)))
            || NULL == (kpair->pk = OPENSSL_secure_zalloc(nid_data->pk_bytes))
            || NULL == (kpair->ss = OPENSSL_secure_zalloc(nid_data->ss_bytes))) {
        ENGNTRUerr(ENGNTRU_F_ENGNTRU_KEM_KEYPAIR_NEW, ERR_R_MALLOC_FAILURE);
        goto err;
    }

    *(int*)(&(kpair->nid)) = nid;
    kpair->nid_data = nid_data;

    return kpair;
err:
    if (kpair != NULL) {
        if (kpair->pk != NULL)
            OPENSSL_secure_free(kpair->pk);
        if (kpair->ss != NULL)
            OPENSSL_secure_free(kpair->ss);

        OPENSSL_secure_free(kpair);
    }

    return NULL;
}

void engntru_kem_keypair_free(ENGNTRU_KEM_KEYPAIR *keypair)
{
    const struct engntru_kem_nid_data_st *nid_data = NULL;

    if (NULL == keypair)
        return;

    nid_data = keypair->nid_data;
    if (NULL == nid_data)
        return;

    if (keypair->pk != NULL)
        OPENSSL_secure_free(keypair->pk);
    if (keypair->sk != NULL)
        OPENSSL_secure_clear_free(keypair->sk, nid_data->sk_bytes);
    if (keypair->ss != NULL)
        OPENSSL_secure_clear_free(keypair->ss, nid_data->ss_bytes);

    OPENSSL_secure_free(keypair);
}
