/*
 *  engNTRU - An engine for batch NTRU Prime PQC in OpenSSL.
 *  Copyright (C) 2019 Tampere University Foundation sr
 *
 *  This file is part of engNTRU.
 *
 *  engNTRU is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or (at your
 *  option) any later version.
 *
 *  engNTRU is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "meths/engntru_asn1_meth.h"

#include "meths/kem_keypair.h"

#include "ossl/engntru_objects.h"
#include <openssl/err.h>
#include "ossl/engntru_err.h"
#include <openssl/x509.h>

#include "ossl/ossl_compat.h"

#include <string.h>

#include "debug/debug.h"

#ifndef OPENSSL_V102_COMPAT
#define RC_CONST const
#else
#define RC_CONST
#endif

#define ATTR_F_UNUSED __attribute__((unused))

#define ENGNTRU_EVP_PKEY_ASN1_FLAGS 0

// ----- ACTUAL STATIC IMPLEMENTATIONS --- {{{

// ----- GENERIC FUNCTIONS             --- {{{

// ----- GENERIC PRINT FUNCTIONS --- {{{
typedef enum {
    ENGNTRU_PUBLIC,
    ENGNTRU_PRIVATE
} engntru_key_op_t;

static
int engntru_key_print(BIO *bp, const EVP_PKEY *pkey,
                      int indent, ASN1_PCTX *ctx, engntru_key_op_t op)
{
    const ENGNTRU_KEM_KEYPAIR *kpair = NULL;
    const struct engntru_kem_nid_data_st *nid_data = NULL;

    (void) ctx;

    if (pkey == NULL)
        return 0;

    kpair = EVP_PKEY_get0(pkey);

    if (op == ENGNTRU_PRIVATE) {
        if (engntru_kem_keypair_is_invalid_private(kpair)) {
            if (BIO_printf(bp, "%*s<INVALID PRIVATE KEY>\n", indent, "") <= 0)
                return 0;
            return 1;
        }
        nid_data = kpair->nid_data;
        if (BIO_printf(bp, "%*s%s Private-Key:\n", indent, "", nid_data->name) <= 0)
            return 0;
        if (BIO_printf(bp, "%*spriv:\n", indent, "") <= 0)
            return 0;
        if (ASN1_buf_print(bp, kpair->sk, nid_data->sk_bytes, indent + 4) == 0)
            return 0;
    } else {
        if (engntru_kem_keypair_is_invalid(kpair)) {
            if (BIO_printf(bp, "%*s<INVALID PUBLIC KEY>\n", indent, "") <= 0)
                return 0;
            return 1;
        }
        nid_data = kpair->nid_data;
        if (BIO_printf(bp, "%*s%s Public-Key:\n", indent, "", nid_data->name) <= 0)
            return 0;
    }
    if (BIO_printf(bp, "%*spub:\n", indent, "") <= 0)
        return 0;
    if (ASN1_buf_print(bp, kpair->pk, nid_data->pk_bytes, indent + 4) == 0)
        return 0;
    return 1;
}

static
int asn1_generic_priv_print(BIO *bp, const EVP_PKEY *pkey, int indent, ASN1_PCTX *ctx)
{
    return engntru_key_print(bp, pkey, indent, ctx, ENGNTRU_PRIVATE);
}

static
int asn1_generic_pub_print(BIO *bp, const EVP_PKEY *pkey, int indent, ASN1_PCTX *ctx)
{
    return engntru_key_print(bp, pkey, indent, ctx, ENGNTRU_PUBLIC);
}

// }}} ----- ABSTRACT PRINT FUNCTIONS

// ----- GENERIC UTILITY FUNCTIONS --- {{{
static
void engntru_pkey_free(EVP_PKEY *pkey)
{
    if (pkey != NULL) {
        ENGNTRU_KEM_KEYPAIR *kp = EVP_PKEY_get0(pkey);
        engntru_kem_keypair_free(kp);
    }
}

/*
 * The function EVP_PKEY_cmp_parameters() and EVP_PKEY_cmp() return 1 if the
 * keys match, 0 if they don't match, -1 if the key types are different and -2
 * if the operation is not supported.
 */
static
int engntru_cmp_parameters(const EVP_PKEY *a, const EVP_PKEY *b)
{
    const ENGNTRU_KEM_KEYPAIR *akey = NULL, *bkey = NULL;

    if (a == NULL || b == NULL)
        return -2;

    if (EVP_PKEY_base_id(a) != EVP_PKEY_base_id(b))
        return -1;

    akey = EVP_PKEY_get0(a);
    bkey = EVP_PKEY_get0(b);

    if (engntru_kem_keypair_is_invalid(akey) || engntru_kem_keypair_is_invalid(bkey) )
        return -2;
    if (EVP_PKEY_base_id(a) != akey->nid)
        return -2;

    if (akey->nid != bkey->nid)
        return 0;

    return 1;
}

/*
 * The function EVP_PKEY_cmp_parameters() and EVP_PKEY_cmp() return 1 if the
 * keys match, 0 if they don't match, -1 if the key types are different and -2
 * if the operation is not supported.
 */
static ATTR_F_UNUSED
int engntru_pub_cmp(const EVP_PKEY *a, const EVP_PKEY *b)
{
    const ENGNTRU_KEM_KEYPAIR *akey = NULL, *bkey = NULL;
    const struct engntru_kem_nid_data_st *adata = NULL;
    int ret;

    ret = engntru_cmp_parameters(a, b);
    switch(ret) {
        case -2:
        case -1:
            return ret;
        case 0:
            return -1;
    }
    if (ret != 1) {
        /* this should never happen */
        return -2;
    }

    akey = EVP_PKEY_get0(a);
    bkey = EVP_PKEY_get0(b);

    adata = akey->nid_data;
    return CRYPTO_memcmp(akey->pk, bkey->pk, adata->pk_bytes) == 0 ? 1 : 0;
}


// }}} ----- GENERIC UTILITY FUNCTIONS

// ----- GENERIC ABSTRACT DECODE/ENCODE/CTRL FUNCTIONS --- {{{

static
int asn1_generic_ctrl(int nid, EVP_PKEY *pkey, int op, long arg1, void *arg2)
{
    ENGNTRU_KEM_KEYPAIR *kp = NULL;
    const unsigned char *p = NULL;
    const struct engntru_kem_nid_data_st *nid_data = engntru_kem_get_nid_data(nid);
    size_t pklen = 0;

    switch (op) {

    case ASN1_PKEY_CTRL_SET1_TLS_ENCPT:
        debug("nid: %d, op: ASN1_PKEY_CTRL_SET1_TLS_ENCPT, pklen: %ld\n", nid, arg1);
        p = arg2;
        if (arg1 < 0)
            return 0;
        pklen = arg1;

        if (p == NULL || pklen != nid_data->pk_bytes ) {
            ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_CTRL, ENGNTRU_R_WRONG_LENGTH);
            return 0;
        }

        kp = engntru_kem_keypair_new(nid);
        if (engntru_kem_keypair_is_invalid(kp)) {
            return 0;
        }

        memcpy(kp->pk, p, pklen);

        return EVP_PKEY_assign(pkey, nid, kp);


    case ASN1_PKEY_CTRL_GET1_TLS_ENCPT:
        debug("nid: %d, op: ASN1_PKEY_CTRL_GET1_TLS_ENCPT\n", nid);
        if (pkey == NULL)
            return 0;
        kp = EVP_PKEY_get0(pkey);
        if (engntru_kem_keypair_is_invalid(kp) || nid != kp->nid) {
            return 0;
        } else {
            unsigned char **ppt = arg2;
            *ppt = OPENSSL_memdup(kp->pk, nid_data->pk_bytes);
            return (*ppt == NULL) ? 0 : nid_data->pk_bytes;
        }


    default:
        return -2;

    }
}


static
int asn1_generic_priv_encode(int nid, PKCS8_PRIV_KEY_INFO *p8, const EVP_PKEY *pkey)
{
    const ENGNTRU_KEM_KEYPAIR *kp = NULL;
    ASN1_OCTET_STRING oct;
    unsigned char *penc = NULL;
    int penclen;
    char *tmp_buf = NULL;
    int ret = 0;
    const struct engntru_kem_nid_data_st *nid_data = engntru_kem_get_nid_data(nid);

    if (pkey == NULL)
        return 0;
    kp = EVP_PKEY_get0(pkey);

    if (nid_data == NULL) {
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PRIV_ENCODE, ENGNTRU_R_MISSING_NID_DATA);
        return 0;
    }

    if (engntru_kem_keypair_is_invalid(kp) || kp->nid != nid) {
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PRIV_ENCODE, ENGNTRU_R_INVALID_PRIVATE_KEY);
        return 0;
    }

    tmp_buf = OPENSSL_secure_malloc(nid_data->sk_bytes);
    if (NULL == tmp_buf) {
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PRIV_ENCODE, ERR_R_MALLOC_FAILURE);
        return 0;
    }

    oct.data = memcpy(tmp_buf, kp->sk, nid_data->sk_bytes);
    oct.length = nid_data->sk_bytes;
    oct.flags = 0;

    penclen = i2d_ASN1_OCTET_STRING(&oct, &penc);
    if (penclen < 0) {
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PRIV_ENCODE, ERR_R_MALLOC_FAILURE);
        ret = 0;
        goto err;
    }

    if (!PKCS8_pkey_set0(p8, OBJ_nid2obj(nid), 0,
                         V_ASN1_UNDEF, NULL, penc, penclen)) {
        OPENSSL_clear_free(penc, penclen);
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PRIV_ENCODE, ERR_R_MALLOC_FAILURE);
        ret = 0;
        goto err;
    }

    ret = 1;
err:
    if (tmp_buf != NULL)
        OPENSSL_secure_free(tmp_buf);
    return ret;
}

static
int asn1_generic_priv_decode(int nid, EVP_PKEY *pkey, RC_CONST PKCS8_PRIV_KEY_INFO *p8)
{
    const unsigned char *p;
    int plen;
    ASN1_OCTET_STRING *oct = NULL;
    RC_CONST X509_ALGOR *palg;
    ENGNTRU_KEM_KEYPAIR *kp = NULL;
    int ret = 0;
    const struct engntru_kem_nid_data_st *nid_data = engntru_kem_get_nid_data(nid);

    if (nid_data == NULL) {
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PRIV_DECODE, ENGNTRU_R_MISSING_NID_DATA);
        return 0;
    }

    if (!PKCS8_pkey_get0(NULL, &p, &plen, &palg, p8))
        return 0;

    oct = d2i_ASN1_OCTET_STRING(NULL, &p, plen);
    if (oct == NULL) {
        p = NULL;
        plen = 0;
    } else {
        p = ASN1_STRING_get0_data(oct);
        plen = ASN1_STRING_length(oct);
    }

    if (palg != NULL) {
        int ptype;

        /* Algorithm parameters must be absent */
        X509_ALGOR_get0(NULL, &ptype, NULL, palg);
        if (ptype != V_ASN1_UNDEF) {
            ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PRIV_DECODE, ENGNTRU_R_INVALID_ENCODING);
            goto err;
        }
    }

    if (p == NULL || plen < 0 ||  (size_t)plen != nid_data->sk_bytes) {
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PRIV_DECODE, ENGNTRU_R_WRONG_LENGTH);
        goto err;
    }

    kp = engntru_kem_keypair_new(nid);
    if (engntru_kem_keypair_is_invalid(kp)){
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PRIV_DECODE, ENGNTRU_R_INVALID_PRIVATE_KEY);
        goto err;
    }
    kp->sk = OPENSSL_memdup(p, nid_data->pk_bytes);
    if (engntru_kem_keypair_is_invalid_private(kp)){
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PRIV_DECODE, ENGNTRU_R_INVALID_PRIVATE_KEY);
        goto err;
    }

    // Generate corresponding public key
    if ( 1 ) {
        // TODO: this is not supported yet
        engntru_kem_keypair_free(kp);
        ret = -2;
        goto err;
    }

    ret = EVP_PKEY_assign(pkey, nid, kp);

 err:
    if (oct != NULL)
        ASN1_OCTET_STRING_free(oct);
    return ret;
}

static
int asn1_generic_pub_encode(int nid, X509_PUBKEY *pk, const EVP_PKEY *pkey)
{
    const ENGNTRU_KEM_KEYPAIR *kp = NULL;
    unsigned char *penc = NULL;
    const struct engntru_kem_nid_data_st *nid_data = engntru_kem_get_nid_data(nid);

    if (pkey == NULL)
        return 0;

    kp = EVP_PKEY_get0(pkey);

    if (engntru_kem_keypair_is_invalid(kp) || kp->nid != nid) {
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PUB_ENCODE, ENGNTRU_R_INVALID_KEY);
        return 0;
    }

    if (nid_data == NULL) {
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PUB_ENCODE, ENGNTRU_R_MISSING_NID_DATA);
        return 0;
    }

    penc = OPENSSL_memdup(kp->pk, nid_data->pk_bytes);
    if (penc == NULL) {
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PUB_ENCODE, ERR_R_MALLOC_FAILURE);
        return 0;
    }

    if (!X509_PUBKEY_set0_param(pk, OBJ_nid2obj(nid), V_ASN1_UNDEF,
                                NULL, penc, nid_data->pk_bytes)) {
        OPENSSL_free(penc);
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PUB_ENCODE, ERR_R_MALLOC_FAILURE);
        return 0;
    }
    return 1;
}

static
int asn1_generic_pub_decode(int nid, EVP_PKEY *pkey, X509_PUBKEY *pubkey)
{
    const unsigned char *p;
    int pklen;
    X509_ALGOR *palg;
    ENGNTRU_KEM_KEYPAIR *kp = NULL;
    const struct engntru_kem_nid_data_st *nid_data = engntru_kem_get_nid_data(nid);

    if (nid_data == NULL) {
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PUB_DECODE, ENGNTRU_R_MISSING_NID_DATA);
        return 0;
    }

    if (!X509_PUBKEY_get0_param(NULL, &p, &pklen, &palg, pubkey))
        return 0;

    if (palg != NULL) {
        int ptype;

        /* Algorithm parameters must be absent */
        X509_ALGOR_get0(NULL, &ptype, NULL, palg);
        if (ptype != V_ASN1_UNDEF) {
            ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PUB_DECODE, ENGNTRU_R_INVALID_ENCODING);
            return 0;
        }
    }

    if (p == NULL || pklen < 0 || (size_t)pklen != nid_data->pk_bytes) {
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PUB_DECODE, ENGNTRU_R_WRONG_LENGTH);
        return 0;
    }

    kp = engntru_kem_keypair_new(nid);
    if (engntru_kem_keypair_is_invalid(kp) ){
        ENGNTRUerr(ENGNTRU_F_ASN1_GENERIC_PUB_DECODE, ENGNTRU_R_INVALID_KEY);
        return 0;
    }

    memcpy(kp->pk, p, pklen);

    return EVP_PKEY_assign(pkey, nid, kp);
}


// }}} ----- GENERIC ABSTRACT DECODE/ENCODE/CTRL FUNCTIONS

// }}} ----- GENERIC FUNCTIONS

// --------- CONCRETE DECODE/ENCODE/CTRL/PRINT FUNCTIONS --- {{{

#define ___debug_concrete(___NAME,___NID,___STRING) \
    verbose("CALLED:\tNID(%d/%s)\t-> \"%s\"\n", ___NID, OBJ_nid2sn(___NID), ___STRING);

#define DECLARE_ENGNTRU_CONCRETE_FUNCTIONS(___NAME,___NID,___STRING) \
	static int engntru_##___NAME##_ctrl(EVP_PKEY *pkey, int op, long arg1, void *arg2)			{ ___debug_concrete(___NAME,___NID,___STRING);return asn1_generic_ctrl(___NID,pkey,op,arg1,arg2); }; \
	static ATTR_F_UNUSED int engntru_##___NAME##_priv_encode(PKCS8_PRIV_KEY_INFO *p8, const EVP_PKEY *pkey)	{ ___debug_concrete(___NAME,___NID,___STRING);return asn1_generic_priv_encode(___NID,p8,pkey); };    \
	static ATTR_F_UNUSED int engntru_##___NAME##_priv_decode(EVP_PKEY *pkey, RC_CONST PKCS8_PRIV_KEY_INFO *p8)	{ ___debug_concrete(___NAME,___NID,___STRING);return asn1_generic_priv_decode(___NID,pkey,p8); };    \
	static ATTR_F_UNUSED int engntru_##___NAME##_priv_print(BIO *bp, const EVP_PKEY *pkey, int indent, ASN1_PCTX *ctx) { ___debug_concrete(___NAME,___NID,___STRING);return asn1_generic_priv_print(bp,pkey,indent,ctx); }; \
	static ATTR_F_UNUSED int engntru_##___NAME##_pub_encode(X509_PUBKEY *pk, const EVP_PKEY *pkey)			{ ___debug_concrete(___NAME,___NID,___STRING);return asn1_generic_pub_encode(___NID,pk,pkey); };		\
	static ATTR_F_UNUSED int engntru_##___NAME##_pub_decode(EVP_PKEY *pkey, X509_PUBKEY *pubkey)				{ ___debug_concrete(___NAME,___NID,___STRING);return asn1_generic_pub_decode(___NID,pkey,pubkey); }; \
	static ATTR_F_UNUSED int engntru_##___NAME##_pub_print(BIO *bp, const EVP_PKEY *pkey, int indent, ASN1_PCTX *ctx) { ___debug_concrete(___NAME,___NID,___STRING);return asn1_generic_pub_print(bp,pkey,indent,ctx); };


DECLARE_ENGNTRU_CONCRETE_FUNCTIONS(sntrup761, NID_SNTRUP761, (OBJ_nid2sn(NID_SNTRUP761)) );
DECLARE_ENGNTRU_CONCRETE_FUNCTIONS(sntrup857, NID_SNTRUP857, (OBJ_nid2sn(NID_SNTRUP857)) );
DECLARE_ENGNTRU_CONCRETE_FUNCTIONS(sikep434, NID_SIKEP434, (OBJ_nid2sn(NID_SIKEP434))    );//Added


// }}} ----- CONCRETE DECODE/ENCODE/CTRL/PRINT FUNCTIONS

static ATTR_F_UNUSED
int engntru_generic_size(const EVP_PKEY *pkey)
{
    const ENGNTRU_KEM_KEYPAIR *kpair = NULL;

    if (pkey == NULL)
        return 0;

    kpair = EVP_PKEY_get0(pkey);

    if (kpair == NULL)
        return 0;

    return kpair->nid_data->ss_bytes;
}

static ATTR_F_UNUSED
int engntru_generic_bits(const EVP_PKEY *pkey)
{
    const ENGNTRU_KEM_KEYPAIR *kpair = NULL;

    if (pkey == NULL)
        return 0;

    kpair = EVP_PKEY_get0(pkey);

    if (kpair == NULL)
        return 0;

    return kpair->nid_data->bits;
}

static ATTR_F_UNUSED
int engntru_generic_security_bits(const EVP_PKEY *pkey)
{
    const ENGNTRU_KEM_KEYPAIR *kpair = NULL;

    if (pkey == NULL)
        return 0;

    kpair = EVP_PKEY_get0(pkey);

    if (kpair == NULL)
        return 0;

    return kpair->nid_data->security_bits;
}


// }}} ----- ACTUAL STATIC IMPLEMENTATIONS


/* Called from engntru.c:engntru_register_ameth():
 * engntru_register_asn1_meth(  NID_ED25519,
 *                            &ameth,
 *                            "ED25519",
 *                            "Ed25519 through libsodium");
 *
 * ameth is guaranteed to be non-NULL and unsupported nid are already filtered
 * out.
 */
int engntru_register_asn1_meth(int nid, EVP_PKEY_ASN1_METHOD **ameth, const char *pem_str, const char *info)
{
    int (*asn1_priv_decode_fn)(EVP_PKEY *pkey, RC_CONST PKCS8_PRIV_KEY_INFO *p8) = NULL;
    int (*asn1_priv_encode_fn)(PKCS8_PRIV_KEY_INFO *p8, const EVP_PKEY *pkey) = NULL;
    int (*asn1_priv_print_fn)(BIO *bp, const EVP_PKEY *pkey, int indent, ASN1_PCTX *ctx) = NULL;

    int (*asn1_pub_decode_fn)(EVP_PKEY *pkey, X509_PUBKEY *pubkey) = NULL;
    int (*asn1_pub_encode_fn)(X509_PUBKEY *pk, const EVP_PKEY *pkey) = NULL;
    int (*asn1_pub_print_fn)(BIO *bp, const EVP_PKEY *pkey, int indent, ASN1_PCTX *ctx) = NULL;

    int (*asn1_param_decode_fn)(EVP_PKEY *pkey, const unsigned char **pder, int derlen) = NULL;
    int (*asn1_param_encode_fn)(const EVP_PKEY *pkey, unsigned char **pder) = NULL;
    int (*asn1_param_missing_fn)(const EVP_PKEY *pk) = NULL;
    int (*asn1_param_copy_fn)(EVP_PKEY *to, const EVP_PKEY *from) = NULL;
    int (*asn1_param_print_fn)(BIO *out, const EVP_PKEY *pkey, int indent, ASN1_PCTX *pctx) = NULL;

    int (*asn1_item_verify_fn)(EVP_MD_CTX *ctx, const ASN1_ITEM *it, void *asn, X509_ALGOR *a, ASN1_BIT_STRING *sig, EVP_PKEY *pkey) = NULL;
    int (*asn1_item_sign_fn)(EVP_MD_CTX *ctx, const ASN1_ITEM *it, void *asn, X509_ALGOR *alg1, X509_ALGOR *alg2, ASN1_BIT_STRING *sig) = NULL;

    debug("REGISTER AMETH NID(%d/%s):%s:%s\n",nid,OBJ_nid2sn(nid),pem_str,info);

    if (nid != NID_SNTRUP761 && nid != NID_SNTRUP857 && nid != NID_SIKEP434) { //Added
        errorf("Unsupported NID(%d/%s)\n", nid, OBJ_nid2sn(nid));
        return 0;
    }

    *ameth = EVP_PKEY_asn1_new(nid, ENGNTRU_EVP_PKEY_ASN1_FLAGS, pem_str, info);
    if (*ameth == NULL) {
        errorf("Failure in EVP_PKEY_asn1_new()\n");
        return 0;
    }

    if (nid == NID_SNTRUP761) {
        debug("USING engntru_sntrup761_* functions\n");
        EVP_PKEY_asn1_set_ctrl(*ameth, engntru_sntrup761_ctrl);

#if 0
        /* decode, encode, print for private and public keys */
        asn1_priv_decode_fn = engntru_sntrup761_priv_decode;
        asn1_priv_encode_fn = engntru_sntrup761_priv_encode;
        asn1_priv_print_fn  = engntru_sntrup761_priv_print;
        asn1_pub_decode_fn  = engntru_sntrup761_pub_decode;
        asn1_pub_encode_fn  = engntru_sntrup761_pub_encode;
        asn1_pub_print_fn   = engntru_sntrup761_pub_print;

        /* functions to handle domain parameters for keys */
        asn1_param_decode_fn  = engntru_sntrup761_param_decode;
        asn1_param_encode_fn  = engntru_sntrup761_param_encode;
        asn1_param_missing_fn  = engntru_sntrup761_param_missing;
        asn1_param_copy_fn  = engntru_sntrup761_param_copy;
        asn1_param_print_fn   = engntru_sntrup761_param_print;

        /* for X509 certificates */
        asn1_item_verify_fn = engntru_sntrup761_item_verify;
        asn1_item_sign_fn   = engntru_sntrup761_item_sign;
#endif
    } else if (nid == NID_SNTRUP857) {
        debug("USING engntru_sntrup857_* functions\n");
        EVP_PKEY_asn1_set_ctrl(*ameth, engntru_sntrup857_ctrl);

#if 0
        /* decode, encode, print for private and public keys */
        asn1_priv_decode_fn = engntru_sntrup857_priv_decode;
        asn1_priv_encode_fn = engntru_sntrup857_priv_encode;
        asn1_priv_print_fn  = engntru_sntrup857_priv_print;
        asn1_pub_decode_fn  = engntru_sntrup857_pub_decode;
        asn1_pub_encode_fn  = engntru_sntrup857_pub_encode;
        asn1_pub_print_fn   = engntru_sntrup857_pub_print;

        /* functions to handle domain parameters for keys */
        asn1_param_decode_fn  = engntru_sntrup857_param_decode;
        asn1_param_encode_fn  = engntru_sntrup857_param_encode;
        asn1_param_missing_fn  = engntru_sntrup857_param_missing;
        asn1_param_copy_fn  = engntru_sntrup857_param_copy;
        asn1_param_print_fn   = engntru_sntrup857_param_print;

        /* for X509 certificates */
        asn1_item_verify_fn = engntru_sntrup857_item_verify;
        asn1_item_sign_fn   = engntru_sntrup857_item_sign;
#endif
} else if (nid == NID_SIKEP434) { //Added
        debug("USING sikep434 functions\n");
        // HMM what to replace with ??
        EVP_PKEY_asn1_set_ctrl(*ameth, engntru_sikep434_ctrl);

#if 0
        /* decode, encode, print for private and public keys */
        asn1_priv_decode_fn = engntru_sikep434_priv_decode;
        asn1_priv_encode_fn = engntru_sikep434_priv_encode;
        asn1_priv_print_fn  = engntru_sikep434_priv_print;
        asn1_pub_decode_fn  = engntru_sikep434_pub_decode;
        asn1_pub_encode_fn  = engntru_sikep434_pub_encode;
        asn1_pub_print_fn   = engntru_sikep434_pub_print;

        /* functions to handle domain parameters for keys */
        asn1_param_decode_fn  = engntru_sikep434_param_decode;
        asn1_param_encode_fn  = engntru_sikep434_param_encode;
        asn1_param_missing_fn  = engntru_sikep434_param_missing;
        asn1_param_copy_fn  = engntru_sikep434_param_copy;
        asn1_param_print_fn   = engntru_sikep434_param_print;

        /* for X509 certificates */
        asn1_item_verify_fn = engntru_sikep434_item_verify;
        asn1_item_sign_fn   = engntru_sikep434_item_sign;
#endif
}

    /* decode, encode, print for private and public keys */
    EVP_PKEY_asn1_set_private(*ameth,
            asn1_priv_decode_fn,
            asn1_priv_encode_fn,
            asn1_priv_print_fn);
    EVP_PKEY_asn1_set_public(*ameth,
            asn1_pub_decode_fn,
            asn1_pub_encode_fn,
            engntru_pub_cmp,
            asn1_pub_print_fn,
            engntru_generic_size,
            engntru_generic_bits);
    /* functions to handle domain parameters for keys */
    EVP_PKEY_asn1_set_param(*ameth,
            asn1_param_decode_fn,
            asn1_param_encode_fn,
            asn1_param_missing_fn,
            asn1_param_copy_fn,
            engntru_cmp_parameters,
            asn1_param_print_fn);

    /* for X509 certificates */
    EVP_PKEY_asn1_set_item(*ameth, asn1_item_verify_fn, asn1_item_sign_fn);

    EVP_PKEY_asn1_set_security_bits(*ameth, engntru_generic_security_bits);
    EVP_PKEY_asn1_set_free(*ameth, engntru_pkey_free);

    return 1;
}
