#include "meths/internal/sntrup761.h"

#include "ossl/engntru_objects.h"

#include <crypto_kem_sntrup761.h>


#define CRYPTO_SECRETKEYBYTES crypto_kem_sntrup761_SECRETKEYBYTES
#define CRYPTO_PUBLICKEYBYTES crypto_kem_sntrup761_PUBLICKEYBYTES
#define CRYPTO_CIPHERTEXTBYTES crypto_kem_sntrup761_CIPHERTEXTBYTES
#define CRYPTO_BYTES crypto_kem_sntrup761_BYTES


const struct engntru_kem_nid_data_st *sntrup761_data(void)
{
    static struct engntru_kem_nid_data_st _sntrup761_data = {
        .nid = NID_undef,
        .name = "SNTRUP761",
        .sk_bytes = CRYPTO_SECRETKEYBYTES,
        .pk_bytes = CRYPTO_PUBLICKEYBYTES,
        .ct_bytes = CRYPTO_CIPHERTEXTBYTES,
        .ss_bytes = CRYPTO_BYTES,

        .bits          = 8 * CRYPTO_PUBLICKEYBYTES,
        .security_bits = 153
    };

    if (_sntrup761_data.nid == NID_undef)
        _sntrup761_data.nid = NID_SNTRUP761;

    return &_sntrup761_data;
}
