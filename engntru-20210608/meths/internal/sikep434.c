#include "meths/internal/sntrup761.h" // Added

#include "ossl/engntru_objects.h"

#include <crypto_kem_sntrup761.h>

// To link to the library liboqs
#include <oqs/oqs.h>

#define CRYPTO_SECRETKEYBYTES OQS_KEM_sike_p434_length_secret_key
#define CRYPTO_PUBLICKEYBYTES OQS_KEM_sike_p434_length_public_key
#define CRYPTO_CIPHERTEXTBYTES OQS_KEM_sike_p434_length_ciphertext
#define CRYPTO_BYTES OQS_KEM_sike_p434_length_shared_secret


const struct engntru_kem_nid_data_st *sikep434_data(void)
{
    static struct engntru_kem_nid_data_st _sikep434_data = {
        .nid = NID_undef,
        .name = "SIKEP434",
        .sk_bytes = CRYPTO_SECRETKEYBYTES,
        .pk_bytes = CRYPTO_PUBLICKEYBYTES,
        .ct_bytes = CRYPTO_CIPHERTEXTBYTES,
        .ss_bytes = CRYPTO_BYTES,

        .bits          = 8 * CRYPTO_PUBLICKEYBYTES,
        .security_bits = 128
    };

    if (_sikep434_data.nid == NID_undef)
        _sikep434_data.nid = NID_SIKEP434;

    return &_sikep434_data;
}
