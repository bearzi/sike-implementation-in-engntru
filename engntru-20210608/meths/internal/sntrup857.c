#include "meths/internal/sntrup857.h"

#include "ossl/engntru_objects.h"

#include <crypto_kem_sntrup857.h>

#define CRYPTO_SECRETKEYBYTES crypto_kem_sntrup857_SECRETKEYBYTES
#define CRYPTO_PUBLICKEYBYTES crypto_kem_sntrup857_PUBLICKEYBYTES
#define CRYPTO_CIPHERTEXTBYTES crypto_kem_sntrup857_CIPHERTEXTBYTES
#define CRYPTO_BYTES crypto_kem_sntrup857_BYTES


const struct engntru_kem_nid_data_st *sntrup857_data(void)
{
    static struct engntru_kem_nid_data_st _sntrup857_data = {
        .nid = NID_undef,
        .name = "SNTRUP857",
        .sk_bytes = CRYPTO_SECRETKEYBYTES,
        .pk_bytes = CRYPTO_PUBLICKEYBYTES,
        .ct_bytes = CRYPTO_CIPHERTEXTBYTES,
        .ss_bytes = CRYPTO_BYTES,

        .bits          = 8 * CRYPTO_PUBLICKEYBYTES,
        .security_bits = 175
    };

    if (_sntrup857_data.nid == NID_undef)
        _sntrup857_data.nid = NID_SNTRUP857;

    return &_sntrup857_data;
}
