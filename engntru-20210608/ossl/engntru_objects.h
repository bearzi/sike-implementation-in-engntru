/*
 *  engNTRU - An engine for batch NTRU Prime PQC in OpenSSL.
 *  Copyright (C) 2019 Tampere University Foundation sr
 *
 *  This file is part of engNTRU.
 *
 *  engNTRU is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or (at your
 *  option) any later version.
 *
 *  engNTRU is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ENGNTRU_OBJECTS_H
#define ENGNTRU_OBJECTS_H

#include <openssl/objects.h>

#ifdef NID_SNTRUP761
#undef NID_SNTRUP761
#endif /* NID_SNTRUP761 */

extern int NID_SNTRUP761;

#ifdef NID_SNTRUP857
#undef NID_SNTRUP857
#endif /* NID_SNTRUP857 */

extern int NID_SNTRUP857;

#ifdef NID_SIKEP434
#undef NID_SIKEP434
#endif  /* NID_SIKEP434 added*/

extern int NID_SIKEP434;

int engntru_register_nids();

#endif /* ENGNTRU_OBJECTS_H */
