/*
 *  engNTRU - An engine for batch NTRU Prime PQC in OpenSSL.
 *  Copyright (C) 2019 Tampere University Foundation sr
 *
 *  This file is part of engNTRU.
 *
 *  engNTRU is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or (at your
 *  option) any later version.
 *
 *  engNTRU is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ENGNTRU_OBJECTS_INTERNAL_H
#define ENGNTRU_OBJECTS_INTERNAL_H

#define engntru_OID_SNTRUP761   "1.3.6.1.4.1.50263.0.3.1.20"
#define engntru_SN_SNTRUP761    "SNTRUP761"
#define engntru_LN_SNTRUP761    "SNTRUP761 KEM"

#define engntru_OID_SNTRUP857   "1.3.6.1.4.1.50263.0.3.1.21"
#define engntru_SN_SNTRUP857    "SNTRUP857"
#define engntru_LN_SNTRUP857    "SNTRUP857 KEM"

#define engntru_OID_SIKEP434    "1.3.6.1.4.1.50263.0.3.1.22" //Added
#define engntru_SN_SIKEP434     "SIKEP434"
#define engntru_LN_SIKEP434     "SIKEP434 KEM"


#endif /* ENGNTRU_OBJECTS_INTERNAL_H */
