/*
 *  engNTRU - An engine for batch NTRU Prime PQC in OpenSSL.
 *  Copyright (C) 2019 Tampere University Foundation sr
 *
 *  This file is part of engNTRU.
 *
 *  engNTRU is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or (at your
 *  option) any later version.
 *
 *  engNTRU is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "engntru_objects.h"
#include "debug/debug.h"

#include "engntru_objects_internal.h"

int NID_SNTRUP761;
int NID_SNTRUP857;
int NID_SIKEP434;

static int _engntru_register_nid(const char *oid_str, const char *sn, const char *ln) {
    int new_nid = NID_undef;

    if (NID_undef != (new_nid = OBJ_sn2nid(sn)) ) {
        debug("'%s' is already registered with NID %d\n", sn, new_nid);
        return new_nid;
    }

    new_nid = OBJ_create(oid_str, sn, ln);

    if (new_nid == NID_undef) {
        fatalf("Failed to register NID for '%s'\n", ln);
        return 0;
    }
    debug("Registered '%s' with NID %d\n", sn, new_nid);

    ASN1_OBJECT *obj = OBJ_nid2obj(new_nid);
    if ( !obj ) {
        fatalf("Failed to retrieve ASN1_OBJECT for dinamically registered NID\n");
        return 0;
    }

    return new_nid;
}

#define ENGNTRU_REGISTER_NID(___BASENAME) \
    if ( NID_undef == (NID_##___BASENAME = _engntru_register_nid(engntru_OID_##___BASENAME, \
                                                                 engntru_SN_##___BASENAME , \
                                                                 engntru_LN_##___BASENAME ))\
       ) { \
        errorf("Failed to register NID for '%s'\n", engntru_SN_##___BASENAME ); \
        return 0; \
    }



int engntru_register_nids()
{
    ENGNTRU_REGISTER_NID(SNTRUP761);
    ENGNTRU_REGISTER_NID(SNTRUP857);
    ENGNTRU_REGISTER_NID(SIKEP434);
    return 1;
}
