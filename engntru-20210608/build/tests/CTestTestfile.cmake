# CMake generated Testfile for 
# Source directory: /home/opensslntru/engntru-20210608/tests
# Build directory: /home/opensslntru/engntru-20210608/build/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(load_engine "/bin/openssl" "engine" "-vvv" "-t" "engntru")
set_tests_properties(load_engine PROPERTIES  ENVIRONMENT "OPENSSL_ENGINES=/home/opensslntru/engntru-20210608/build" _BACKTRACE_TRIPLES "/home/opensslntru/engntru-20210608/tests/CMakeLists.txt;6;add_test;/home/opensslntru/engntru-20210608/tests/CMakeLists.txt;0;")
add_test(create_unrelated_key "/bin/openssl" "genpkey" "-algorithm" "EC" "-pkeyopt" "ec_paramgen_curve:prime256v1")
set_tests_properties(create_unrelated_key PROPERTIES  ENVIRONMENT "OPENSSL_CONF=/home/opensslntru/engntru-20210608/tests/engntru.cnf;OPENSSL_ENGINES=/home/opensslntru/engntru-20210608/build;ENGNTRU_DEBUG=-9" _BACKTRACE_TRIPLES "/home/opensslntru/engntru-20210608/tests/CMakeLists.txt;11;add_test;/home/opensslntru/engntru-20210608/tests/CMakeLists.txt;0;")
