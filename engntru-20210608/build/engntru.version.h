#define ENGNTRU__DESC_STR "An engine for batch NTRU Prime PQC in OpenSSL"

#define ENGNTRU__VERSION_STR "0.1.3"
#define ENGNTRU__PROVIDER_STR "async_batch_lib"
#define ENGNTRU__BUILD_STR "Debug"
#define ENGNTRU__BUILD_TIMESTAMP_STR "2022-01-06T21:10:20Z"

#define ENGNTRU_ENGINE_NAME \
    ENGNTRU__DESC_STR \
    " (" \
        "v" ENGNTRU__VERSION_STR \
        ", provider: " ENGNTRU__PROVIDER_STR \
        ", build: " ENGNTRU__BUILD_STR \
        ", built on: " ENGNTRU__BUILD_TIMESTAMP_STR \
    ")"
