# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/opensslntru/engntru-20210608/debug/debug.c" "/home/opensslntru/engntru-20210608/build/CMakeFiles/engntru.dir/debug/debug.c.o"
  "/home/opensslntru/engntru-20210608/engntru.c" "/home/opensslntru/engntru-20210608/build/CMakeFiles/engntru.dir/engntru.c.o"
  "/home/opensslntru/engntru-20210608/meths/engntru_asn1_meth.c" "/home/opensslntru/engntru-20210608/build/CMakeFiles/engntru.dir/meths/engntru_asn1_meth.c.o"
  "/home/opensslntru/engntru-20210608/meths/engntru_pmeth_sntrup.c" "/home/opensslntru/engntru-20210608/build/CMakeFiles/engntru.dir/meths/engntru_pmeth_sntrup.c.o"
  "/home/opensslntru/engntru-20210608/meths/kem_keypair.c" "/home/opensslntru/engntru-20210608/build/CMakeFiles/engntru.dir/meths/kem_keypair.c.o"
  "/home/opensslntru/engntru-20210608/ossl/engntru_err.c" "/home/opensslntru/engntru-20210608/build/CMakeFiles/engntru.dir/ossl/engntru_err.c.o"
  "/home/opensslntru/engntru-20210608/ossl/engntru_objects.c" "/home/opensslntru/engntru-20210608/build/CMakeFiles/engntru.dir/ossl/engntru_objects.c.o"
  "/home/opensslntru/engntru-20210608/ossl/ossl_compat.c" "/home/opensslntru/engntru-20210608/build/CMakeFiles/engntru.dir/ossl/ossl_compat.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BATCH_SIZE="
  "DEBUG_COLORS"
  "_DEBUG"
  "engntru_EXPORTS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../"
  "."
  "/usr/local/include/oqs"
  "/home/opensslntru/local/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/opensslntru/engntru-20210608/build/meths/internal/CMakeFiles/internal_data.dir/DependInfo.cmake"
  "/home/opensslntru/engntru-20210608/build/providers/async_batch_lib/CMakeFiles/async_batch_lib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
