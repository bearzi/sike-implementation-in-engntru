# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/opensslntru/engntru-20210608/providers/serial_lib/base.c" "/home/opensslntru/engntru-20210608/build/providers/serial_lib/CMakeFiles/serial_lib.dir/base.c.o"
  "/home/opensslntru/engntru-20210608/providers/serial_lib/kem.c" "/home/opensslntru/engntru-20210608/build/providers/serial_lib/CMakeFiles/serial_lib.dir/kem.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BATCH_SIZE="
  "DEBUG_COLORS"
  "_DEBUG"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "providers/serial_lib"
  "../"
  "/home/opensslntru/local/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
