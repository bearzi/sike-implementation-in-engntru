# CMake generated Testfile for 
# Source directory: /home/opensslntru/engntru-20210608
# Build directory: /home/opensslntru/engntru-20210608/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("meths/internal")
subdirs("providers/async_batch_lib")
subdirs("tests")
