/*
 *  engNTRU - An engine for batch NTRU Prime PQC in OpenSSL.
 *  Copyright (C) 2019 Tampere University Foundation sr
 *
 *  This file is part of engNTRU.
 *
 *  engNTRU is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or (at your
 *  option) any later version.
 *
 *  engNTRU is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string.h> /* memcpy() */
#include <openssl/engine.h>
#include <openssl/obj_mac.h>

#include "ossl/ossl_compat.h"

#include "ossl/engntru_objects.h"

#include "ossl/engntru_err.h"

#include "meths/engntru_pmeth_sntrup.h"
#include "meths/engntru_asn1_meth.h"

#include "debug/debug.h"

#include "providers/api/base.h"

#include "nelem.h"

#define ENGNTRU_DEBUG_ENVVAR "ENGNTRU_DEBUG"
#define ENGNTRU_DEBUG_DEFAULT_LEVEL LOG_WARN

#ifndef ENGNTRU_ENGINE_ID
#define ENGNTRU_ENGINE_ID "engNTRU"
#endif /* !defined(ENGNTRU_ENGINE_ID) */

#ifndef ENGNTRU_ENGINE_NAME
# include "engntru.version.h"
#endif /* !defined(ENGNTRU_ENGINE_NAME) */

static const char *engine_id = ENGNTRU_ENGINE_ID;
static const char *engine_name = ENGNTRU_ENGINE_NAME;

static void engntru_clear_methods(void);


static int engntru_e_init(ENGINE *e)
{
    verbose("STUB\n");
    (void)e;
    return 1;
}

static int engntru_e_finish(ENGINE *e)
{
    verbose("STUB\n");
    (void)e;
    return 1;
}

static int engntru_e_destroy(ENGINE *e)
{
    (void)e;
    verbose("CALLED\n");

    if (!engntru_implementation_deinit()) {
        errorf("engntru_implementation_deinit failed\n");
    }

    engntru_clear_methods();

    debug_logging_finish();
    ERR_unload_ENGNTRU_strings();
    OBJ_cleanup();

    return 1;
}

/* --------------- PKEY methods ---------------- */

static int engntru_pkey_meth_nids[] = {
    0 /* NID_SNTRUP761 */,
    0 /* NID_SNTRUP857 */,
    0 /* NID_SIKEP434 Added */,
    0
};

static void engntru_pkey_meth_nids_init()
{
    engntru_pkey_meth_nids[0] = NID_SNTRUP761;
    engntru_pkey_meth_nids[1] = NID_SNTRUP857;
    engntru_pkey_meth_nids[2] = NID_SIKEP434;//Added
}

static EVP_PKEY_METHOD *pmeth_sntrup761 = NULL;
static EVP_PKEY_METHOD *pmeth_sntrup857 = NULL;
static EVP_PKEY_METHOD *pmeth_sikep434  = NULL;//Added

/* --------------- ASN1 methods ---------------- */

static int engntru_pkey_asn1_meth_nids[] = {
    0 /* NID_SNTRUP761 */,
    0 /* NID_SNTRUP857 */,
    0 /* NID_SIKEP434 Added*/,
    0
};

static void engntru_pkey_asn1_meth_nids_init()
{
    engntru_pkey_asn1_meth_nids[0] = NID_SNTRUP761;
    engntru_pkey_asn1_meth_nids[1] = NID_SNTRUP857;
    engntru_pkey_asn1_meth_nids[2] = NID_SIKEP434;//Added
}

static EVP_PKEY_ASN1_METHOD *ameth_sntrup761 = NULL;
static EVP_PKEY_ASN1_METHOD *ameth_sntrup857 = NULL;
static EVP_PKEY_ASN1_METHOD *ameth_sikep434 = NULL; //Added
/* --------------------------------------------- */


static
int engntru_pkey_meths(ENGINE *e, EVP_PKEY_METHOD **pmeth, const int **nids, int nid)
{
    (void)e;
    if(!pmeth) {
        debug("GET LIST\n");
        *nids = engntru_pkey_meth_nids;
        return ENGNTRU_NELEM(engntru_pkey_meth_nids) - 1;
    }
    debug("NID(%d/%s) ->", nid, OBJ_nid2sn(nid));

    if (nid == NID_SNTRUP761) {
        *pmeth = pmeth_sntrup761;
        debug_sl("pmeth_sntrup761\n");
        return 1;
    } else if (nid == NID_SNTRUP857) {
        *pmeth = pmeth_sntrup857;
        debug_sl("pmeth_sntrup857\n");
        return 1;
    } else if (nid == NID_SIKEP434) { //Added
    	*pmeth = pmeth_sikep434;
    	debug_sl("pmeth_sikep434\n");
    	return 1;
    }

    debug_sl("NOT FOUND\n");
    *pmeth = NULL;
    return 0;
}

/* Register a new EVP_PKEY_METHOD for alg id under given flags */
static
int engntru_register_pmeth(int id, EVP_PKEY_METHOD **pmeth, int flags)
{
    if (*pmeth != NULL) {
        debug("pmeth was already registered\n");
        return 1;
    }

    *pmeth = EVP_PKEY_meth_new(id, flags);

    if (*pmeth == NULL)
        return 0;

    if (id == NID_SNTRUP761) {
        engntru_register_sntrup761(*pmeth);
    } else if (id == NID_SNTRUP857) {
        engntru_register_sntrup857(*pmeth);
    } else if (id == NID_SIKEP434) { //Added
	      engntru_register_sikep434(*pmeth);
    } else {
        /* Unsupported method */
        return 0;
    }

    return 1;
}

static
int engntru_pkey_asn1_meths(ENGINE *e, EVP_PKEY_ASN1_METHOD **ameth, const int **nids, int nid)
{
    (void)e;
    if(!ameth) {
        debug("GET LIST\n");
        *nids = engntru_pkey_asn1_meth_nids;
        return ENGNTRU_NELEM(engntru_pkey_asn1_meth_nids) - 1;
    }
    debug("NID(%d/%s) ->", nid, OBJ_nid2sn(nid));

    if (nid == NID_SNTRUP761) {
        *ameth = ameth_sntrup761;
        debug_sl("ameth_sntrup761\n");
        return 1;
    } else if (nid == NID_SNTRUP857) {
        *ameth = ameth_sntrup857;
        debug_sl("ameth_sntrup857\n");
        return 1;
    } else if (nid == NID_SIKEP434) { //Added
      	*ameth = ameth_sikep434;
      	debug_sl("ameth_sikep434\n");
      	return 1;
    }

    debug_sl("NOT FOUND\n");
    *ameth = NULL;
    return 0;
}

/* Register a new EVP_PKEY_ASN1_METHOD for alg id under given flags */
static
int engntru_register_ameth(int id, EVP_PKEY_ASN1_METHOD **ameth, int flags)
{
    const char *pem_str = NULL;
    const char *info = NULL;

    (void)flags;

    if (!ameth)
        return 0;

    if (*ameth != NULL) {
        debug("ameth was already registered\n");
        return 1;
    }

    if (id == NID_SNTRUP761) {
        pem_str = OBJ_nid2sn(id);
        info = "Experimental sntrup761 KEM";
    } else if (id == NID_SNTRUP857) {
        pem_str = OBJ_nid2sn(id);
        info = "Experimental sntrup857 KEM";
    } else if (id == NID_SIKEP434) {  //Added
      	pem_str = OBJ_nid2sn(id);
      	info = "Experimental sikep434 KEM";
      } else {
        /* Unsupported method */
        return 0;
    }

    return engntru_register_asn1_meth(id, ameth, pem_str, info);
}

static int engntru_register_methods(void)
{
    /* AMETHS ----- {{{ */
    if (!engntru_register_ameth(NID_SNTRUP761, &ameth_sntrup761, 0)) {
        return 0;
    }
    if (!engntru_register_ameth(NID_SNTRUP857, &ameth_sntrup857, 0)) {
        return 0;
    }
    if (!engntru_register_ameth(NID_SIKEP434, &ameth_sikep434, 0)) { //Added
	return 0;
    }
    /* }}} ----- AMETHS */

    /* PMETHS ----- {{{ */
    if (!engntru_register_pmeth(NID_SNTRUP761, &pmeth_sntrup761, 0)) {
        return 0;
    }
    if (!engntru_register_pmeth(NID_SNTRUP857, &pmeth_sntrup857, 0)) {
        return 0;
    }
    if (!engntru_register_pmeth(NID_SIKEP434, &pmeth_sikep434, 0 )) { //Added
	return 0;
    }
    /* }}} ----- PMETHS */

    return 1;
}

static void engntru_clear_methods(void)
{
    verbose("CALLED\n");

    pmeth_sntrup857 = NULL;
    pmeth_sntrup761 = NULL;
    pmeth_sikep434  = NULL; // Added
    ameth_sntrup857 = NULL;
    ameth_sntrup761 = NULL;
    ameth_sikep434  = NULL; //Added
}

static int engntru_bind(ENGINE *e, const char *id)
{
    (void)id;
    debug_logging_init(ENGNTRU_DEBUG_DEFAULT_LEVEL, ENGNTRU_DEBUG_ENVVAR);

    verbose("CALLED(%p, \"%s\"[%p])\n", e, id, id);
    int ret = 0;
    if (!ENGINE_set_id(e, engine_id)) {
        errorf("ENGINE_set_id failed\n");
        goto end;
    }
    if (!ENGINE_set_name(e, engine_name)) {
        errorf("ENGINE_set_name failed\n");
        goto end;
    }

    if(!ENGINE_set_destroy_function(e, engntru_e_destroy)) {
        errorf("ENGINE_set_destroy_function failed\n");
        goto end;
    }
    if(!ENGINE_set_init_function(e, engntru_e_init)) {
        errorf("ENGINE_set_init_function failed\n");
        goto end;
    }
    if(!ENGINE_set_finish_function(e, engntru_e_finish)) {
        errorf("ENGINE_set_finish_function failed\n");
        goto end;
    }

    if (!ERR_load_ENGNTRU_strings()) {
        errorf("ERR_load_ENGNTRU_strings failed\n");
        goto end;
    }

    if (!engntru_register_nids()) {
        errorf("Failure registering NIDs\n");
        goto end;
    } else {
        engntru_pkey_meth_nids_init();
        engntru_pkey_asn1_meth_nids_init();
    }

    if (!engntru_register_methods()) {
        errorf("Failure registering methods\n");
        goto end;
    }

    if (!ENGINE_set_pkey_asn1_meths(e, engntru_pkey_asn1_meths)) {
        errorf("ENGINE_set_pkey_asn1_meths failed\n");
        goto end;
    }

    if (!ENGINE_set_pkey_meths(e, engntru_pkey_meths)) {
        errorf("ENGINE_set_pkey_meths failed\n");
        goto end;
    }


    if (!engntru_implementation_init()) {
        errorf("engntru_implementation_init failed\n");
        goto end;
    }

    ret = 1;
end:
    return ret;
}

IMPLEMENT_DYNAMIC_BIND_FN(engntru_bind)
IMPLEMENT_DYNAMIC_CHECK_FN()

/* vim: set ts=4 sw=4 tw=78 et : */
