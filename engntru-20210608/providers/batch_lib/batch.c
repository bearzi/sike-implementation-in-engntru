/*
 *  engNTRU - An engine for batch NTRU Prime PQC in OpenSSL.
 *  Copyright (C) 2019 Tampere University Foundation sr
 *
 *  This file is part of engNTRU.
 *
 *  engNTRU is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or (at your
 *  option) any later version.
 *
 *  engNTRU is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *

 */

#include "debug/debug.h"

#include <string.h>

#include <openssl/err.h>
#include "ossl/engntru_err.h"

#include "ossl/engntru_objects.h"
#include "meths/kem_keypair.h"

#include "internal.h"
#include "batch.h"

#include <crypto_kem_sntrup761.h>
#include <crypto_kem_sntrup857.h>

#include <openssl/crypto.h>

#include <oqs/oqs.h>

#define SNTRUP761_BATCH_SIZE 32
#define SNTRUP857_BATCH_SIZE 32
#define SIKEP434_BATCH_SIZE 32

// scaffolding for tests of -async:
// #define FAKE_BATCH_KEYGEN 1

static CRYPTO_ONCE init_once = CRYPTO_ONCE_STATIC_INIT;
static struct {
    CRYPTO_RWLOCK *lock;
    int ref_count;

    BATCH_CTX *sntrup761_ctx;
    BATCH_CTX *sntrup857_ctx;
    BATCH_CTX *sikep434_ctx;
} global_ctx;

static
void global_ctx_lock_init(void)
{
    if (global_ctx.lock != NULL) {
        errorf("global_ctx.lock is not NULL\n");
    }
    global_ctx.lock = CRYPTO_THREAD_lock_new();
}

#ifdef FAKE_BATCH_KEYGEN
/* Returns 0 on success, 1 otherwise */
static
int crypto_kem_sntrup761_fake_keypair_batch(unsigned char *pk, unsigned char *sk, unsigned n)
{
    unsigned i;
    const struct engntru_kem_nid_data_st *nid_data = NULL;
    nid_data = engntru_kem_get_nid_data(NID_SNTRUP761);

    for (i = 0; i < n; i++) {
        int ret;
        ret = crypto_kem_sntrup761_keypair(pk + i * nid_data->pk_bytes,
                                           sk + i * nid_data->sk_bytes);
        if (ret != 0) {
            errorf("Error generating keypair");
            return 1;
        }
    }

    return 0;
}

/* Returns 0 on success, 1 otherwise */
static
int crypto_kem_sntrup857_fake_keypair_batch(unsigned char *pk, unsigned char *sk, unsigned n)
{
    unsigned i;
    const struct engntru_kem_nid_data_st *nid_data = NULL;
    nid_data = engntru_kem_get_nid_data(NID_SNTRUP857);

    for (i = 0; i < n; i++) {
        int ret;
        ret = crypto_kem_sntrup857_keypair(pk + i * nid_data->pk_bytes,
                                           sk + i * nid_data->sk_bytes);
        if (ret != 0) {
            errorf("Error generating keypair");
            return 1;
        }
    }

    return 0;
}
#endif

/* Returns 0 on success, 1 otherwise */
// Added
static int crypto_kem_sike434_keypair_batch(unsigned char *pk, unsigned char *sk, unsigned n)
{
    unsigned i;
    const struct engntru_kem_nid_data_st *nid_data = NULL;
    nid_data = engntru_kem_get_nid_data(NID_SIKEP434);

    for (i = 0; i < n; i++) {
        int ret;
        ret = OQS_KEM_sike_p434_keypair(pk + i * nid_data->pk_bytes,
                                           sk + i * nid_data->sk_bytes);
        if (ret != 0) {
            errorf("Error generating keypair");
            return 1;
        }
    }

    return 0;
}


static inline
int BATCH_STORE_fill(BATCH_STORE *store, int nid, size_t batch_size)
{
    int (*crypto_kem_batch_keygen_fn)(unsigned char *pk, unsigned char *sk, unsigned n) = NULL;

    if (nid == NID_SNTRUP761) {
#ifndef FAKE_BATCH_KEYGEN
        crypto_kem_batch_keygen_fn = crypto_kem_sntrup761_keypair_batch;
#else
        crypto_kem_batch_keygen_fn = crypto_kem_sntrup761_fake_keypair_batch;
#endif
        info("crypto_kem_sntrup761_keypair_batch\n");
    } else if (nid == NID_SNTRUP857) {
#ifndef FAKE_BATCH_KEYGEN
        crypto_kem_batch_keygen_fn = crypto_kem_sntrup857_keypair_batch;
#else
        crypto_kem_batch_keygen_fn = crypto_kem_sntrup857_fake_keypair_batch;
#endif
        info("crypto_kem_sntrup857_keypair_batch\n");
    } else if (nid == NID_SIKEP434) { // Added
#ifndef FAKE_BATCH_KEYGEN // Function declared right above
        crypto_kem_batch_keygen_fn = crypto_kem_sike434_keypair_batch;
#else
        crypto_kem_batch_keygen_fn = crypto_kem_sike434_keypair_batch;//_fake_keypair_batch;
#endif
        info("crypto_kem_sikep434_keypair_batch\n");
    } else {
        return 0;
    }

    if (crypto_kem_batch_keygen_fn(store->pks, store->sks, batch_size) == 0) {
        store->available = batch_size;
        return 1;
    }
    return 0;
}

static inline
BATCH_STORE *BATCH_STORE_new(const struct engntru_kem_nid_data_st *nid_data, size_t batch_size)
{
    int ok = 0;
    BATCH_STORE *store = NULL;
    size_t data_size = 0, sks_len = 0, pks_len = 0;
    CRYPTO_RWLOCK *lock = NULL;

    pks_len = batch_size * nid_data->pk_bytes;
    sks_len = batch_size * nid_data->sk_bytes;
    data_size = pks_len + sks_len;

    if (data_size <= 0
            || NULL == (store = OPENSSL_secure_zalloc(sizeof(*store)+data_size))
            || NULL == (lock = CRYPTO_THREAD_lock_new()))
        goto end;

    store->data_size = data_size;
    store->lock = lock;
    store->pks = &(store->_data[0]);
    store->sks = &(store->_data[pks_len]);

    if (!BATCH_STORE_fill(store, nid_data->nid, batch_size))
        goto end;

    ok = 1;

 end:
    if (!ok) {
        CRYPTO_THREAD_lock_free(lock);
        OPENSSL_secure_free(store);
        store = NULL;
    }
    return store;
}

static inline
void BATCH_STORE_free(BATCH_STORE *store)
{
    size_t data_size = 0;

    if (store == NULL)
        return;

    data_size = store->data_size;

    CRYPTO_THREAD_lock_free(store->lock);
    OPENSSL_secure_clear_free(store, sizeof(*store) + data_size);
}

static inline
BATCH_CTX *BATCH_CTX_new(const struct engntru_kem_nid_data_st *nid_data)
{
    int ok = 0;
    size_t batch_size = 0;
    BATCH_CTX *ctx = NULL;

    if (NULL == (ctx = OPENSSL_zalloc(sizeof(*ctx))))
        goto err;

    ctx->nid_data = nid_data;

    if (nid_data->nid == NID_SNTRUP761) {
        batch_size = ctx->batch_size = SNTRUP761_BATCH_SIZE;
    } else if (nid_data->nid == NID_SNTRUP857) {
        batch_size = ctx->batch_size = SNTRUP857_BATCH_SIZE;
    } else if (nid_data->nid == NID_SIKEP434) { // Added
        batch_size = ctx->batch_size = SIKEP434_BATCH_SIZE;
    }

    if (batch_size == 0)
        goto err;

    if (NULL == (ctx->store = BATCH_STORE_new(nid_data, batch_size)))
        goto err;

    ok = 1;

 err:
    if (!ok) {
        OPENSSL_free(ctx);
        ctx = NULL;
    }
    return ctx;
}

static inline
void BATCH_CTX_free(BATCH_CTX *ctx)
{
    /* This should be called while holding the parent lock */
    if (ctx == NULL)
        return;

    BATCH_STORE_free(ctx->store);
    OPENSSL_free(ctx);
}

static inline
int engntru_prov_sntrup_get_keypair(ENGNTRU_KEM_KEYPAIR *kp)
{
    struct engntru_kem_nid_data_st nid_data;
    BATCH_CTX *ctx = NULL;
    size_t i;

    info("CALLED\n");

    /* This is always called only internally, assume kp is valid */

    if (!CRYPTO_THREAD_write_lock(global_ctx.lock)) {
        errorf("Failed acquiring global_ctx.lock\n");
        return 0;
    }
    if (kp->nid == NID_SNTRUP761) {
        if (global_ctx.sntrup761_ctx == NULL) {
            ctx = global_ctx.sntrup761_ctx = BATCH_CTX_new(kp->nid_data);
            if (ctx == NULL)
                return 0;
        } else {
            ctx = global_ctx.sntrup761_ctx;
        }
    } else if (kp->nid == NID_SNTRUP857) {
        if (global_ctx.sntrup857_ctx == NULL) {
            ctx = global_ctx.sntrup857_ctx = BATCH_CTX_new(kp->nid_data);
            if (ctx == NULL)
                return 0;
        } else {
            ctx = global_ctx.sntrup857_ctx;
        }
    } else if (kp->nid == NID_SIKEP434) { // Added
        if (global_ctx.sikep434_ctx == NULL) {
            ctx = global_ctx.sikep434_ctx = BATCH_CTX_new(kp->nid_data);
            if (ctx == NULL)
                return 0;
        } else {
            ctx = global_ctx.sikep434_ctx;
        }
    }
    if (!CRYPTO_THREAD_unlock(global_ctx.lock)) {
        errorf("Failed releasing global_ctx.lock\n");
        return 0;
    }

    nid_data = *(ctx->nid_data);

    if (!CRYPTO_THREAD_write_lock(ctx->store->lock)) {
        errorf("Failed acquiring store lock\n");
        return 0;
    }
    if (ctx->store->available == 0) {
        /* We need to replenish the store */
        if (!BATCH_STORE_fill(ctx->store, nid_data.nid, ctx->batch_size)) {
            if (!CRYPTO_THREAD_unlock(ctx->store->lock))
                errorf("Failed releasing store lock\n");
            return 0;
        }
        if (ctx->store->available == 0) {
            if (!CRYPTO_THREAD_unlock(ctx->store->lock))
                errorf("Failed releasing store lock\n");
            return 0;
        }
    }
    i = --ctx->store->available;

    memcpy(kp->pk, ctx->store->pks + i * nid_data.pk_bytes, nid_data.pk_bytes);
    memcpy(kp->sk, ctx->store->sks + i * nid_data.sk_bytes, nid_data.sk_bytes);

    /* Erase keypair from buffer for PFS */
    OPENSSL_cleanse(ctx->store->sks + i * nid_data.sk_bytes, nid_data.sk_bytes);
    OPENSSL_cleanse(ctx->store->pks + i * nid_data.pk_bytes, nid_data.pk_bytes);

    if (!CRYPTO_THREAD_unlock(ctx->store->lock)) {
        errorf("Failed releasing store lock\n");
        return 0;
    }

    return 1;
}

int engntru_prov_kem_batch_init(void)
{
    if (!CRYPTO_THREAD_run_once(&init_once, global_ctx_lock_init) || global_ctx.lock == NULL)
        return 0;

    if (!CRYPTO_THREAD_write_lock(global_ctx.lock)) {
        errorf("Failed acquiring global_ctx.lock\n");
        return 0;
    }

    global_ctx.ref_count++;

    if (!CRYPTO_THREAD_unlock(global_ctx.lock)) {
        errorf("Failed releasing global_ctx.lock\n");
        return 0;
    }

    return 1;
}

int engntru_prov_kem_batch_deinit(void)
{
    CRYPTO_RWLOCK *l = NULL;
    if (!CRYPTO_THREAD_write_lock(global_ctx.lock)) {
        errorf("Failed acquiring global_ctx.lock\n");
        return 0;
    }

    global_ctx.ref_count--;

    if (global_ctx.ref_count == 0) {
        BATCH_CTX_free(global_ctx.sntrup761_ctx);
        BATCH_CTX_free(global_ctx.sntrup857_ctx);
        BATCH_CTX_free(global_ctx.sikep434_ctx);  // Added
        l = global_ctx.lock;
        global_ctx.lock = NULL;
        if (!CRYPTO_THREAD_unlock(l)) {
            errorf("Failed releasing global_ctx.lock\n");
            return 0;
        }
        CRYPTO_THREAD_lock_free(l);
    } else {
        if (!CRYPTO_THREAD_unlock(global_ctx.lock)) {
            errorf("Failed releasing global_ctx.lock\n");
            return 0;
        }
    }

    return 1;
}

int engntru_prov_kem_batch_get_keypair(ENGNTRU_KEM_KEYPAIR *kp)
{
    if (kp == NULL)
        return 0;

    if (kp->nid == NID_SNTRUP761 || kp->nid == NID_SNTRUP857 || kp->nid == NID_SIKEP434) // Added
        return engntru_prov_sntrup_get_keypair(kp);

    return 0;
}
