/*
 *  engNTRU - An engine for batch NTRU Prime PQC in OpenSSL.
 *  Copyright (C) 2019 Tampere University Foundation sr
 *
 *  This file is part of engNTRU.
 *
 *  engNTRU is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or (at your
 *  option) any later version.
 *
 *  engNTRU is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "providers/api/kem.h"

#include "debug/debug.h"

int engntru_prov_kem_keygen(ENGNTRU_KEM_KEYPAIR *kp)
{
    errorf("SHOULD NOT BE CALLED\n");

    (void)kp; /* unused parameter */

    return 0; /* always return failure */
}

int engntru_prov_kem_encapsulate(const ENGNTRU_KEM_KEYPAIR *kp,
                                 unsigned char *ct,
                                 unsigned char *ss)
{
    errorf("SHOULD NOT BE CALLED\n");

    (void)kp; /* unused parameter */
    (void)ct; /* unused parameter */
    (void)ss; /* unused parameter */

    return 0; /* always return failure */
}

int engntru_prov_kem_decapsulate(const ENGNTRU_KEM_KEYPAIR *kp,
                                 const unsigned char *ct,
                                 unsigned char *ss)
{
    errorf("SHOULD NOT BE CALLED\n");

    (void)kp; /* unused parameter */
    (void)ct; /* unused parameter */
    (void)ss; /* unused parameter */

    return 0; /* always return failure */
}
