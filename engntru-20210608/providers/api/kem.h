/*
 *  engNTRU - An engine for batch NTRU Prime PQC in OpenSSL.
 *  Copyright (C) 2019 Tampere University Foundation sr
 *
 *  This file is part of engNTRU.
 *
 *  engNTRU is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or (at your
 *  option) any later version.
 *
 *  engNTRU is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ENGNTRU_IMPLEMENTATION_KEM_API_H
#define ENGNTRU_IMPLEMENTATION_KEM_API_H

#include "meths/kem_keypair.h"

/*-
 * Generate a new keypair for KEM.
 *
 *  Returns:
 *     - 1 on success
 *     - 0 otherwise
 */
extern int engntru_prov_kem_keygen(ENGNTRU_KEM_KEYPAIR *kp);

/*-
 * Compute KEM encapsulation.
 *
 *  Inputs:
 *     - kp
 *          is a keypair struct containing a public key
 *  Outputs:
 *     - ct
 *          is the key encapsulation message (ciphertext)
 *          (assume buffer of appropriate size for kp)
 *     - ss
 *          is the shared secret
 *          (assume buffer of appropriate size for kp)
 *
 *  Returns:
 *     - 1 on success
 *     - 0 otherwise
 */
extern int engntru_prov_kem_encapsulate(const ENGNTRU_KEM_KEYPAIR *kp, unsigned char *ct, unsigned char *ss);

/*-
 * Compute KEM decapsulation.
 *
 *  Inputs:
 *     - kp
 *          is a keypair struct containing a private key
 *     - ct
 *          is the key encapsulation message (ciphertext)
 *          (assume buffer of appropriate size for kp)
 *  Outputs:
 *     - ss
 *          is the decapsulated shared secret
 *          (assume buffer of appropriate size for kp)
 *
 *  Returns:
 *     - 1 on success
 *     - 0 otherwise
 */
extern int engntru_prov_kem_decapsulate(const ENGNTRU_KEM_KEYPAIR *kp, const unsigned char *ct, unsigned char *ss);


#endif /* ! defined(ENGNTRU_IMPLEMENTATION_KEM_API_H) */
