/*
 *  engNTRU - An engine for batch NTRU Prime PQC in OpenSSL.
 *  Copyright (C) 2019 Tampere University Foundation sr
 *
 *  This file is part of engNTRU.
 *
 *  engNTRU is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or (at your
 *  option) any later version.
 *
 *  engNTRU is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "providers/api/kem.h"

#include "debug/debug.h"

#include <openssl/rand.h>

#include <openssl/err.h>
#include "ossl/engntru_err.h"

#include "ossl/engntru_objects.h"

#include "internal.h"

#include <crypto_kem_sntrup761.h>
#include <crypto_kem_sntrup857.h>

#include <oqs/oqs.h>

int engntru_prov_kem_keygen(ENGNTRU_KEM_KEYPAIR *kp)
{
    if (kp == NULL)
        return 0;

    if (kp->nid == NID_SNTRUP761) {
	     info("crypto_kem_sntrup761_keypair\n");
      return crypto_kem_sntrup761_keypair(kp->pk, kp->sk) ? 0 : 1;
    } else if (kp->nid == NID_SNTRUP857) {
        info("crypto_kem_sntrup857_keypair\n");
        return crypto_kem_sntrup857_keypair(kp->pk, kp->sk) ? 0 : 1;
    } else if (kp->nid == NID_SIKEP434) { // Added
      info("OQS_KEM_SIKE_p434 keypair\n");
      return OQS_KEM_sike_p434_keypair(kp->pk, kp->sk) ? 0 : 1;
    }

    return 0;
}

int engntru_prov_kem_encapsulate(const ENGNTRU_KEM_KEYPAIR *kp, unsigned char *ct, unsigned char *ss)
{
    if (kp == NULL || ct == NULL || ss == NULL)
        return 0;

    if (kp->nid == NID_SNTRUP761) {
	     info("crypto_kem_sntrup761_enc\n");
        return crypto_kem_sntrup761_enc(ct, ss, kp->pk) ? 0 : 1;
    } else if (kp->nid == NID_SNTRUP857) {
        info("crypto_kem_sntrup857_enc\n");
        return crypto_kem_sntrup857_enc(ct, ss, kp->pk) ? 0 : 1;
    } else if (kp->nid == NID_SIKEP434) {//ADDED
      	info("OQS_KEM_sike_p434 encapsulation\n");
      	return OQS_KEM_sike_p434_encaps(ct, ss, kp->pk) ? 0 : 1;
    }

    return 0;
}

int engntru_prov_kem_decapsulate(const ENGNTRU_KEM_KEYPAIR *kp, const unsigned char *ct, unsigned char *ss)
{
    if (kp == NULL || ct == NULL || ss == NULL)
        return 0;

    if (kp->nid == NID_SNTRUP761) {
	     info("crypto_kem_sntrup761_dec\n");
        return crypto_kem_sntrup761_dec(ss, ct, kp->sk) ? 0 : 1;
    } else if (kp->nid == NID_SNTRUP857) {
        info("crypto_kem_sntrup857_dec\n");
        return crypto_kem_sntrup857_dec(ss, ct, kp->sk) ? 0 : 1;
    } else if (kp->nid == NID_SIKEP434) { //Added
      	info("OQS_KEM_SIKE decaps\n");
	      return OQS_KEM_sike_p434_decaps(ss, ct, kp->sk) ? 0 : 1;
    }

    return 0;
}
