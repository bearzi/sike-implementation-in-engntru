/*
 *  engNTRU - An engine for batch NTRU Prime PQC in OpenSSL.
 *  Copyright (C) 2019 Tampere University Foundation sr
 *
 *  This file is part of engNTRU.
 *
 *  engNTRU is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or (at your
 *  option) any later version.
 *
 *  engNTRU is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "debug/debug.h"

#include <string.h>
#include <stdint.h>

#include <openssl/err.h>
#include "ossl/engntru_err.h"

#include "ossl/engntru_objects.h"
#include "meths/kem_keypair.h"

#include "internal.h"
#include "batch.h"

#include <crypto_kem_sntrup761.h>
#include <crypto_kem_sntrup857.h>

#include <openssl/crypto.h>

#include <pthread.h>

//ADDED
#include <oqs/oqs.h>
#include <time.h>

#define SNTRUP761_BATCH_SIZE 32
#define SNTRUP857_BATCH_SIZE 32
#define SIKEP434_BATCH_SIZE 32

// scaffolding for tests of -async:
// #define FAKE_BATCH_KEYGEN 1

static void *engntru_async_batch_filler_routine(void *arg);

static CRYPTO_ONCE init_once = CRYPTO_ONCE_STATIC_INIT;
static struct {
    CRYPTO_RWLOCK *lock;
    int ref_count;

    BATCH_CTX *sntrup761_ctx;
    BATCH_CTX *sntrup857_ctx;
    BATCH_CTX *sikep434_ctx;
} global_ctx;

static
void global_ctx_lock_init(void)
{
    if (global_ctx.lock != NULL) {
        errorf("global_ctx.lock is not NULL\n");
    }
    global_ctx.lock = CRYPTO_THREAD_lock_new();
}

#ifdef FAKE_BATCH_KEYGEN
/* Returns 0 on success, 1 otherwise */
static
int crypto_kem_sntrup761_fake_keypair_batch(unsigned char *pk, unsigned char *sk, unsigned n)
{
    unsigned i;
    const struct engntru_kem_nid_data_st *nid_data = NULL;
    nid_data = engntru_kem_get_nid_data(NID_SNTRUP761);

    for (i = 0; i < n; i++) {
        int ret;
        ret = crypto_kem_sntrup761_keypair(pk + i * nid_data->pk_bytes,
                                           sk + i * nid_data->sk_bytes);
        if (ret != 0) {
            errorf("Error generating keypair");
            return 1;
        }
    }

    return 0;
}

/* Returns 0 on success, 1 otherwise */
static
int crypto_kem_sntrup857_fake_keypair_batch(unsigned char *pk, unsigned char *sk, unsigned n)
{
    unsigned i;
    const struct engntru_kem_nid_data_st *nid_data = NULL;
    nid_data = engntru_kem_get_nid_data(NID_SNTRUP857);

    for (i = 0; i < n; i++) {
        int ret;
        ret = crypto_kem_sntrup857_keypair(pk + i * nid_data->pk_bytes,
                                           sk + i * nid_data->sk_bytes);
        if (ret != 0) {
            errorf("Error generating keypair");
            return 1;
        }
    }

    return 0;
}
#endif
// Added
static int crypto_kem_sike434_keypair_batch(unsigned char *pk, unsigned char *sk, unsigned n)
{
    unsigned i;
    const struct engntru_kem_nid_data_st *nid_data = NULL;
    nid_data = engntru_kem_get_nid_data(NID_SIKEP434);

    for (i = 0; i < n; i++) {
        int ret;
        ret = OQS_KEM_sike_p434_keypair(pk + i * nid_data->pk_bytes,
                                           sk + i * nid_data->sk_bytes);
        if (ret != 0) {
            errorf("Error generating keypair");
            return 1;
        }
    }

    return 0;
}


static inline
int BATCH_STORE_fill(BATCH_STORE *store, int nid, size_t batch_size)
{
    int (*crypto_kem_batch_keygen_fn)(unsigned char *pk, unsigned char *sk, unsigned n) = NULL;

    if (nid == NID_SNTRUP761) {
#ifndef FAKE_BATCH_KEYGEN
        crypto_kem_batch_keygen_fn = crypto_kem_sntrup761_keypair_batch;
#else
        crypto_kem_batch_keygen_fn = crypto_kem_sntrup761_fake_keypair_batch;
#endif
        info("crypto_kem_sntrup761_keypair_batch\n");
    } else if (nid == NID_SNTRUP857) {
#ifndef FAKE_BATCH_KEYGEN
        crypto_kem_batch_keygen_fn = crypto_kem_sntrup857_keypair_batch;
#else
        crypto_kem_batch_keygen_fn = crypto_kem_sntrup857_fake_keypair_batch;
#endif
        info("crypto_kem_sntrup857_keypair_batch\n");
    } else if (nid == NID_SIKEP434) { // Added
#ifndef FAKE_BATCH_KEYGEN
	     crypto_kem_batch_keygen_fn = crypto_kem_sike434_keypair_batch;
#else
	     crypto_kem_batch_keygen_fn = crypto_kem_sike434_keypair_batch;//_fake_k>
#endif
	     info("crypto_kem_sikep434_keypair_batch\n");
    } else {
        return 0;
    }


    if (crypto_kem_batch_keygen_fn(store->pks, store->sks, batch_size) == 0) {
        store->available = batch_size;
        return 1;
    }

	return 0;
}

static inline
BATCH_STORE *BATCH_STORE_new(const struct engntru_kem_nid_data_st *nid_data, size_t batch_size)
{
    int ok = 0;
    BATCH_STORE *store = NULL;
    size_t data_size = 0, sks_len = 0, pks_len = 0;

    pks_len = batch_size * nid_data->pk_bytes;
    sks_len = batch_size * nid_data->sk_bytes;
    data_size = pks_len + sks_len;

    if (data_size <= 0
            || NULL == (store = OPENSSL_secure_zalloc(sizeof(*store)+data_size)))
        goto end;

    store->data_size = data_size;
    store->pks = &(store->_data[0]);
    store->sks = &(store->_data[pks_len]);

    if (!BATCH_STORE_fill(store, nid_data->nid, batch_size))
        goto end;

    ok = 1;

 end:
    if (!ok) {
        OPENSSL_secure_free(store);
        store = NULL;
    }
    return store;
}

static inline
void BATCH_STORE_free(BATCH_STORE *store)
{
    size_t data_size = 0;

    if (store == NULL)
        return;

    data_size = store->data_size;

    OPENSSL_secure_clear_free(store, sizeof(*store) + data_size);
}

static inline
BATCH_CTX *BATCH_CTX_new(const struct engntru_kem_nid_data_st *nid_data)
{
    int i;
    int ok = 0;
    size_t batch_size = 0;
    BATCH_CTX *ctx = NULL;

    if (NULL == (ctx = OPENSSL_zalloc(sizeof(*ctx))))
        goto err;

    ctx->nid_data = nid_data;

    if (nid_data->nid == NID_SNTRUP761) {
        batch_size = ctx->batch_size = SNTRUP761_BATCH_SIZE;
    } else if (nid_data->nid == NID_SNTRUP857) {
        batch_size = ctx->batch_size = SNTRUP857_BATCH_SIZE;
    } else if (nid_data->nid == NID_SIKEP434) { // Added
	batch_size = ctx->batch_size = SIKEP434_BATCH_SIZE;
    }

    if (batch_size == 0)
        goto err;

    if (pthread_mutex_init(&ctx->mutex, NULL)
            || pthread_cond_init(&ctx->emptied, NULL)
            || pthread_cond_init(&ctx->filled, NULL))
        goto err;

    for (i = 0; i < BATCH_STORE_N; i++) {
        if (NULL == (ctx->stores[i] = BATCH_STORE_new(nid_data, batch_size)))
            goto err;
    }

    if (pthread_create(&ctx->filler_th, NULL,
                &engntru_async_batch_filler_routine, (void*)ctx)) {
        fatalf("pthread_create() failed\n");
        goto err;
    }

    debug("(%d) locking mutex", nid_data->nid)
    pthread_mutex_lock(&ctx->mutex);
    debug("(%d) locked mutex", nid_data->nid);

    while (ctx->store == NULL) {
        debug("(%d) wait on filled\n", nid_data->nid);
        pthread_cond_wait(&ctx->filled, &ctx->mutex);
    }

    debug("(%d) awake", nid_data->nid)

    debug("(%d) unlocking mutex\n", nid_data->nid);
    pthread_mutex_unlock(&ctx->mutex);

    ok = 1;

 err:
    if (!ok) {
        for (i=0; i<BATCH_STORE_N; i++) {
            BATCH_STORE_free(ctx->stores[i]);
        }
        if (pthread_cond_destroy(&ctx->filled)) {
            errorf("Failed destroying filled cond\n");
        }
        if (pthread_cond_destroy(&ctx->emptied)) {
            errorf("Failed destroying emptied cond\n");
        }
        if (pthread_mutex_destroy(&ctx->mutex)) {
            errorf("Failed destroying mutex\n");
        }
        OPENSSL_free(ctx);
        ctx = NULL;
    }
    debug("(%d) done\n", nid_data->nid);
    return ctx;
}

static inline
void BATCH_CTX_free(BATCH_CTX *ctx)
{
    /* This should be called while holding the parent lock */
    int nid;
    void *tret;
    int i;

    if (ctx == NULL)
        return;

    nid = ctx->nid_data->nid;

    debug("(%d) locking mutex", nid)
    pthread_mutex_lock(&ctx->mutex);
    debug("(%d) locked mutex", nid);

    ctx->destroy = 1;
    ctx->store = NULL;

    debug("(%d) signal emptied (for destroy)\n", nid);
    pthread_cond_signal(&ctx->emptied);

    debug("(%d) unlocking mutex\n", nid);
    pthread_mutex_unlock(&ctx->mutex);

    debug("(%d) pthread_join()\n", nid);
    if (pthread_join(ctx->filler_th, &tret)) {
        errorf("(%d) pthread_join() failed\n", nid);
    } else {
        intptr_t ret = (intptr_t)tret;
        if (ret != 1)
            errorf("(%d) filler thread returned %" PRIxPTR "\n", nid, ret);
    }

    for (i=0; i<BATCH_STORE_N; i++) {
        BATCH_STORE_free(ctx->stores[i]);
    }

    if (pthread_cond_destroy(&ctx->filled)
            || pthread_cond_destroy(&ctx->emptied)
            || pthread_mutex_destroy(&ctx->mutex))
        errorf("(%d) failure destroying cond or mutex\n", nid);

    OPENSSL_free(ctx);
}

static inline
int BATCH_STORE_get_keypair(BATCH_STORE *store, ENGNTRU_KEM_KEYPAIR *kp)
{
    int ret = 0;
    size_t i;
    struct engntru_kem_nid_data_st nid_data = *(kp->nid_data);

    if (store->available == 0) {
        /* This branch should never be taken */
        fatalf("Store should never be empty\n");
        return 0;
    }
    i = --store->available;

    memcpy(kp->pk, store->pks + i * nid_data.pk_bytes, nid_data.pk_bytes);
    memcpy(kp->sk, store->sks + i * nid_data.sk_bytes, nid_data.sk_bytes);

    /* Erase keypair from buffer for PFS */
    OPENSSL_cleanse(store->sks + i * nid_data.sk_bytes, nid_data.sk_bytes);
    OPENSSL_cleanse(store->pks + i * nid_data.pk_bytes, nid_data.pk_bytes);

    if (store->available == 0) {
        /*
         * We took the last key!
         */
        ret = -1;
    } else {
        ret = 1;
    }

    return ret;
}

static inline
int BATCH_CTX_get_keypair(BATCH_CTX *ctx, ENGNTRU_KEM_KEYPAIR *kp)
{
    int ret = 0, r, nid;

    nid = ctx->nid_data->nid;
    (void)nid; /* could be unused in some builds */

    debug("(%d) locking mutex", nid)
    pthread_mutex_lock(&ctx->mutex);
    debug("(%d) locked mutex", nid);

    while (ctx->store == NULL) {
        debug("(%d) wait on filled\n", nid);
        pthread_cond_wait(&ctx->filled, &ctx->mutex);
    }
    debug("(%d) awake", nid)

    r = BATCH_STORE_get_keypair(ctx->store, kp);
    if (r == -1) {
        /*
         * The store has been emptied.
         */
        ctx->store = NULL;

        debug("(%d) signal emptied\n", nid);
        pthread_cond_signal(&ctx->emptied);
    } else if (r != 1) {
        errorf("(%d) BATCH_STORE_get_keypair() failed\n", nid);
        goto end;
    }

    ret = 1;
 end:
    debug("(%d) unlocking mutex\n", nid);
    pthread_mutex_unlock(&ctx->mutex);

    return ret;
}

static inline
int engntru_prov_sntrup_get_keypair(ENGNTRU_KEM_KEYPAIR *kp)
{
    BATCH_CTX *ctx = NULL;

    info("CALLED\n");

    /* This is always called only internally, assume kp is valid */

    if (!CRYPTO_THREAD_write_lock(global_ctx.lock)) {
        errorf("Failed acquiring global_ctx.lock\n");
        return 0;
    }
    if (kp->nid == NID_SNTRUP761) {
        if (global_ctx.sntrup761_ctx == NULL) {
            ctx = global_ctx.sntrup761_ctx = BATCH_CTX_new(kp->nid_data);
            if (ctx == NULL)
                return 0;
        } else {
            ctx = global_ctx.sntrup761_ctx;
        }
    } else if (kp->nid == NID_SNTRUP857) {
        if (global_ctx.sntrup857_ctx == NULL) {
            ctx = global_ctx.sntrup857_ctx = BATCH_CTX_new(kp->nid_data);
            if (ctx == NULL)
                return 0;
        } else {
            ctx = global_ctx.sntrup857_ctx;
        }
    } else if (kp->nid == NID_SIKEP434) { // Added
	if (global_ctx.sikep434_ctx == NULL) {
		ctx = global_ctx.sikep434_ctx = BATCH_CTX_new(kp->nid_data);
		if (ctx == NULL)
			return 0;
	} else {
		ctx = global_ctx.sikep434_ctx;
	}
    }
    if (!CRYPTO_THREAD_unlock(global_ctx.lock)) {
        errorf("Failed releasing global_ctx.lock\n");
        return 0;
    }

    if (!BATCH_CTX_get_keypair(ctx, kp)) {
        errorf("BATCH_CTX_get_keypair() failed\n");
        return 0;
    }

    return 1;
}

int engntru_prov_kem_batch_init(void)
{
    if (!CRYPTO_THREAD_run_once(&init_once, global_ctx_lock_init)
            || global_ctx.lock == NULL)
        return 0;

    if (!CRYPTO_THREAD_write_lock(global_ctx.lock)) {
        errorf("Failed acquiring global_ctx.lock\n");
        return 0;
    }

    global_ctx.ref_count++;

    if (!CRYPTO_THREAD_unlock(global_ctx.lock)) {
        errorf("Failed releasing global_ctx.lock\n");
        return 0;
    }

    return 1;
}

int engntru_prov_kem_batch_deinit(void)
{
    CRYPTO_RWLOCK *l = NULL;
    if (!CRYPTO_THREAD_write_lock(global_ctx.lock)) {
        errorf("Failed acquiring global_ctx.lock\n");
        return 0;
    }

    global_ctx.ref_count--;

    if (global_ctx.ref_count == 0) {
        BATCH_CTX_free(global_ctx.sntrup761_ctx);
        BATCH_CTX_free(global_ctx.sntrup857_ctx);
        BATCH_CTX_free(global_ctx.sikep434_ctx); // Added
	l = global_ctx.lock;
        global_ctx.lock = NULL;
        if (!CRYPTO_THREAD_unlock(l)) {
            errorf("Failed releasing global_ctx.lock\n");
            return 0;
        }
        CRYPTO_THREAD_lock_free(l);
    } else {
        if (!CRYPTO_THREAD_unlock(global_ctx.lock)) {
            errorf("Failed releasing global_ctx.lock\n");
            return 0;
        }
    }

    return 1;
}

int engntru_prov_kem_batch_get_keypair(ENGNTRU_KEM_KEYPAIR *kp)
{
    if (kp == NULL)
        return 0;

    if (kp->nid == NID_SNTRUP761 || kp->nid == NID_SNTRUP857 || kp->nid == NID_SIKEP434) // Added
        return engntru_prov_sntrup_get_keypair(kp);

    return 0;
}

static inline
int engntru_async_batch_filler(BATCH_CTX *ctx)
{
    int ret = 0;
    int i, j;
    int nid = ctx->nid_data->nid;
    size_t batch_size = ctx->batch_size;
    BATCH_STORE *q[BATCH_STORE_N] = { NULL };

    debug("(%d) started\n", nid);

    while (1) {
        debug("(%d) locking mutex\n", nid);
        pthread_mutex_lock(&ctx->mutex);
        debug("(%d) locked mutex\n", nid);

        while (ctx->store != NULL && ctx->destroy != 1) {
            debug("(%d) wait on emptied\n", nid);
            pthread_cond_wait(&ctx->emptied, &ctx->mutex);
        }
        debug("(%d) awake\n", nid);

        if (ctx->destroy) {
            debug("(%d) destroy signal\n", nid);
            break;
        }

        /* assert(ctx->store == NULL); */

        for (i = 0, j = 0; i < BATCH_STORE_N; i++) {
            if (ctx->stores[i]->available == 0) {
                debug("(%d) queuing stores[%d] for refill\n", nid, i);
                q[j++] = ctx->stores[i];
            } else {
                debug("(%d) ctx->store = stores[%d]\n", nid, i);
                ctx->store = ctx->stores[i];
            }
        }

        if (ctx->store != NULL) {
            debug("(%d) broadcast filled\n", nid);
            pthread_cond_broadcast(&ctx->filled);
        }

        debug("(%d) unlocking mutex\n", nid);
        pthread_mutex_unlock(&ctx->mutex);

        for (--j; j >= 0; j--) {
            debug("(%d) filling q[%d]\n", nid, j);
            if (!BATCH_STORE_fill(q[j], nid, batch_size)) {
                errorf("(%d) BATCH_STORE_fill() failed\n", nid);
                goto end;
            }
            q[j] = NULL;
        }
    }

    debug("(%d) unlocking mutex\n", nid);
    pthread_mutex_unlock(&ctx->mutex);

    ret = 1;

 end:
    debug("(%d) exiting (ret=%d)\n", nid, ret);
    return ret;
}

static
void *engntru_async_batch_filler_routine(void *arg)
{
    intptr_t ret;
    BATCH_CTX *ctx = arg;

    ret = (intptr_t) engntru_async_batch_filler(ctx);

    return (void*)ret;
}
