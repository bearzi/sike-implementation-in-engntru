/*
 *  engNTRU - An engine for batch NTRU Prime PQC in OpenSSL.
 *  Copyright (C) 2019 Tampere University Foundation sr
 *
 *  This file is part of engNTRU.
 *
 *  engNTRU is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the
 *  Free Software Foundation, either version 3 of the License, or (at your
 *  option) any later version.
 *
 *  engNTRU is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PROVIDER_INTERNAL_H
#define PROVIDER_INTERNAL_H

#include "meths/kem_keypair.h"

/*-
 * Get a new keypair for KEM.
 *
 * Returns:
 *      * 1 on success
 *      * 0 otherwise
 */
int engntru_prov_kem_batch_get_keypair(ENGNTRU_KEM_KEYPAIR *kp);

/*
 * Returns 0 on success, non-0 otherwise.
 */
int engntru_prov_kem_batch_init(void);

/*
 * Returns 0 on success, non-0 otherwise.
 */
int engntru_prov_kem_batch_deinit(void);

#endif /* !defined(PROVIDER_INTERNAL_H) */
