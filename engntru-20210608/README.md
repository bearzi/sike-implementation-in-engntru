# engNTRU OpenSSL ENGINE

This ENGINE is part of [OpenSSLNTRU](), a project demonstrating how to
transparently enable post-quantum TLS 1.3 key exchanges for the existing
software ecosystem built on top of OpenSSL `libssl`.
Visit <https://opensslntru.cr.yp.to/> for more details and instructions.

`engNTRU` aims at developing an OpenSSL ENGINE to provide a
batch implementation for KEM cryptographic primitives, particularly the
post-quantum algorithms `sntrup761` and `sntrup857`, part of
[NTRU Prime][], a candidate in the ongoing [NIST PQC project][].

The project currently aims at compability with OpenSSL releases in the
`1.1.1` LTS branch, but to fully support post-quantum handshakes in
TLS 1.3 you will need a version of OpenSSL 1.1.1 appropriately patched.
In particular, we developed `engNTRU` against the
[OpenSSLNTRU patchset][].

## Backends and limitations

- The project currently supports three _back-end providers_ (not to be
  confused with OpenSSL 3.0 Providers, which will supersede ENGINEs):
  - [`serial_lib`](providers/serial_lib): each key generation request
    from a linked application is served with a fresh keypair is
    generated on-demand for each request. This provider does not
    take advantage of the batching performance improvementes offered
    by `libsntrup761` and `libsntrup857`.
  - [`batch_lib`](providers/batch_lib): (default build configuration)
    key generation is done in batches
    that are handled as process local heap-allocated pools.
    Each thread of an application dinamically linking against
    OpenSSL and loading this ENGINE will get a keypair from the pool
    storing a precomputed batch. If `engNTRU` finds the pool empty
    upon a keypair request, it will on-demand generate a fresh batch
    of keypairs via the optimized `libsntrup761` or `libsntru857`
    implementation.
  - [`async_batch_lib`](providers/async_batch_lib): this is based
    on `batch_lib`, featuring as the main difference an asynchronous
    batching strategy. When `engNTRU` consumes the last keypair in a
    batch, serving an application request, just before returning control
    to the application, the ENGINE awakes a dedicated "filler" thread.
    This thread is tasked with refilling the batch pool concurrently to
    the main application.

  In both batching modes, each process will handle its own batches
  indipendently.

  **CAVEAT**: for both batch variants, if a process loads `engNTRU`,
  generates a batch, and then forks, it can result in key reuse, if
  the heap is not erased (e.g., before calling `exec()`).
- The project currently only uses batch generation in the context of key
  generation;
- The project currently supports ephemeral keys and does not allow to
  serialize keys as files;

## Table of Contents

<!--ts-->
* [Project structure](#project-structure)
* [Installation](#installation)
   * [Binary distributions of OpenSSL](#binary-distributions-of-openssl)
   * [Installing prerequisites from source](#installing-prerequisites-from-source)
      * [OpenSSL](#openssl)
      * [libsntrup761](#libsntrup761)
      * [libsntrup857](#libsntrup857)
   * [Install engNTRU from sources](#install-engntru-from-sources)
* [Usage](#usage)
   * [List algorithms](#list-algorithms)
   * [TLS test](#tls-test)
      * [Generate an RSA cert for the server](#generate-an-rsa-cert-for-the-server)
      * [Launch the server](#launch-the-server)
      * [Launch the client](#launch-the-client)
   * [ENV variables](#env-variables)
      * [ENGNTRU_DEBUG](#engntru_debug)
      * [OpenSSL ENV variables](#openssl-env-variables)
* [License](#license)
* [Acknowledgments](#acknowledgments)
<!--te-->

## Project structure

The source code of the project is organized hierarchically.

```
engntru
├── debug
├── meths
│   └── internal
│       └── CMakeLists.txt
├── ossl
├── providers
│   ├── api
│   ├── _dummy
│   │   └── CMakeLists.txt
│   ├── async_batch_lib
│   │   └── CMakeLists.txt
│   ├── batch_lib
│   │   └── CMakeLists.txt
│   └── serial_lib
│       └── CMakeLists.txt
├── tests
│   └── CMakeLists.txt
└── engntru.c
```

- `engntru.c` contains the main entry point for loading the `ENGINE`;
- `meths` contains the implementation of the OpenSSL method structures
  defining the implemented cryptosystems, i.e.,
  `EVP_PKEY_METHOD`/`EVP_PKEY_ASN1_METHOD`;
- `ossl` contains code to integrate error codes, messages, NIDs, and
  OIDs in the OpenSSL abstractions;
- `providers` contains the code to map the primitives referenced in
  the `meths` structures to the actual cryptographic implementation
  provider:
  * `api` describes the API that a valid provider module needs to
    implement;
  * `_dummy` includes boilerplate code for additional functions
    (e.g. an empty `engntru_implementation_init()` that can be used
    when the backend provider does not require any initialization
    before being used);
  * `serial_lib`, `batch_lib` and `async_batch_lib` map the
    cryptographic functionality to the
    corresponding backend implementation;
- `tests` contains code used to automate testing of the `ENGINE`;
- `debug` contains definitions used to implement the debug messaging
  system;

## Installation

These instructions are just an excerpt of the demo instructions
published at <https://opensslntru.cr.yp.to/demo.html>.
For the latest updates, and for more comprehensive instructions
regarding the whole end-to-end demo, please refer to the online version.

To build `engNTRU` from source you will need:
-   `git` to clone the latest source version from this repository and
    other dependencies you plan to build from source;
-   `cmake`, `pkg-config`, `make`, `gcc`/`clang` and the
    required development headers specific for your system, to ensure a
    working build system.

In Debian-like distributions the following should suffice:

```sh
apt-get install git pkg-config cmake build-essential
```

Other flavours of UNIX will use a different package manager (replacing
`apt-get install` with something similar) and use slightly different
package names.

### Binary distributions of OpenSSL

**Note**: verify that your OS vendor provides OpenSSL >= `1.1.1`

**Note**: currently, using KEM in TLS requires a patched version of
OpenSSL's `libssl`, such as the patchset published within the
[OpenSSLNTRU][] project.


To use OpenSSL as provided by your Linux distribution, you
need to make sure the development headers are also installed.

In Debian/Ubuntu this means to install the corresponding `*-dev`
packages:

```sh
apt-get install libssl-dev
```

**Note**: the above step is not required if installing OpenSSL from
source.

Other flavours of UNIX will use a different package manager (replacing
`apt-get install` with something similar) and use slightly different
package names.

### Installing prerequisites from source

#### OpenSSL

```sh
cd
wget https://www.openssl.org/source/openssl-1.1.1k.tar.gz
wget https://opensslntru.cr.yp.to/openssl-1.1.1k-ntru.patch
tar -xf openssl-1.1.1k.tar.gz
mv openssl-1.1.1k openssl-1.1.1k-ntru
cd openssl-1.1.1k-ntru
patch -p1 < ../openssl-1.1.1k-ntru.patch
export OPENSSLNTRU_PREFIX=$HOME/opensslntru
./config -d shared --prefix=$OPENSSLNTRU_PREFIX --openssldir=$OPENSSLNTRU_PREFIX -Wl,-rpath=$OPENSSLNTRU_PREFIX/lib
make -j8
make test
make install_sw
```

#### `libsntrup761`

```sh
cd
wget https://opensslntru.cr.yp.to/libsntrup761-20210608.tar.gz
tar -xf libsntrup761-20210608.tar.gz
cd libsntrup761-20210608
env CPATH="${OPENSSLNTRU_PREFIX}/include" LIBRARY_PATH="${OPENSSLNTRU_PREFIX}/lib" \
      make USE_RPATH=RUNPATH OPENSSL_PREFIX="${OPENSSLNTRU_PREFIX}" PREFIX="${OPENSSLNTRU_PREFIX}" all install test
```

#### `libsntrup857`

```sh
cd
wget https://opensslntru.cr.yp.to/libsntrup857-20210608.tar.gz
tar -xf libsntrup857-20210608.tar.gz
cd libsntrup857-20210608
env CPATH="${OPENSSLNTRU_PREFIX}/include" LIBRARY_PATH="${OPENSSLNTRU_PREFIX}/lib" \
      make USE_RPATH=RUNPATH OPENSSL_PREFIX="${OPENSSLNTRU_PREFIX}" PREFIX="${OPENSSLNTRU_PREFIX}" all install test
```

### Install engNTRU from sources

```sh
cd
wget https://opensslntru.cr.yp.to/engntru-20210608.tar.gz
cd engntru
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -Dprovider="batch_lib" -DCMAKE_PREFIX_PATH="${OPENSSL_PREFIX}" ..
make
make install
```

## Usage

### List algorithms

```sh
${OPENSSLNTRU_PREFIX}/bin/openssl engine -tt -vvvv -c engntru
```

### TLS test

#### Generate an RSA cert for the server

```sh
${OPENSSLNTRU_PREFIX}/bin/openssl req -x509 -sha256 -nodes -newkey rsa:2048 -keyout /tmp/rsa.key -days 730 -out /tmp/rsa.cert -subj '/C=FI/ST=Uusimaa/L=Helsinki/CN=localhost' -config /etc/ssl/openssl.cnf

```

#### Launch the server

```sh
${OPENSSLNTRU_PREFIX}/bin/openssl s_server -engine engntru \
    -key /tmp/rsa.key -cert /tmp/rsa.cert \
    -tls1_3 -groups "SNTRUP761:SNTRUP857"
```

#### Launch the client

```sh
${OPENSSLNTRU_PREFIX}/bin/openssl s_client -engine engntru \
    -tls1_3 -groups "SNTRUP761"
    # add -no_ticket -reconnect -async for multithread client testing
    # you might want to patch apps/s_client.c to increase the number of reconnections
```

> [`-groups`](https://www.openssl.org/docs/man1.1.1/man3/SSL_CONF_cmd.html)
> takes a colon separated list of groups in order of
> preference, controlling, for clients, what is advertised as supported groups
> (and in TLS 1.3 also what is used in `key_share`) and, in servers, to
> determine which supported group to pick (first shared match).

### ENV variables

#### `ENGNTRU_DEBUG`

In Debug builds of `engNTRU` (i.e., passing `-DCMAKE_BUILD_TYPE=Debug`
at build time), the verbosity level of engNTRU logging output is
controlled by the `ENGNTRU_DEBUG` environment variable. It can be set to
an integer value as detailed in this list:

-   1 (LOG_FATAL)
-   2 (LOG_ERR)
-   3 (LOG_WARN) **default**
-   4 (LOG_INFO)
-   5 (LOG_DBG)
-   6 (LOG_VRB)
-   10 (LOG_EXTRM)

**NOTE**: Using a negative value will set the verbosity level to the
specified absolute value, but will activate _brief debug mode_,
shortening each line prefix.

_Example_: `export ENGNTRU_DEBUG=5` will setup engNTRU to print messages
with priority `LOG_DBG` or higher (highest priority is `LOG_FATAL`).

#### OpenSSL ENV variables

Environment variables relevant to the project include those affecting
OpenSSL, specifically the following ones:

-   `OPENSSL_ENGINES` sets the directory from which engines are loaded
    (the default value can be obtained by `openssl version -e`
-   `OPENSSL_CONF` sets a custom configuration file (the default value
    is `$OPENSSLDIR/openssl.cnf`, which can be inspected with
    `openssl version -d`)

## License

engNTRU is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

engNTRU is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

The full text of the license is contained in the files `COPYING` and
`COPYING.LESSER`.

## Acknowledgments

- This project has received funding from the European Research Council
  (ERC) under the European Union's Horizon 2020 research and
  innovation programme (grant agreement No 804476).

## Links

[OpenSSLNTRU]: <https://opensslntru.cr.yp.to/>
[OpenSSLNTRU patchset]: <https://opensslntru.cr.yp.to/openssl-1.1.1k-ntru.patch>
[NTRU Prime]: <https://ntruprime.cr.yp.to/>
[NIST PQC project]: <https://csrc.nist.gov/Projects/post-quantum-cryptography>
