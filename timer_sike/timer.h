#ifndef TIMER_H_
#define TIMER_H_

// #define CRYPTO_SECRETKEYBYTES OQS_KEM_sike_p434_length_secret_key
// #define CRYPTO_PUBLICKEYBYTES OQS_KEM_sike_p434_length_public_key
// #define CRYPTO_CIPHERTEXTBYTES OQS_KEM_sike_p434_length_ciphertext
// #define CRYPTO_BYTES OQS_KEM_sike_p434_length_shared_secret
// #include <oqs/oqs.h>
// #include <oqs/kem_sike.h>

#include <stdint.h>
#include <string.h>

int preheat(const char * cmd, size_t nb_iterations);

int launch_server_process(void);
int kill_server_process(void);

uint64_t start_measuring_simple_task(const char * cmd, size_t nb_iterations);

unsigned long long connections_loop(const size_t nb_loop,
    const size_t size_list_indices, const size_t * list_indices,
    uintmax_t *list_micro_sec);

int write_mean_measurement(const char * filename, const size_t nb_loop, const uintmax_t total,
    const size_t size_list_indices, const size_t * list_indices,
    uintmax_t *list_micro_sec);

#endif /* TIMER_H_ */
