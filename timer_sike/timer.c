/*
 * Copyright 2021 Nicola Tuveri <nic.tuv@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "timer.h"

#include <openssl/err.h>
#include <openssl/ssl.h>
#include <openssl/engine.h>
#include <openssl/obj_mac.h>
#include <openssl/evp.h>

#include <sys/time.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>

#include "../engntru-20210608/meths/kem_keypair.h"
#include "../engntru-20210608/ossl/engntru_objects.h"

#include <openssl/rand.h>

int preheat(const char * cmd, size_t nb_iterations){
  for(size_t i = 0; i < nb_iterations; i++){
    if(system(cmd) == -1){
      fprintf(stderr, "Error when preheating \n");
      return 1;
    }
  }
  return 0;
}

// To create the server process if no visual connection is needed
int launch_server_process(){
  const char * launch_server = "sudo /home/opensslntru/local/bin/openssl s_server -engine engntru     -key /home/opensslntru/service/test.key -cert /home/opensslntru/service/test.cert     -tls1_3 -groups SIKEP434 &";
  if(system(launch_server) == -1){
    fprintf(stderr, "Error when launching the server\n");
    return 1;
  }
  return 0;
}


// To kill the server process
int kill_server_process(){
  const char * kill_server = "ps aux | grep -i '[s]_server' | awk '{print $2}' | xargs -i{} kill -9 {}";
  if(system(kill_server) == -1){
    fprintf(stderr, "Error when killing the server\n");
    return 1;
  }
  return 0;
}


uint64_t start_measuring_simple_task(const char * cmd, size_t nb_iterations){
  uint64_t ret=0;
  struct timeval start, end;
  gettimeofday(&start, NULL);
  for(size_t i = 0; i < nb_iterations; i++){
    if(system(cmd)==-1){
      fprintf(stderr, "Error when measuring simple task. \n" );
      return 0;
    }
  }
  gettimeofday(&end, NULL);
  ret = (end.tv_sec - start.tv_sec)*1E6 + end.tv_usec - start.tv_usec;

  return ret;
}

int write_mean_measurement(const char * filename, const uintmax_t total,const size_t nb_loop,
    const size_t size_list_indices, const size_t * list_indices,
    uintmax_t *list_micro_sec){
  FILE *file;
  file=fopen(filename, "w");

  if(file==NULL) {
      fprintf(stderr,"Error opening file.");
      return 1;
  }
  else {
    for(size_t i = 0; i< size_list_indices; i++){
      fprintf(file, "Index : %ld , mean time in microsec %ld\n",list_indices[i], list_micro_sec[i] );
    }
    fprintf(file, "Mean of the total time for %ld connections : %ld\n", list_indices[size_list_indices-1],total/nb_loop);
  }
  fclose(file);
  return 0;
}

/*
* Method that executes a certain number of connection and return the elapsed time
*/
unsigned long long connections_loop(const size_t nb_loop,
    const size_t size_list_indices, const size_t * list_indices,
    uintmax_t *list_micro_sec){

    // To retrieve and kill the process of the client (only done when the client is
    // waiting for user input, i.e. the number of connections has been reached)
    const char* kill_client = "ps aux | grep -i '[s]_client' | awk '{print $2}' | xargs -i{} kill -9 {}";
    // To retrieve the state of the client process (and only one '[s]')
    const char* check_client = "ps aux | grep -i '[s]_client'  | awk '{print $8}'";
    // To launch the client In case of async batch  add -async flags
    const char* launch_client = "sudo /home/opensslntru/local/bin/openssl s_client -engine engntru -async -connect 127.0.0.1:4433 -groups SIKEP434 &";

    // For the timer
    struct timeval start, end;
    // To record the total time and the time for this connection
    uintmax_t total = 0;
    uint64_t ret;
    // Where we'll read the state of the client process
    FILE *file;
    char output[1024];

    // Faster to check for equality of strings that checking that one string
    //  contains the other one
    int cmp_client;
    FILE* complete = fopen("measures/measure_test500complete.txt", "w");

    // To look for the complete number of connections
    uint64_t nb_connections = list_indices[size_list_indices-1];

    fprintf(stdout, "Number of connections %ld\n", nb_connections);
    size_t j = 0;

    // Preheating part
    preheat(check_client, 100);

    // Start of the big loop
    for(size_t i = 0; i< nb_connections; i++){

      cmp_client =1;
      // Start the timer for the connection
      gettimeofday(&start, NULL);

      // Launch the client
      if(system(launch_client) ==-1){
        fprintf(stderr, "Error when launching the client. Aborting \n");
        return 1;
      }

      while(1){

        // Check that the client is idle
        file = popen(check_client, "r");
        if (file == NULL){
          pclose(file);
          fprintf(stderr,"Could not run the check command \n");
          return 1;
        }

        // Read the ouptput
        while(fgets(output, sizeof(output), file)!= NULL){
          // Compare the command, if S+ (interruptible sleep) that means it is waiting for user input
          // That means the client is waiting and that the connection has been
          // successfully carried out

          cmp_client = memcmp(output, "S+", 2);
          if (cmp_client ==0){
            // Stop the timer
            gettimeofday(&end, NULL);

            // Needed for completing correctly the handshake with the server
            sleep(1);
            ret = (end.tv_sec - start.tv_sec)*1E6 + end.tv_usec - start.tv_usec;
            total += ret;

            // Add the value to the file complete
            fprintf(complete, "%ld\n",total );

            if(system(kill_client) == -1){
              fprintf(stderr, "Error when killing the client process \n");
              return 0;
            }

            // When it corresponds to an index -> we add a mean to the array
            // list_micro_sec
            if(i == list_indices[j]-1){
              (list_micro_sec)[j]+= (uintmax_t)(total/nb_loop);
              ++j;
            }

            //fprintf(stdout,"\nThe time for creation + handshake is %ld microseconds \n",ret );

            // Test to the content of the array
            // for(size_t i = 0; i<size_list_indices; i++){
            //   fprintf(stderr, "Connection number %ld ; Current value in list_micro_sec %ld\n",list_indices[i], list_micro_sec[i] );
            // }
            break;
          }
        }
        pclose(file);
        break;
      }
    }
    fclose(complete);

    // return the total time for the nb_iteration connectoons
    return total;
}


// First the server must be running before running the timer (it's better otherwise there is some conflicts)
int main(int argc, char **argv)
{
  // Avoid compilation warnings
  (void)argc;
  argv =argv;
  uintmax_t total = 0;
  // Corresponds to the number of times we redo the experiment to get a better
  // probability estimate
  const size_t nb_loop = 1;

  // List of indices representing the #-th connection at which we'll  take a measure
  const size_t list_indices[] = {1,5,10,15,20,25,50,100,150,200,250,300,350,400,450,500};//,550,600,650,700,750,800,850,900,950,1000,1200,1500,2000};//,2500,3000};
  const size_t nb_samples = sizeof(list_indices)/sizeof(list_indices[0]);

  // Allocate memory and set it to 0
  uintmax_t list_micro_sec[nb_samples];
  memset(list_micro_sec, 0, nb_samples*sizeof(uintmax_t));

  // To create the server process if no visual connection is needed
  // if(launch_server_process() == 1){
  //   return 1;
  // }

  // Start the experiments according to the number of repetitions
  for(size_t i = 0; i < nb_loop; i++){
    total += connections_loop(nb_loop, nb_samples, list_indices,list_micro_sec);
  }

  sleep(1);

  // Kill the server process if needed
  // if(kill_server_process() == 1){
  //   return 1;
  // }

  fprintf(stdout, "The experiment ran successfully !\n" );
  fprintf(stdout, "Here is the unbiased time of %lu commands : %ld\n",list_indices[nb_samples-1], total/nb_loop );

  // Write the mean time measurement result in a file
  const char * filename_mean= "measures/measure_test500.txt";
  write_mean_measurement(filename_mean,total, nb_loop, nb_samples, list_indices,list_micro_sec);

  return 0;
}
