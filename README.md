# SIKE implementation in ENGNTRU

Efficiency in Post-Quantum Cryptography:

The objective of this semester was to add SIKE to the engine ENGNTRU to experiment possible optimisations on it.

SIKE is an alternate candidate to the NIST Post-Quantum Cryptography Standardisation currently in the 3rd round.
Here we use its KEM functions to create TLS connections between a server and a client.


# Content of the repository : 
	- engntru : contains all the files of the engine (providers, meths, build, README)
	- libsntrup* : contains the (new) implementation of the NTRU functions (https://opensslntru.cr.yp.to/)
	- timer_sike : contains the file to take the measurement of the TLS handshake times and how to modify it
	- oqs : file containing the implementation of sike (only)
	- liboqs.a : library used to link against the engine, to be able to use SIKE functions, contains SIKE kem functions
	- opensslntru : contains the modified version of OpenSSL to incorporate the engine and the other libraries

################################################
# Step by step installation for engntru
################################################

# 1st Installation
	- Install two VMs
	- Enable the NAT network in both parameters of the VMs and make them share the same network
	- Create the server on one of the VMs by following the following installation lines from https://opensslntru.cr.yp.to/demo.html(this also installs opensslntru, engntru, libsntrup*)
		
	- create the client on the other VM using the instructions of the same web site
		BUT here don't create another user, it just introduces bugs. Do everything as a root. And follow instructions almost one after the other.
		Also the client need to install engntru (not written on the website https://opensslntru.cr.yp.to/demo.html) !
	(the patch for the portable implementation isn't necessary)
	
# 2nd interaction : 

	- Be sure that these variables are set before executing commands in both the server and client VMS (server as opensslntru) and client as root :
		export OPENSSLNTRU_PREFIX=$HOME/local
		export PATH=$OPENSSLNTRU_PREFIX/bin:$PATH

	- You may need to create the key and certificate in the server as **opensslntru** (the first one is only when root -normally don't use it)   : 
		${OPENSSLNTRU_PREFIX}/bin/openssl req -x509 -sha256 -nodes -newkey rsa:2048 -keyout /tmp/rsa.key -days 730 -out /tmp/rsa.cert -subj '/C=FI/ST=Uusimaa/L=Helsinki/CN=localhost' -config /etc/ssl/openssl.cnf
		${OPENSSLNTRU_PREFIX}/bin/openssl req -x509 -sha256 -nodes -newkey rsa:2048 -keyout /home/opensslntru/service/test.key -days 730 -out /home/opensslntru/service/test.cert -subj '/C=FI/ST=Uusimaa/L=Helsinki/CN=localhost' -config /etc/ssl/openssl.cnf
	
	- Launch the server (only with "sudo systemctl restart opensslntru", it actually restarts the server process) 
		or use the following command as opensslntru :
			${OPENSSLNTRU_PREFIX}/bin/openssl s_server -engine engntru     -key /home/opensslntru/service/test.key -cert /home/opensslntru/service/test.cert     -tls1_3 -groups "SNTRUP761:SNTRUP857"

	
	- Launch the client with the following command as root : " ${OPENSSLNTRU_PREFIX}/bin/openssl s_client -engine engntru  -connect 192.168.100.4:65000   -tls1_3 -groups "SNTRUP761" " if the process opensslntru has started, else use 
		${OPENSSLNTRU_PREFIX}/bin/openssl s_client -engine engntru -connect 192.168.100.4 -debug -security_debug   -tls1_3 -groups SNTRUP761


		NOTE : both SNTRUP761 and SNTRUP857 are supported
		NOTE : if it doesn't work anymore after a reboot, it seems that the engntru.so is missing form /usr/lib/x86_64_linux-gnu/engines-1.1/engntru.so
		So I need to copy it there :
		"cp /local/lib/x86_64_linux-gnu/engines-1.1/engntru.so /usr/lib/x86_64_linux-gnu/engines-1.1/engntru.so"
		Need also to activate AXV2 : https://mzh.io/how-to-turn-on-avx2-in-virtualbox/
		NOTE : be sure again that the environment variables are correctly set
		
		
# 3rd installation of liboqs to easily use the SIKE function:
		https://github.com/open-quantum-safe/liboqs
		Download code on the server machine and compile in liboqs/build with : cmake -GNinja -DBUILD_SHARED_LIBS=OFF -DOQS_MINIMAL_BUILD="OQS_ENABLE_KEM_sike_p434" ..
		ninja
		ninja install
		
		This installs the library liboqs.a in /usr/local/lib/liboqs.a
		-- Installing: /usr/local/lib/liboqs.a               <---------
		-- Installing: /usr/local/lib/cmake/liboqs/liboqsConfig.cmake
		-- Installing: /usr/local/lib/cmake/liboqs/liboqsConfig-noconfig.cmake
		-- Installing: /usr/local/include/oqs/oqs.h 		<----- * C programs using liboqs can include just this one file, and it will include all
		* other necessary headers from liboqs.

		-- Installing: /usr/local/include/oqs/common.h
		-- Installing: /usr/local/include/oqs/rand.h
		-- Installing: /usr/local/include/oqs/aes.h
		-- Installing: /usr/local/include/oqs/sha2.h
		-- Installing: /usr/local/include/oqs/sha3.h
		-- Installing: /usr/local/include/oqs/sha3x4.h
		-- Installing: /usr/local/include/oqs/kem.h
		-- Installing: /usr/local/include/oqs/sig.h
		-- Installing: /usr/local/include/oqs/kem_sike.h     <-------
		-- Installing: /usr/local/include/oqs/oqsconfig.h


		Now we need to compile opensslntru with the liboqs.a created and need to include the header kem_sike.h . We do so by add the library and the headers inside the CMakeFile : 
			set(LIBOQS_LIB "/usr/local/lib/liboqs.a")
			target_link_libraries(${ENGINE_NAME} ${LIBOQS_LIB})
			set(LIBOQS_HEADER "/usr/local/include/oqs/")
			target_include_directories(${ENGINE_NAME} PRIVATE ${LIBOQS_HEADER})

# 4th Modify to incorporate SIKEp434 : 

	All modifications are listed inside the README_INCORPORATE_SIKEP434
					
		
# 5th  Build for async_batch_lib or any other providers (batch_lib, serial_lib) : 
		!!! Do not forget to set the environment variables as previously done !! 
		
		Inside the build folder of engntru
		cmake -DCMAKE_BUILD_TYPE=Debug -Dprovider="async_batch_lib" -DCMAKE_PREFIX_PATH="${OPENSSL_PREFIX}" ..
		make
		make install
		
		
# 6th : to make SIKEP434 work :


	ATTENTION where to put the folder "bin/openssl" which contains the modified version ?? 
	So it seems that the folder of interest is in opensslntru-1.1.1l-ntru, we need to modify those files and those changes will be ported to local/include/openssl
	
	export OPENSSLNTRU_PREFIX=$HOME/local
	export PATH=$OPENSSLNTRU_PREFIX/bin:$PATH
	
	Modifying openssl
		cd openssl-1.1.1l-ntru/
		./config -d shared --prefix=$OPENSSLNTRU_PREFIX --openssldir=$OPENSSLNTRU_PREFIX -Wl,-rpath=$OPENSSLNTRU_PREFIX/lib
		make -j8
		make test
		make install_sw
	
	Compile engntru: 
		For both the server and the client : 
			Inside the build folder of engntru
			cmake -DCMAKE_BUILD_TYPE=Debug -Dprovider="async_batch_lib" -DCMAKE_PREFIX_PATH="${OPENSSL_PREFIX}" ..
			make
			make install


	Use the commands :
		Server works well  : 
			${OPENSSLNTRU_PREFIX}/bin/openssl s_server -engine engntru     -key /home/opensslntru/service/test.key -cert /home/opensslntru/service/test.cert     -tls1_3 -groups SIKEP434

		Client works well with : 
		${OPENSSLNTRU_PREFIX}/bin/openssl s_client -engine engntru -connect 192.168.100.4 -debug -security_debug   -tls1_3 -groups SIKEP434
		
		[HERE replace "192.168.100.4" with the address of the server that was assigned]
	


==========> WORKS
	Also in the server terminal : /home/opensslntru/local/bin/openssl engine -tt -vvvv -c engntru => to check that SIKEP434 has been added
		

# For the time measurements
	Look at the folder timer_sike and its README
