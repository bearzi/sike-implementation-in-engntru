#include "stdio.h"

#include "crypto_core_multbigsntrup857.h"
#include "crypto_core_testmultbig.h"

//#include "crypto_core_testmulbig2.h"

//#define crypto_core_multbigsntrup857 crypto_core_multbigsntrup857_ref
//#define crypto_core_multbigsntrup857_OUTPUTBYTES crypto_core_multbigsntrup857_ref_OUTPUTBYTES
//#define crypto_core_multbigsntrup857_INPUTBYTES crypto_core_multbigsntrup857_ref_INPUTBYTES
//#define crypto_core_multbigsntrup857_KEYBYTES crypto_core_multbigsntrup857_ref_KEYBYTES



#define LEN crypto_core_multbigsntrup857_OUTPUTBYTES

#define FUNC1 crypto_core_multbigsntrup857
#define FUNC2 crypto_core_testmultbig
//#define FUNC3 crypto_core_testmulbig2


#include "stdint.h"

uint8_t vec_diff(const uint8_t *a, const uint8_t* b , int len )
{
  uint8_t r = 0;
  for(int i=0;i<len;i++) {
    r |= a[i]^b[i];
    //if( r ) { printf("diff: [%d]: %x.\n", i , r ); break; }
  }
  return r;
}

void vec_dump(const char *c, const int16_t *v, int len)
{
  printf("%s, ", c );
  for(int i=0;i<len;i++) { printf("%d,",v[i]); if(3==i%4) printf(" "); }
  printf("\n");
}

#define q 5167

int16_t vec_diff_mod5167(const int16_t *a, const int16_t *b, int len)
{
  int16_t r = 0;
  for(int i=0;i<len;i++){
    int16_t pa = ((a[i]%q)+q)%q;
    int16_t pb = ((b[i]%q)+q)%q;
    //if( pa ^ pb ) { printf("[%d] a: %d:%d, b: %d:%d\n", i , a[i], pa , b[i], pb ); }
    r |= pa^pb;
  }
  return r;
}


int main()
{
  unsigned char output0[LEN] = {0};
  unsigned char output1[LEN] = {0};
  unsigned char input[LEN] = {0};
  unsigned char keybytes[LEN] = {0};

for(int j=0;j<10;j++) {
  //for(int i=0;i<LEN;i++) {
  uint16_t *inp0 = (uint16_t*)input;
  uint16_t *inp1 = (uint16_t*)keybytes;
  //for(int i=0;i<768;i++) {
  for(int i=0;i<857;i++) {
    inp0[i]=(j*7+3+i)*13 % 2500;
    inp1[i]=(j*11+7+i)*17 % 2500;
  }
  //inp1[0] = 1; for(int i=1;i<768;i++) inp1[i] = 0;
  //for(int i=2;i<768;i++) inp0[i] = 0;
  //for(int i=0;i<768;i++) {
  //  inp0[i]=(3+i)*13 % 2500;
  //  inp1[i]=(3+i)*17 % 2500;
  //}

  FUNC1( output0 , input , keybytes , NULL );
  FUNC2( output1 , input , keybytes , NULL );

  //uint8_t diff = vec_diff( output0 , output1 , LEN );
  //printf("[%d] test: out diff: %x\n", j , diff );

  uint16_t diff16 = vec_diff_mod5167( (int16_t*)output0 , (int16_t*)output1 , 857 );
  printf("[%d] is congruent: out diff: %x\n", j , diff16 );
  if( diff16 ) {
    vec_dump( "inp0" , inp0 , 16 );
    vec_dump( "inp1" , inp1 , 16 );
    vec_dump( "out0" , (int16_t*)output0 , 16 );
    vec_dump( "out1" , (int16_t*)output1 , 16 );
  }
}

  return 0;
}
