#ifndef crypto_verify_1184_H
#define crypto_verify_1184_H

#define crypto_verify_1184_avx_BYTES 1184

#define crypto_verify_1184 crypto_verify_1184_avx
#define crypto_verify_1184_BYTES crypto_verify_1184_avx_BYTES

#ifdef __cplusplus
extern "C" {
#endif
extern int crypto_verify_1184(const unsigned char *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_verify_1184_IMPLEMENTATION "crypto_verify/1184/avx"
#define crypto_verify_1184_implementation "crypto_verify/1184/avx"
#ifndef crypto_verify_1184_avx_VERSION
#define crypto_verify_1184_avx_VERSION "-"
#endif
#define crypto_verify_1184_VERSION crypto_verify_1184_avx_VERSION

#endif
