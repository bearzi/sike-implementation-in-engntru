#ifndef crypto_core_mult3sntrup857_H
#define crypto_core_mult3sntrup857_H

#define crypto_core_mult3sntrup857_avx800_OUTPUTBYTES 857
#define crypto_core_mult3sntrup857_avx800_INPUTBYTES 857
#define crypto_core_mult3sntrup857_avx800_KEYBYTES 857
#define crypto_core_mult3sntrup857_avx800_CONSTBYTES 0

#define crypto_core_mult3sntrup857 crypto_core_mult3sntrup857_avx800
#define crypto_core_mult3sntrup857_OUTPUTBYTES crypto_core_mult3sntrup857_avx800_OUTPUTBYTES
#define crypto_core_mult3sntrup857_INPUTBYTES crypto_core_mult3sntrup857_avx800_INPUTBYTES
#define crypto_core_mult3sntrup857_KEYBYTES crypto_core_mult3sntrup857_avx800_KEYBYTES
#define crypto_core_mult3sntrup857_CONSTBYTES crypto_core_mult3sntrup857_avx800_CONSTBYTES

#ifdef __cplusplus
extern "C" {
#endif
extern int crypto_core_mult3sntrup857(unsigned char *,const unsigned char *,const unsigned char *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_core_mult3sntrup857_IMPLEMENTATION "crypto_core/mult3sntrup857/avx800"
#define crypto_core_mult3sntrup857_implementation "crypto_core/mult3sntrup857/avx800"
#ifndef crypto_core_mult3sntrup857_avx800_VERSION
#define crypto_core_mult3sntrup857_avx800_VERSION "-"
#endif
#define crypto_core_mult3sntrup857_VERSION crypto_core_mult3sntrup857_avx800_VERSION

#endif
