#ifndef crypto_decode_857x1723_H
#define crypto_decode_857x1723_H

#define crypto_decode_857x1723_avx_STRBYTES 1152
#define crypto_decode_857x1723_avx_ITEMBYTES 2
#define crypto_decode_857x1723_avx_ITEMS 857

#define crypto_decode_857x1723 crypto_decode_857x1723_avx
#define crypto_decode_857x1723_STRBYTES crypto_decode_857x1723_avx_STRBYTES
#define crypto_decode_857x1723_ITEMBYTES crypto_decode_857x1723_avx_ITEMBYTES
#define crypto_decode_857x1723_ITEMS crypto_decode_857x1723_avx_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_decode_857x1723(void *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_decode_857x1723_IMPLEMENTATION "crypto_decode/857x1723/avx"
#define crypto_decode_857x1723_implementation "crypto_decode/857x1723/avx"
#ifndef crypto_decode_857x1723_avx_VERSION
#define crypto_decode_857x1723_avx_VERSION "-"
#endif
#define crypto_decode_857x1723_VERSION crypto_decode_857x1723_avx_VERSION

#endif
