#ifndef crypto_core_multsntrup857_H
#define crypto_core_multsntrup857_H

#define crypto_core_multsntrup857_avx800_OUTPUTBYTES 1714
#define crypto_core_multsntrup857_avx800_INPUTBYTES 1714
#define crypto_core_multsntrup857_avx800_KEYBYTES 857
#define crypto_core_multsntrup857_avx800_CONSTBYTES 0

#define crypto_core_multsntrup857 crypto_core_multsntrup857_avx800
#define crypto_core_multsntrup857_OUTPUTBYTES crypto_core_multsntrup857_avx800_OUTPUTBYTES
#define crypto_core_multsntrup857_INPUTBYTES crypto_core_multsntrup857_avx800_INPUTBYTES
#define crypto_core_multsntrup857_KEYBYTES crypto_core_multsntrup857_avx800_KEYBYTES
#define crypto_core_multsntrup857_CONSTBYTES crypto_core_multsntrup857_avx800_CONSTBYTES

#ifdef __cplusplus
extern "C" {
#endif
extern int crypto_core_multsntrup857(unsigned char *,const unsigned char *,const unsigned char *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_core_multsntrup857_IMPLEMENTATION "crypto_core/multsntrup857/avx800"
#define crypto_core_multsntrup857_implementation "crypto_core/multsntrup857/avx800"
#ifndef crypto_core_multsntrup857_avx800_VERSION
#define crypto_core_multsntrup857_avx800_VERSION "-"
#endif
#define crypto_core_multsntrup857_VERSION crypto_core_multsntrup857_avx800_VERSION

#endif
