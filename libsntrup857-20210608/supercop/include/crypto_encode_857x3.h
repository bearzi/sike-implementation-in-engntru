#ifndef crypto_encode_857x3_H
#define crypto_encode_857x3_H

#define crypto_encode_857x3_avx_STRBYTES 215
#define crypto_encode_857x3_avx_ITEMBYTES 1
#define crypto_encode_857x3_avx_ITEMS 857

#define crypto_encode_857x3 crypto_encode_857x3_avx
#define crypto_encode_857x3_STRBYTES crypto_encode_857x3_avx_STRBYTES
#define crypto_encode_857x3_ITEMBYTES crypto_encode_857x3_avx_ITEMBYTES
#define crypto_encode_857x3_ITEMS crypto_encode_857x3_avx_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_encode_857x3(unsigned char *,const void *);
#ifdef __cplusplus
}
#endif

#define crypto_encode_857x3_IMPLEMENTATION "crypto_encode/857x3/avx"
#define crypto_encode_857x3_implementation "crypto_encode/857x3/avx"
#ifndef crypto_encode_857x3_avx_VERSION
#define crypto_encode_857x3_avx_VERSION "-"
#endif
#define crypto_encode_857x3_VERSION crypto_encode_857x3_avx_VERSION

#endif
