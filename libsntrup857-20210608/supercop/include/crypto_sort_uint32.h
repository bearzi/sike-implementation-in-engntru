#ifndef crypto_sort_uint32_H
#define crypto_sort_uint32_H

#define crypto_sort_uint32_useint32_BYTES 4

#define crypto_sort_uint32 crypto_sort_uint32_useint32
#define crypto_sort_uint32_BYTES crypto_sort_uint32_useint32_BYTES

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_sort_uint32(void *,long long);
#ifdef __cplusplus
}
#endif

#define crypto_sort_uint32_IMPLEMENTATION "crypto_sort/uint32/useint32"
#define crypto_sort_uint32_implementation "crypto_sort/uint32/useint32"
#ifndef crypto_sort_uint32_useint32_VERSION
#define crypto_sort_uint32_useint32_VERSION "-"
#endif
#define crypto_sort_uint32_VERSION crypto_sort_uint32_useint32_VERSION

#endif
