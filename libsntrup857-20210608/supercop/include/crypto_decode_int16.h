#ifndef crypto_decode_int16_H
#define crypto_decode_int16_H

#define crypto_decode_int16_ref_STRBYTES 2
#define crypto_decode_int16_ref_ITEMBYTES 2
#define crypto_decode_int16_ref_ITEMS 1

#define crypto_decode_int16 crypto_decode_int16_ref
#define crypto_decode_int16_STRBYTES crypto_decode_int16_ref_STRBYTES
#define crypto_decode_int16_ITEMBYTES crypto_decode_int16_ref_ITEMBYTES
#define crypto_decode_int16_ITEMS crypto_decode_int16_ref_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_decode_int16(void *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_decode_int16_IMPLEMENTATION "crypto_decode/int16/ref"
#define crypto_decode_int16_implementation "crypto_decode/int16/ref"
#ifndef crypto_decode_int16_ref_VERSION
#define crypto_decode_int16_ref_VERSION "-"
#endif
#define crypto_decode_int16_VERSION crypto_decode_int16_ref_VERSION

#endif
