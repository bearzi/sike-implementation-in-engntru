#ifndef crypto_sort_H
#define crypto_sort_H

#include "crypto_sort_uint32.h"
#define crypto_sort crypto_sort_uint32
#define crypto_sort_BYTES crypto_sort_uint32_BYTES

#define crypto_sort_PRIMITIVE "uint32"
#define crypto_sort_implementation crypto_sort_uint32_implementation
#define crypto_sort_IMPLEMENTATION crypto_sort_uint32_IMPLEMENTATION

#endif
