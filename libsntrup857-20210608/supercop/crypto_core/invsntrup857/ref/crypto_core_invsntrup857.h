#ifndef crypto_core_invsntrup857_H
#define crypto_core_invsntrup857_H

#define crypto_core_invsntrup857_ref_OUTPUTBYTES 1715
#define crypto_core_invsntrup857_ref_INPUTBYTES 857
#define crypto_core_invsntrup857_ref_KEYBYTES 0
#define crypto_core_invsntrup857_ref_CONSTBYTES 0

#define crypto_core_invsntrup857 crypto_core_invsntrup857_ref
#define crypto_core_invsntrup857_OUTPUTBYTES crypto_core_invsntrup857_ref_OUTPUTBYTES
#define crypto_core_invsntrup857_INPUTBYTES crypto_core_invsntrup857_ref_INPUTBYTES
#define crypto_core_invsntrup857_KEYBYTES crypto_core_invsntrup857_ref_KEYBYTES
#define crypto_core_invsntrup857_CONSTBYTES crypto_core_invsntrup857_ref_CONSTBYTES

#ifdef __cplusplus
extern "C" {
#endif
extern int crypto_core_invsntrup857(unsigned char *,const unsigned char *,const unsigned char *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_core_invsntrup857_IMPLEMENTATION "crypto_core/invsntrup857/ref"
#define crypto_core_invsntrup857_implementation "crypto_core/invsntrup857/ref"
#ifndef crypto_core_invsntrup857_ref_VERSION
#define crypto_core_invsntrup857_ref_VERSION "-"
#endif
#define crypto_core_invsntrup857_VERSION crypto_core_invsntrup857_ref_VERSION

#endif
