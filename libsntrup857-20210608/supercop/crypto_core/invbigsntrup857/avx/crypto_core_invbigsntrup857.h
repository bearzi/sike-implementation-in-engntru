#ifndef crypto_core_invbigsntrup857_H
#define crypto_core_invbigsntrup857_H

#define crypto_core_invbigsntrup857_avx_OUTPUTBYTES 1715
#define crypto_core_invbigsntrup857_avx_INPUTBYTES 1714
#define crypto_core_invbigsntrup857_avx_KEYBYTES 0
#define crypto_core_invbigsntrup857_avx_CONSTBYTES 0

#define crypto_core_invbigsntrup857 crypto_core_invbigsntrup857_avx
#define crypto_core_invbigsntrup857_OUTPUTBYTES crypto_core_invbigsntrup857_avx_OUTPUTBYTES
#define crypto_core_invbigsntrup857_INPUTBYTES crypto_core_invbigsntrup857_avx_INPUTBYTES
#define crypto_core_invbigsntrup857_KEYBYTES crypto_core_invbigsntrup857_avx_KEYBYTES
#define crypto_core_invbigsntrup857_CONSTBYTES crypto_core_invbigsntrup857_avx_CONSTBYTES

#ifdef __cplusplus
extern "C" {
#endif
extern int crypto_core_invbigsntrup857(unsigned char *,const unsigned char *,const unsigned char *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_core_invbigsntrup857_IMPLEMENTATION "crypto_core/invbigsntrup857/avx"
#define crypto_core_invbigsntrup857_implementation "crypto_core/invbigsntrup857/avx"
#ifndef crypto_core_invbigsntrup857_avx_VERSION
#define crypto_core_invbigsntrup857_avx_VERSION "-"
#endif
#define crypto_core_invbigsntrup857_VERSION crypto_core_invbigsntrup857_avx_VERSION

#endif
