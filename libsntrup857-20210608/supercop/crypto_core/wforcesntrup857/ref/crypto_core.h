#ifndef crypto_core_H
#define crypto_core_H

#include "crypto_core_wforcesntrup857.h"
#define crypto_core crypto_core_wforcesntrup857
#define crypto_core_OUTPUTBYTES crypto_core_wforcesntrup857_OUTPUTBYTES
#define crypto_core_INPUTBYTES crypto_core_wforcesntrup857_INPUTBYTES
#define crypto_core_KEYBYTES crypto_core_wforcesntrup857_KEYBYTES
#define crypto_core_CONSTBYTES crypto_core_wforcesntrup857_CONSTBYTES

#define crypto_core_PRIMITIVE "wforcesntrup857"
#define crypto_core_implementation crypto_core_wforcesntrup857_implementation
#define crypto_core_IMPLEMENTATION crypto_core_wforcesntrup857_IMPLEMENTATION

#endif
