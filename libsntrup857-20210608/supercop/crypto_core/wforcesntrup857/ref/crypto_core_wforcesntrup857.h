#ifndef crypto_core_wforcesntrup857_H
#define crypto_core_wforcesntrup857_H

#define crypto_core_wforcesntrup857_ref_OUTPUTBYTES 857
#define crypto_core_wforcesntrup857_ref_INPUTBYTES 857
#define crypto_core_wforcesntrup857_ref_KEYBYTES 0
#define crypto_core_wforcesntrup857_ref_CONSTBYTES 0

#define crypto_core_wforcesntrup857 crypto_core_wforcesntrup857_ref
#define crypto_core_wforcesntrup857_OUTPUTBYTES crypto_core_wforcesntrup857_ref_OUTPUTBYTES
#define crypto_core_wforcesntrup857_INPUTBYTES crypto_core_wforcesntrup857_ref_INPUTBYTES
#define crypto_core_wforcesntrup857_KEYBYTES crypto_core_wforcesntrup857_ref_KEYBYTES
#define crypto_core_wforcesntrup857_CONSTBYTES crypto_core_wforcesntrup857_ref_CONSTBYTES

#ifdef __cplusplus
extern "C" {
#endif
extern int crypto_core_wforcesntrup857(unsigned char *,const unsigned char *,const unsigned char *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_core_wforcesntrup857_IMPLEMENTATION "crypto_core/wforcesntrup857/ref"
#define crypto_core_wforcesntrup857_implementation "crypto_core/wforcesntrup857/ref"
#ifndef crypto_core_wforcesntrup857_ref_VERSION
#define crypto_core_wforcesntrup857_ref_VERSION "-"
#endif
#define crypto_core_wforcesntrup857_VERSION crypto_core_wforcesntrup857_ref_VERSION

#endif
