#ifndef crypto_decode_857x3_H
#define crypto_decode_857x3_H

#define crypto_decode_857x3_avx_STRBYTES 215
#define crypto_decode_857x3_avx_ITEMBYTES 1
#define crypto_decode_857x3_avx_ITEMS 857

#define crypto_decode_857x3 crypto_decode_857x3_avx
#define crypto_decode_857x3_STRBYTES crypto_decode_857x3_avx_STRBYTES
#define crypto_decode_857x3_ITEMBYTES crypto_decode_857x3_avx_ITEMBYTES
#define crypto_decode_857x3_ITEMS crypto_decode_857x3_avx_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_decode_857x3(void *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_decode_857x3_IMPLEMENTATION "crypto_decode/857x3/avx"
#define crypto_decode_857x3_implementation "crypto_decode/857x3/avx"
#ifndef crypto_decode_857x3_avx_VERSION
#define crypto_decode_857x3_avx_VERSION "-"
#endif
#define crypto_decode_857x3_VERSION crypto_decode_857x3_avx_VERSION

#endif
