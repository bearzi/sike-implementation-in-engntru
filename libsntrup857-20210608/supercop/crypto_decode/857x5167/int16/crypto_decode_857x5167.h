#ifndef crypto_decode_857x5167_H
#define crypto_decode_857x5167_H

#define crypto_decode_857x5167_int16_STRBYTES 1322
#define crypto_decode_857x5167_int16_ITEMBYTES 2
#define crypto_decode_857x5167_int16_ITEMS 857

#define crypto_decode_857x5167 crypto_decode_857x5167_int16
#define crypto_decode_857x5167_STRBYTES crypto_decode_857x5167_int16_STRBYTES
#define crypto_decode_857x5167_ITEMBYTES crypto_decode_857x5167_int16_ITEMBYTES
#define crypto_decode_857x5167_ITEMS crypto_decode_857x5167_int16_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_decode_857x5167(void *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_decode_857x5167_IMPLEMENTATION "crypto_decode/857x5167/int16"
#define crypto_decode_857x5167_implementation "crypto_decode/857x5167/int16"
#ifndef crypto_decode_857x5167_int16_VERSION
#define crypto_decode_857x5167_int16_VERSION "-"
#endif
#define crypto_decode_857x5167_VERSION crypto_decode_857x5167_int16_VERSION

#endif
