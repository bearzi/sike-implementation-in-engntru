#ifndef crypto_decode_857x5167_H
#define crypto_decode_857x5167_H

#define crypto_decode_857x5167_portable_STRBYTES 1322
#define crypto_decode_857x5167_portable_ITEMBYTES 2
#define crypto_decode_857x5167_portable_ITEMS 857

#define crypto_decode_857x5167 crypto_decode_857x5167_portable
#define crypto_decode_857x5167_STRBYTES crypto_decode_857x5167_portable_STRBYTES
#define crypto_decode_857x5167_ITEMBYTES crypto_decode_857x5167_portable_ITEMBYTES
#define crypto_decode_857x5167_ITEMS crypto_decode_857x5167_portable_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_decode_857x5167(void *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_decode_857x5167_IMPLEMENTATION "crypto_decode/857x5167/portable"
#define crypto_decode_857x5167_implementation "crypto_decode/857x5167/portable"
#ifndef crypto_decode_857x5167_portable_VERSION
#define crypto_decode_857x5167_portable_VERSION "-"
#endif
#define crypto_decode_857x5167_VERSION crypto_decode_857x5167_portable_VERSION

#endif
