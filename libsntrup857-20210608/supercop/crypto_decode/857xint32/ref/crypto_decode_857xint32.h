#ifndef crypto_decode_857xint32_H
#define crypto_decode_857xint32_H

#define crypto_decode_857xint32_ref_STRBYTES 3428
#define crypto_decode_857xint32_ref_ITEMBYTES 4
#define crypto_decode_857xint32_ref_ITEMS 857

#define crypto_decode_857xint32 crypto_decode_857xint32_ref
#define crypto_decode_857xint32_STRBYTES crypto_decode_857xint32_ref_STRBYTES
#define crypto_decode_857xint32_ITEMBYTES crypto_decode_857xint32_ref_ITEMBYTES
#define crypto_decode_857xint32_ITEMS crypto_decode_857xint32_ref_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_decode_857xint32(void *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_decode_857xint32_IMPLEMENTATION "crypto_decode/857xint32/ref"
#define crypto_decode_857xint32_implementation "crypto_decode/857xint32/ref"
#ifndef crypto_decode_857xint32_ref_VERSION
#define crypto_decode_857xint32_ref_VERSION "-"
#endif
#define crypto_decode_857xint32_VERSION crypto_decode_857xint32_ref_VERSION

#endif
