#ifndef crypto_decode_857x1723_H
#define crypto_decode_857x1723_H

#define crypto_decode_857x1723_portable_STRBYTES 1152
#define crypto_decode_857x1723_portable_ITEMBYTES 2
#define crypto_decode_857x1723_portable_ITEMS 857

#define crypto_decode_857x1723 crypto_decode_857x1723_portable
#define crypto_decode_857x1723_STRBYTES crypto_decode_857x1723_portable_STRBYTES
#define crypto_decode_857x1723_ITEMBYTES crypto_decode_857x1723_portable_ITEMBYTES
#define crypto_decode_857x1723_ITEMS crypto_decode_857x1723_portable_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_decode_857x1723(void *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_decode_857x1723_IMPLEMENTATION "crypto_decode/857x1723/portable"
#define crypto_decode_857x1723_implementation "crypto_decode/857x1723/portable"
#ifndef crypto_decode_857x1723_portable_VERSION
#define crypto_decode_857x1723_portable_VERSION "-"
#endif
#define crypto_decode_857x1723_VERSION crypto_decode_857x1723_portable_VERSION

#endif
