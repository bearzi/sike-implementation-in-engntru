#ifndef crypto_decode_857x1723_H
#define crypto_decode_857x1723_H

#define crypto_decode_857x1723_int16_STRBYTES 1152
#define crypto_decode_857x1723_int16_ITEMBYTES 2
#define crypto_decode_857x1723_int16_ITEMS 857

#define crypto_decode_857x1723 crypto_decode_857x1723_int16
#define crypto_decode_857x1723_STRBYTES crypto_decode_857x1723_int16_STRBYTES
#define crypto_decode_857x1723_ITEMBYTES crypto_decode_857x1723_int16_ITEMBYTES
#define crypto_decode_857x1723_ITEMS crypto_decode_857x1723_int16_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_decode_857x1723(void *,const unsigned char *);
#ifdef __cplusplus
}
#endif

#define crypto_decode_857x1723_IMPLEMENTATION "crypto_decode/857x1723/int16"
#define crypto_decode_857x1723_implementation "crypto_decode/857x1723/int16"
#ifndef crypto_decode_857x1723_int16_VERSION
#define crypto_decode_857x1723_int16_VERSION "-"
#endif
#define crypto_decode_857x1723_VERSION crypto_decode_857x1723_int16_VERSION

#endif
