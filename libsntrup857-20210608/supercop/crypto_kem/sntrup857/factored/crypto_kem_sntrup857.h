#ifndef crypto_kem_sntrup857_H
#define crypto_kem_sntrup857_H

#define crypto_kem_sntrup857_factored_PUBLICKEYBYTES 1322
#define crypto_kem_sntrup857_factored_SECRETKEYBYTES 1999
#define crypto_kem_sntrup857_factored_BYTES 32
#define crypto_kem_sntrup857_factored_CIPHERTEXTBYTES 1184

#define crypto_kem_sntrup857_keypair crypto_kem_sntrup857_factored_keypair
#define crypto_kem_sntrup857_keypair_batch crypto_kem_sntrup857_factored_keypair_batch
#define crypto_kem_sntrup857_enc crypto_kem_sntrup857_factored_enc
#define crypto_kem_sntrup857_dec crypto_kem_sntrup857_factored_dec
#define crypto_kem_sntrup857_PUBLICKEYBYTES crypto_kem_sntrup857_factored_PUBLICKEYBYTES
#define crypto_kem_sntrup857_SECRETKEYBYTES crypto_kem_sntrup857_factored_SECRETKEYBYTES
#define crypto_kem_sntrup857_BYTES crypto_kem_sntrup857_factored_BYTES
#define crypto_kem_sntrup857_CIPHERTEXTBYTES crypto_kem_sntrup857_factored_CIPHERTEXTBYTES

#ifdef __cplusplus
extern "C" {
#endif
extern int crypto_kem_sntrup857_keypair(unsigned char *,unsigned char *) __attribute__((visibility("default")));
extern int crypto_kem_sntrup857_keypair_batch(unsigned char *pk,unsigned char *sk, unsigned num) __attribute__((visibility("default")));
extern int crypto_kem_sntrup857_enc(unsigned char *,unsigned char *,const unsigned char *) __attribute__((visibility("default")));
extern int crypto_kem_sntrup857_dec(unsigned char *,const unsigned char *,const unsigned char *) __attribute__((visibility("default")));
#ifdef __cplusplus
}
#endif

#define crypto_kem_sntrup857_IMPLEMENTATION "crypto_kem/sntrup857/factored"
#define crypto_kem_sntrup857_implementation "crypto_kem/sntrup857/factored"
#ifndef crypto_kem_sntrup857_factored_VERSION
#define crypto_kem_sntrup857_factored_VERSION "-"
#endif
#define crypto_kem_sntrup857_VERSION crypto_kem_sntrup857_factored_VERSION

#endif
