#ifndef crypto_kem_sntrup857_H
#define crypto_kem_sntrup857_H

#define crypto_kem_sntrup857_avx_PUBLICKEYBYTES 1322
#define crypto_kem_sntrup857_avx_SECRETKEYBYTES 1999
#define crypto_kem_sntrup857_avx_BYTES 32
#define crypto_kem_sntrup857_avx_CIPHERTEXTBYTES 1184

#define crypto_kem_sntrup857_keypair crypto_kem_sntrup857_avx_keypair
#define crypto_kem_sntrup857_keypair_batch crypto_kem_sntrup857_avx_keypair_batch
#define crypto_kem_sntrup857_enc crypto_kem_sntrup857_avx_enc
#define crypto_kem_sntrup857_dec crypto_kem_sntrup857_avx_dec
#define crypto_kem_sntrup857_PUBLICKEYBYTES crypto_kem_sntrup857_avx_PUBLICKEYBYTES
#define crypto_kem_sntrup857_SECRETKEYBYTES crypto_kem_sntrup857_avx_SECRETKEYBYTES
#define crypto_kem_sntrup857_BYTES crypto_kem_sntrup857_avx_BYTES
#define crypto_kem_sntrup857_CIPHERTEXTBYTES crypto_kem_sntrup857_avx_CIPHERTEXTBYTES

#ifdef __cplusplus
extern "C" {
#endif
extern int crypto_kem_sntrup857_keypair(unsigned char *,unsigned char *) __attribute__((visibility("default")));
extern int crypto_kem_sntrup857_keypair_batch(unsigned char *pk,unsigned char *sk, unsigned num) __attribute__((visibility("default")));
extern int crypto_kem_sntrup857_enc(unsigned char *,unsigned char *,const unsigned char *) __attribute__((visibility("default")));
extern int crypto_kem_sntrup857_dec(unsigned char *,const unsigned char *,const unsigned char *) __attribute__((visibility("default")));
#ifdef __cplusplus
}
#endif

#define crypto_kem_sntrup857_IMPLEMENTATION "crypto_kem/sntrup857/avx"
#define crypto_kem_sntrup857_implementation "crypto_kem/sntrup857/avx"
#ifndef crypto_kem_sntrup857_avx_VERSION
#define crypto_kem_sntrup857_avx_VERSION "-"
#endif
#define crypto_kem_sntrup857_VERSION crypto_kem_sntrup857_avx_VERSION

#endif
