#ifndef crypto_rng_H
#define crypto_rng_H

#include "crypto_rng_aes256.h"
#define crypto_rng crypto_rng_aes256
#define crypto_rng_KEYBYTES crypto_rng_aes256_KEYBYTES
#define crypto_rng_OUTPUTBYTES crypto_rng_aes256_OUTPUTBYTES

#define crypto_rng_PRIMITIVE "aes256"
#define crypto_rng_implementation crypto_rng_aes256_implementation
#define crypto_rng_IMPLEMENTATION crypto_rng_aes256_IMPLEMENTATION

#endif
