#ifndef crypto_verify_H
#define crypto_verify_H

#include "crypto_verify_1184.h"
#define crypto_verify crypto_verify_1184
#define crypto_verify_BYTES crypto_verify_1184_BYTES

#define crypto_verify_PRIMITIVE "1184"
#define crypto_verify_implementation crypto_verify_1184_implementation
#define crypto_verify_IMPLEMENTATION crypto_verify_1184_IMPLEMENTATION

#endif
