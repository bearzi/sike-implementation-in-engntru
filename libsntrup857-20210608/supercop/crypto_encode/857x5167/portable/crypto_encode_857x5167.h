#ifndef crypto_encode_857x5167_H
#define crypto_encode_857x5167_H

#define crypto_encode_857x5167_portable_STRBYTES 1322
#define crypto_encode_857x5167_portable_ITEMBYTES 2
#define crypto_encode_857x5167_portable_ITEMS 857

#define crypto_encode_857x5167 crypto_encode_857x5167_portable
#define crypto_encode_857x5167_STRBYTES crypto_encode_857x5167_portable_STRBYTES
#define crypto_encode_857x5167_ITEMBYTES crypto_encode_857x5167_portable_ITEMBYTES
#define crypto_encode_857x5167_ITEMS crypto_encode_857x5167_portable_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_encode_857x5167(unsigned char *,const void *);
#ifdef __cplusplus
}
#endif

#define crypto_encode_857x5167_IMPLEMENTATION "crypto_encode/857x5167/portable"
#define crypto_encode_857x5167_implementation "crypto_encode/857x5167/portable"
#ifndef crypto_encode_857x5167_portable_VERSION
#define crypto_encode_857x5167_portable_VERSION "-"
#endif
#define crypto_encode_857x5167_VERSION crypto_encode_857x5167_portable_VERSION

#endif
