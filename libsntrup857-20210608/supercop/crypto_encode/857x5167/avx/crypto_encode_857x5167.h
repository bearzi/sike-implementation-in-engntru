#ifndef crypto_encode_857x5167_H
#define crypto_encode_857x5167_H

#define crypto_encode_857x5167_avx_STRBYTES 1322
#define crypto_encode_857x5167_avx_ITEMBYTES 2
#define crypto_encode_857x5167_avx_ITEMS 857

#define crypto_encode_857x5167 crypto_encode_857x5167_avx
#define crypto_encode_857x5167_STRBYTES crypto_encode_857x5167_avx_STRBYTES
#define crypto_encode_857x5167_ITEMBYTES crypto_encode_857x5167_avx_ITEMBYTES
#define crypto_encode_857x5167_ITEMS crypto_encode_857x5167_avx_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_encode_857x5167(unsigned char *,const void *);
#ifdef __cplusplus
}
#endif

#define crypto_encode_857x5167_IMPLEMENTATION "crypto_encode/857x5167/avx"
#define crypto_encode_857x5167_implementation "crypto_encode/857x5167/avx"
#ifndef crypto_encode_857x5167_avx_VERSION
#define crypto_encode_857x5167_avx_VERSION "-"
#endif
#define crypto_encode_857x5167_VERSION crypto_encode_857x5167_avx_VERSION

#endif
