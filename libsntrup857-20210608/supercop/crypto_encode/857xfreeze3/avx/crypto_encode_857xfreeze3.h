#ifndef crypto_encode_857xfreeze3_H
#define crypto_encode_857xfreeze3_H

#define crypto_encode_857xfreeze3_avx_STRBYTES 857
#define crypto_encode_857xfreeze3_avx_ITEMBYTES 2
#define crypto_encode_857xfreeze3_avx_ITEMS 857

#define crypto_encode_857xfreeze3 crypto_encode_857xfreeze3_avx
#define crypto_encode_857xfreeze3_STRBYTES crypto_encode_857xfreeze3_avx_STRBYTES
#define crypto_encode_857xfreeze3_ITEMBYTES crypto_encode_857xfreeze3_avx_ITEMBYTES
#define crypto_encode_857xfreeze3_ITEMS crypto_encode_857xfreeze3_avx_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_encode_857xfreeze3(unsigned char *,const void *);
#ifdef __cplusplus
}
#endif

#define crypto_encode_857xfreeze3_IMPLEMENTATION "crypto_encode/857xfreeze3/avx"
#define crypto_encode_857xfreeze3_implementation "crypto_encode/857xfreeze3/avx"
#ifndef crypto_encode_857xfreeze3_avx_VERSION
#define crypto_encode_857xfreeze3_avx_VERSION "-"
#endif
#define crypto_encode_857xfreeze3_VERSION crypto_encode_857xfreeze3_avx_VERSION

#endif
