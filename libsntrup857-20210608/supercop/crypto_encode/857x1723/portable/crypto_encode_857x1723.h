#ifndef crypto_encode_857x1723_H
#define crypto_encode_857x1723_H

#define crypto_encode_857x1723_portable_STRBYTES 1152
#define crypto_encode_857x1723_portable_ITEMBYTES 2
#define crypto_encode_857x1723_portable_ITEMS 857

#define crypto_encode_857x1723 crypto_encode_857x1723_portable
#define crypto_encode_857x1723_STRBYTES crypto_encode_857x1723_portable_STRBYTES
#define crypto_encode_857x1723_ITEMBYTES crypto_encode_857x1723_portable_ITEMBYTES
#define crypto_encode_857x1723_ITEMS crypto_encode_857x1723_portable_ITEMS

#ifdef __cplusplus
extern "C" {
#endif
extern void crypto_encode_857x1723(unsigned char *,const void *);
#ifdef __cplusplus
}
#endif

#define crypto_encode_857x1723_IMPLEMENTATION "crypto_encode/857x1723/portable"
#define crypto_encode_857x1723_implementation "crypto_encode/857x1723/portable"
#ifndef crypto_encode_857x1723_portable_VERSION
#define crypto_encode_857x1723_portable_VERSION "-"
#endif
#define crypto_encode_857x1723_VERSION crypto_encode_857x1723_portable_VERSION

#endif
