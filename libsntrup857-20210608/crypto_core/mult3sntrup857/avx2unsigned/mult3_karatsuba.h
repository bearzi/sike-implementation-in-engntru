#ifndef _CORE_KARATSUBA_H_
#define _CORE_KARATSUBA_H_


#define mult3_766_refined_karatsuba3 CRYPTO_NAMESPACE(mult3_766_refined_karatsuba3)

#define mult3_894_karatsuba CRYPTO_NAMESPACE(mult3_894_karatsuba)


// assuming: a[512+254]=a[512+255]=0  b[512+254]=b[512+255]=0
void mult3_766_refined_karatsuba3( unsigned char * c , const unsigned char * a , const unsigned char * b );


// assuming: a[894]=a[895]=0  b[894]=b[895]=0
void mult3_894_karatsuba( unsigned char * c , const unsigned char * a , const unsigned char * b );


#endif
