#ifndef montproduct_H
#define montproduct_H


#include "ref.h"

// note : montproduct with v4158_16 to multiply by R=2^16

// q = 5167,  xgcd(2**16 , q ) = (1, 1558, -19761)
// define q 5167
// define qinv -19761


static inline
int16 mullo__(int16 x,int16 y) { return x*y; }

static inline
int16 mulhi__(int16 x,int16 y) { return (x*(int32)y)>>16; }

static inline
int16 montproduct( int16 x , int16 y )
{
  int16 lo = mullo__( x , y );
  int16 hi = mulhi__( x , y );
  int16 d = mullo__( lo , -19761 );
  int16 e = mulhi__( d , 5167 );
  return hi-e;
}

#endif
